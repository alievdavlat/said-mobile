/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path')

/** @type {import('next').NextConfig} */


module.exports = {
  trailingSlash: true,
  reactStrictMode: false,
  ranspilePackages: ['mui-tel-input'],
  images: {
    domains: ['test.janaporcelain.uz', 'localhost:3000'],
  },

  webpack: config => {
    config.resolve.alias = {
      ...config.resolve.alias,
      apexcharts: path.resolve(__dirname, './node_modules/apexcharts-clevision')
    }

    return config
  }
}
