
backend hatola
------------------------

7 - transfer product qgndan kein product boshqa storega yozlmavoti  va ozidanam kamaymavoti 



bugungi ishla
--------------------



const Codes = (props: CodesProps) => {
  const { control, index } = props
  const codes = useFieldArray({
    control,
    name: `cargo_products.${index}.imei`
  })
  const handleRemoveCode = (codeIndex: any) => {
    codes.remove(codeIndex)
  }

  const handleAddCode = () => {
    codes.append({code:''})
  }

  return (
    <>
      {codes.fields.map((codeItem, codeIndex: number) => (
        <Grid item xs={6} key={codeItem.id} sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <ControllerInput
            control={control}
            name={`cargo_products.${index}.imei.${codeIndex}`}
            label={`Code ${codeIndex + 1}`}
            type='number'
            placeholder='Enter code..'
          />
          <Button color='error' variant='text' sx={{ mt: 3, ml: 2 }} onClick={() => handleRemoveCode(codeIndex)}>
            <Icon fontSize='1.5rem' icon='tabler:trash' />
          </Button>
        </Grid>
      ))}

      <Grid item xs={12} sx={{ display: 'flex', alignItems: 'left', justifyContent: 'left' }}>
        <Button color='primary' variant='text' sx={{ mt: 3 }} onClick={handleAddCode}>
          <Icon fontSize='1.5rem' icon='tabler:plus' />
          <Translations text='Add Code' />
        </Button>
      </Grid>
    </>
  )

}




import { Box, Button, Grid, Typography } from '@mui/material'
import React from 'react'
import Translations from 'src/layouts/components/Translations'
import { LoadingButton } from '@mui/lab'
import Form from '../../form'
import * as yup from 'yup'
import useQueryParams from 'src/hooks/useQueryParams'
import { useRouter } from 'next/router'
import ControllerInput from '../../with-controller/ControllerInput'
import Icon from 'src/@core/components/icon'
import { Control, useFieldArray } from 'react-hook-form'

interface CargoCodesProps {
  control: Control
}

const validationSchema = yup.object().shape<any>({
  codes: yup.array(),
  cargo: yup.number(),
  store: yup.number(),
  product: yup.number()
})

function CargoCodes(props: CargoCodesProps) {
  const { control } = props

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'imei'
  })

  const handleAddCode = () => {
    append({ code: '' })
  }

  const handleRemoveCode = (codeIndex: any) => {
    remove(codeIndex)
  }

  return (
    <Grid container>
      <Grid item xs={12}>
        <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between', mb: 6 }}>
          <Typography variant='body1' sx={{ my: 4 }}></Typography>
          <Button color='primary' variant='outlined' sx={{ mt: 3, ml: 2 }} onClick={handleAddCode}>
            <Icon fontSize='1.5rem' icon='tabler:plus' /> {'  '}
            <Translations text='Add Code' />
          </Button>
        </Box>
      </Grid>
      {fields.map((item, index: number) => (
        <Grid item xs={12} sm={6} key={item.id} sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', mb: 4 }}>
          <ControllerInput
            control={control}
            name={`imei.${index}.code`}
            type='number'
            label={`Code ${index + 1}`}
            placeholder='add code..'
          />
          <Button color='primary' variant='text' sx={{ mt: 4, ml: 2 }} onClick={() => handleRemoveCode(index)}>
            <Icon fontSize='1.5rem' icon='tabler:trash' />
          </Button>
        </Grid>
      ))}
    </Grid>
  )
}



function CargoUpdateCode() {
  const query = useQueryParams()
  const route = useRouter()
  const { id } = route.query

  const initialValues = {
    imei: [{ code: '' }],
    cargo: query.get('cargoId'),
    store: query.get('storeId'),
    product: query.get('productId')
  }

  return (
    <Form
      url={'/cargo/update-code'}
      validationSchema={validationSchema}
      initialValues={initialValues}
      withOutIdOnpUt={true}
      method='PUT'
    >
      {({ handleFinish, createInfo, form }) => {
        const { control, handleSubmit } = form

        return (
          <Grid container spacing={6} p={10}>
            {/* codes  */}
            <Grid item xs={12}>
              <CargoCodes control={control} />
            </Grid>

            <Grid item sm={4}>
              <LoadingButton
                variant='contained'
                loading={createInfo?.isLoading}
                fullWidth
                onClick={handleSubmit(async (data: any) => {
                  const codes: any[] = []

                  for (let i = 0; i < data.imei.length; i++) {
                    codes.push(data.imei[i].code)
                  }
                  await handleFinish({ ...data, codes, id: Number(id) })
                  query.removeMany('updateCode', 'cargoId', 'storeId', 'productId')
                })}
              >
                <Translations text='Submit' />
              </LoadingButton>
            </Grid>
          </Grid>
        )
      }}
    </Form>
  )
}

export default CargoUpdateCode




import { Grid } from '@mui/material'
import React from 'react'
import Translations from 'src/layouts/components/Translations'
import { LoadingButton } from '@mui/lab'
import Form from '../../form'
import * as yup from 'yup'
import useQueryParams from 'src/hooks/useQueryParams'
import { useRouter } from 'next/router'
import CargoUpdateProductArrayForm from './CargoUpdateProductArrayForm'

const validationSchema = yup.object().shape<any>({
  cargo: yup.number(),
  cargo_products: yup.array().of(
    yup.object().shape({
      product: yup.number().required('Product is required'),
      quantity: yup.string().required('Quantity is required'),
      price: yup.string().required('Price is required'),
      total_price: yup.string().required('Total Price is required'),
      store: yup.number().required('Store is required'),
      remain_price: yup.string().required('Remain Price is required'),
      remain_count: yup.number().required('Remain Count is required'),
      codes: yup.array().of(yup.string())
    })
  )
})

function CargoFormUpdateProductForm() {
  const query = useQueryParams()
  const route = useRouter()
  const { id } = route.query

  const initialValues = {
    cargo: query.get('id'),
    cargo_products: [
      {
        product: 0,
        quantity: '',
        price: '',
        total_price: '',
        store: 0,
        remain_price: '',
        remain_count: 0,
        imei: [{code:''}]
      }
    ]
  }

  return (
    <Form

      url={'/cargo/update-product'}
      validationSchema={validationSchema}
      initialValues={initialValues}
      name='Product'
      method='PUT'
      withOutIdOnpUt={true}
    >
      {({ handleFinish, createInfo, form }) => {
        const { control, handleSubmit } = form

        return (
          <Grid container spacing={6} p={10}>
            <Grid item sm={12}>
                  <CargoUpdateProductArrayForm control={control} />
            </Grid>

            <Grid item sm={12}>
              <LoadingButton
                variant='contained'
                loading={createInfo?.isLoading}
                fullWidth
                onClick={handleSubmit(async (data: any) => {
                  const codes:any[] = []
                  data?.cargo_products?.forEach((product:any) => {
                    product?.imei.forEach((imeiItem:any) => {
                      codes.push(imeiItem)
                    });
                  });
                  await handleFinish({ ...data, codes, id: Number(id) })
                  query.remove('addProduct')
                })}
              >
                <Translations text='Submit' />
              </LoadingButton>
            </Grid>
          </Grid>
        )
      }}
    </Form>
  )
}

export default CargoFormUpdateProductForm






import React, { useState } from 'react'
import { Accordion, AccordionDetails, AccordionSummary, Button, Grid, Typography } from '@mui/material'
import { Control, useFieldArray } from 'react-hook-form'
import Icon from 'src/@core/components/icon'
import Translations from 'src/layouts/components/Translations'
import ControllerInput from '../../with-controller/ControllerInput'
import AsyncControllerSelect from '../../with-controller/AsyncControllerSelect'
import { SyntheticEvent } from 'react-draft-wysiwyg'

type Props = {
  control: Control
}

interface CodesProps {
  control: Control
  index: number
}
const Codes = (props: CodesProps) => {
  const { control, index } = props
  const codes = useFieldArray({
    control,
    name: `cargo_products.${index}.imei`
  })
  const handleRemoveCode = (codeIndex: any) => {
    codes.remove(codeIndex)
  }

  const handleAddCode = () => {
    codes.append({code:''})
  }

  return (
    <>
      {codes.fields.map((codeItem, codeIndex: number) => (
        <Grid item xs={6} key={codeItem.id} sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <ControllerInput
            control={control}
            name={`cargo_products.${index}.imei.${codeIndex}`}
            label={`Code ${codeIndex + 1}`}
            type='number'
            placeholder='Enter code..'
          />
          <Button color='error' variant='text' sx={{ mt: 3, ml: 2 }} onClick={() => handleRemoveCode(codeIndex)}>
            <Icon fontSize='1.5rem' icon='tabler:trash' />
          </Button>
        </Grid>
      ))}

      <Grid item xs={12} sx={{ display: 'flex', alignItems: 'left', justifyContent: 'left' }}>
        <Button color='primary' variant='text' sx={{ mt: 3 }} onClick={handleAddCode}>
          <Icon fontSize='1.5rem' icon='tabler:plus' />
          <Translations text='Add Code' />
        </Button>
      </Grid>
    </>
  )
}

const CargoUpdateProductArrayForm = (props: Props) => {
  const [expanded, setExpanded] = useState<number | false>(0)

  const handleChange = (panel: number) => (event: SyntheticEvent, isExpanded: boolean) => {
    setExpanded(isExpanded ? panel : false)
  }

  const { control } = props

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'cargo_products'
  })

  const handleAddNewField = () => {
    append({
      product: 0,
      quantity: '',
      price: '',
      total_price: '',
      store: 0,
      remain_price: '',
      remain_count: 0,
      imei: ['']
    })
  }

  const handleRemove = (idx: any) => {
    remove(idx)
  }

  return (
    <Grid container spacing={6}>
      <Grid item xs={12} sx={{ display: 'flex', alignItems: 'left', justifyContent: 'left' }}>
        <Button color='primary' variant='text' sx={{ mt: 3 }} onClick={handleAddNewField}>
          <Icon fontSize='1.5rem' icon='tabler:plus' />
          <Translations text='Add Product' />
        </Button>
      </Grid>

      {fields.map((item, index:number) => (
        <Accordion key={item.id} expanded={expanded === index} onChange={handleChange(index)}>
          <AccordionSummary
            id='controlled-panel-header-1'
            aria-controls='controlled-panel-content-1'
            expandIcon={<Icon fontSize='1.25rem' icon='tabler:chevron-down' />}
          >
            <Typography sx={{ my: 4 }}>
              <Translations text='Cargo' /> <Translations text='Products' /> {index + 1}
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Grid container spacing={6} mt={6} key={item.id}>
              <Grid item xs={12} sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                <Button color='error' variant='outlined' sx={{ mt: 3, ml: 2 }} onClick={() => handleRemove(index)}>
                  <Icon fontSize='1.5rem' icon='tabler:trash' /> {'  '}
                </Button>
              </Grid>

              <Grid item xs={6}>
                <AsyncControllerSelect
                  control={control}
                  label='Product'
                  name={`cargo_products.${index}.product`}
                  url='/product/list'
                  keyWord='id'
                  getLabel={data => data?.name}
                />
              </Grid>

              <Grid item xs={6}>
                <ControllerInput
                  control={control}
                  name={`cargo_products.${index}.quantity`}
                  type='number'
                  label={'Product quantity'}
                  placeholder=''
                />
              </Grid>
              <Grid item xs={6}>
                <ControllerInput
                  control={control}
                  name={`cargo_products.${index}.price`}
                  type='number'
                  label={'Price'}
                  placeholder=''
                />
              </Grid>

              <Grid item xs={6}>
                <ControllerInput
                  control={control}
                  name={`cargo_products.${index}.total_price`}
                  type='number'
                  label={'Total Price'}
                  placeholder=''
                />
              </Grid>

              <Grid item xs={6}>
                <AsyncControllerSelect
                  control={control}
                  label={'Store'}
                  name={`cargo_products.${index}.store`}
                  url='/stock/list'
                  keyWord='id'
                  getLabel={data => data?.name}
                />
              </Grid>

              <Grid item xs={6}>
                <ControllerInput
                  control={control}
                  name={`cargo_products.${index}.remain_price`}
                  type='number'
                  label={'Remain Price'}
                  placeholder=''
                />
              </Grid>

              <Grid item xs={12}>
                <ControllerInput
                  control={control}
                  name={`cargo_products.${index}.remain_count`}
                  type='number'
                  label={'Remain Count'}
                  placeholder=''
                />
              </Grid>

              {/* Code fields */}
              <Codes control={control} index={index} />
            </Grid>
          </AccordionDetails>
        </Accordion>
      ))}
    </Grid>
  )
}

export default CargoUpdateProductArrayForm
