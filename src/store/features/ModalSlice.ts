// ** Redux Imports
import { createSlice } from '@reduxjs/toolkit'





export const EditSlice = createSlice({
  name: 'edit',
  initialState: {
    open: false,
    id:'',
    openDelete:false
  },
  reducers: {
    setEditOpen: (state, action) => {
      state.open = action.payload
    },
    setId: (state, action) => {
     state.id = action.payload
    },
    setOpenDelete :(state, action) => {
      state.openDelete = action.payload
    }
  },
 
})

export const { setEditOpen, setId, setOpenDelete } = EditSlice.actions

export default EditSlice.reducer
