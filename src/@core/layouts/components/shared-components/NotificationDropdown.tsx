// ** React Imports
import { useState, SyntheticEvent, Fragment, ReactNode, useContext } from 'react'

// ** MUI Imports
import Box from '@mui/material/Box'
import Badge from '@mui/material/Badge'
import IconButton from '@mui/material/IconButton'
import { styled, Theme } from '@mui/material/styles'
import useMediaQuery from '@mui/material/useMediaQuery'
import MuiMenu, { MenuProps } from '@mui/material/Menu'
import MuiMenuItem, { MenuItemProps } from '@mui/material/MenuItem'
import Typography, { TypographyProps } from '@mui/material/Typography'

// ** Icon Imports
import Icon from 'src/@core/components/icon'

// ** Third Party Components
import PerfectScrollbarComponent from 'react-perfect-scrollbar'

// ** Type Imports
import { Settings } from 'src/@core/context/settingsContext'

// ** Custom Components Imports

// ** Util Import
import Translations from 'src/components/translations'
import { useTranslation } from 'react-i18next'

// ** MUI Imports
import Tab from '@mui/material/Tab'
import TabList from '@mui/lab/TabList'
import TabPanel from '@mui/lab/TabPanel'
import TabContext from '@mui/lab/TabContext'
import { Button } from '@mui/material'
import { NotificationContext } from 'src/context/NotificationContext'

export type NotificationsType = {
  id: number
  created_at: string
  updated_at: string
  price: string
  status: boolean
  from_store: number
  to_store: number
}

type NotificationsType2 = {
  id: number
  product: {
    id: number
    name: string
    brand: {
      id: number
      name: string
    }
    category: {
      id: number
      name: string
    }
    type: {
      id: number
      name: string
    }
  }
  status: boolean
  count: number
  from_store: {
    id: number
    name: string
  }
  to_store: {
    id: number
    name: string
  }
}
interface Props {
  settings: Settings
}

// ** Styled Menu component
const Menu = styled(MuiMenu)<MenuProps>(({ theme }) => ({
  '& .MuiMenu-paper': {
    width: 380,
    overflow: 'hidden',
    marginTop: theme.spacing(4.25),
    [theme.breakpoints.down('sm')]: {
      width: '100%'
    }
  },
  '& .MuiMenu-list': {
    padding: 0,
    '& .MuiMenuItem-root': {
      margin: 0,
      borderRadius: 0,
      padding: theme.spacing(4, 6),
      '&:hover': {
        backgroundColor: theme.palette.action.hover
      }
    }
  }
}))

// ** Styled MenuItem component
const MenuItem = styled(MuiMenuItem)<MenuItemProps>(({ theme }) => ({
  paddingTop: theme.spacing(3),
  paddingBottom: theme.spacing(3),
  '&:not(:last-of-type)': {
    borderBottom: `1px solid ${theme.palette.divider}`
  }
}))

// ** Styled PerfectScrollbar component
const PerfectScrollbar = styled(PerfectScrollbarComponent)({
  maxHeight: 349
})

// ** Styled component for the title in MenuItems
const MenuItemTitle = styled(Typography)<TypographyProps>({
  fontWeight: 500,
  flex: '1 1 100%',
  overflow: 'hidden',
  whiteSpace: 'nowrap',
  textOverflow: 'ellipsis'
})

// ** Styled component for the subtitle in MenuItems
const MenuItemSubtitle = styled(Typography)<TypographyProps>({
  flex: '1 1 100%',
  overflow: 'hidden',
  whiteSpace: 'nowrap',
  textOverflow: 'ellipsis'
})

const ScrollWrapper = ({ children, hidden }: { children: ReactNode; hidden: boolean }) => {
  if (hidden) {
    return <Box sx={{ maxHeight: 349, overflowY: 'auto', overflowX: 'hidden' }}>{children}</Box>
  } else {
    return <PerfectScrollbar options={{ wheelPropagation: false, suppressScrollX: true }}>{children}</PerfectScrollbar>
  }
}

const NotificationDropdown = (props: Props) => {
  // ** Props
  const { settings } = props

  const { t } = useTranslation()
  const {
    transactionData,
    transactionMutate,
    transactionRefetch,
    transactionSuccess,
    transferData,
    transferMutate,
    transferRefetch,
    transferSuccess,
    noteLength
  } = useContext(NotificationContext)
  
  // ** States
  const [value, setValue] = useState<string>('1')
  const [anchorEl, setAnchorEl] = useState<(EventTarget & Element) | null>(null)

  // ** Hook
  const hidden = useMediaQuery((theme: Theme) => theme.breakpoints.down('lg'))

  // ** Vars
  const { direction } = settings

  const handleDropdownOpen = (event: SyntheticEvent) => {
    setAnchorEl(event.currentTarget)
  }

  const handleDropdownClose = () => {
    setAnchorEl(null)
  }

  const handleChange = (event: SyntheticEvent, newValue: string) => {
    setValue(newValue)
  }

  return (
    <Fragment>
      <IconButton color='inherit' aria-haspopup='true' onClick={handleDropdownOpen} aria-controls='customized-menu'>
        <Badge
          color='error'
          badgeContent={noteLength}
          sx={{
            '& .MuiBadge-badge': { top: 4, right: 4, boxShadow: theme => `0 0 0 2px ${theme.palette.background.paper}` }
          }}
        >
          <Icon fontSize='1.625rem' icon='tabler:bell' />
        </Badge>
      </IconButton>

      <Menu
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleDropdownClose}
        anchorOrigin={{ vertical: 'bottom', horizontal: direction === 'ltr' ? 'right' : 'left' }}
        transformOrigin={{ vertical: 'top', horizontal: direction === 'ltr' ? 'right' : 'left' }}
      >
        <MenuItem
          disableRipple
          disableTouchRipple
          sx={{ cursor: 'default', userSelect: 'auto', backgroundColor: 'transparent !important' }}
        >
          {/* notification Title */}
          <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', width: '100%' }}>
            <Typography variant='h5' sx={{ cursor: 'text' }}>
              <Translations text='Notifications' />
            </Typography>
          </Box>
        </MenuItem>

        {/* tab header */}
        <TabContext value={value}>
         
          <TabList scrollButtons variant='fullWidth' onChange={handleChange} aria-label='forced scroll tabs example'>
            <Tab value='1' label={t('Transactions')} icon={<Icon icon='uil:transaction' />} />
            <Tab value='2' label={t('Transfers')} icon={<Icon icon='ri:currency-line' />} />
          </TabList>

          {/* Transactions */}
          <TabPanel value='1'>
            <ScrollWrapper hidden={hidden}>
              {/* transaction render  */}
              {transactionSuccess &&
                transactionData?.results.length &&
                transactionData?.results?.map((notification: NotificationsType, index: number) => (
                  <MenuItem key={index} disableRipple disableTouchRipple onClick={handleDropdownClose}>
                    <Box sx={{ width: '100%', display: 'flex', alignItems: 'center' }}>
                      <Box
                        sx={{
                          mr: 4,
                          ml: 2.5,
                          flex: '1 1',
                          display: 'flex',
                          overflow: 'hidden',
                          flexDirection: 'column'
                        }}
                      >
                        <MenuItemTitle>{notification.price}</MenuItemTitle>
                        <MenuItemSubtitle variant='body2'>{notification.created_at}</MenuItemSubtitle>
                      </Box>
                      <Button
                        variant='contained'
                        onClick={() => {
                          transactionMutate(notification.id)
                          transactionRefetch()
                        }}
                      >
                        <Translations text='Accept' />
                      </Button>
                    </Box>
                  </MenuItem>
                ))}

              {/* transaction no data  */}
              {!transactionData?.results.length && (
                <MenuItem
                  disableRipple
                  disableTouchRipple
                  sx={{
                    borderBottom: 0,
                    cursor: 'default',
                    userSelect: 'auto',
                    backgroundColor: 'transparent !important',
                    borderTop: theme => `1px solid ${theme.palette.divider}`
                  }}
                >
                  <Typography variant='h4'>
                    <Translations text='Data Not Found!' />
                  </Typography>
                </MenuItem>
              )}
            </ScrollWrapper>
          </TabPanel>

          {/* Transfers */}
          <TabPanel value='2'>
            <ScrollWrapper hidden={hidden}>
              {/* tranfer render */}

              {transferSuccess &&
                transferData?.results?.length &&
                transferData?.results?.map((notification: NotificationsType2, index: number) => (
                  <MenuItem key={index} disableRipple disableTouchRipple onClick={handleDropdownClose}>
                    <Box sx={{ width: '100%', display: 'flex', alignItems: 'center' }}>
                      <Box
                        sx={{
                          mr: 4,
                          ml: 2.5,
                          flex: '1 1',
                          display: 'flex',
                          overflow: 'hidden',
                          flexDirection: 'column'
                        }}
                      >
                        <MenuItemTitle>{notification.count}</MenuItemTitle>
                        <MenuItemSubtitle variant='body2'>
                          {notification.product.name} - {notification.product.category.name} -{' '}
                          {notification.product.brand.name} - {notification.product.type.name}
                        </MenuItemSubtitle>
                      </Box>

                      <Button
                        variant='contained'
                        onClick={() => {
                          transferMutate(notification.id)
                          transferRefetch()
                        }}
                      >
                        <Translations text='Accept' />
                      </Button>
                    </Box>
                  </MenuItem>
                ))}

              {/* No data message */}
              {!transferData?.results.length && (
                <MenuItem
                  disableRipple
                  disableTouchRipple
                  sx={{
                    borderBottom: 0,
                    cursor: 'default',
                    userSelect: 'auto',
                    backgroundColor: 'transparent !important',
                    borderTop: theme => `1px solid ${theme.palette.divider}`
                  }}
                >
                  <Typography variant='h4'>
                    <Translations text='Data Not Found!' />
                  </Typography>
                </MenuItem>
              )}
            </ScrollWrapper>
          </TabPanel>
        </TabContext>
      </Menu>
    </Fragment>
  )
}

export default NotificationDropdown
