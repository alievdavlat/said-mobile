// ** Next Import
import Link from 'next/link'

// ** MUI Imports
import Box from '@mui/material/Box'
import { styled } from '@mui/material/styles'
import Typography from '@mui/material/Typography'
import { useTranslation } from 'react-i18next'

const StyledCompanyName = styled(Link)(({ theme }) => ({
  fontWeight: 500,
  textDecoration: 'none',
  color: `${theme.palette.primary.main} !important`
}))



const FooterContent = () => {
  // ** Var
  const {t} = useTranslation()
  
  return (
    <Box sx={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'space-between' }}>
      <Typography sx={{ mr: 2, display: 'flex', color: 'text.secondary' }}>
        {`© ${new Date().getFullYear()}, ${t('Created By')}  `}
        <Typography sx={{ ml: 1 }} target='_blank' href='https://www.novastudio.uz/' component={StyledCompanyName}>
          Novas 
        </Typography>
      </Typography>
    
    </Box>
  )
}

export default FooterContent
