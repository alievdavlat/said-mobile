import qs from 'qs'
import { useRouter } from 'next/router'

export interface IMultiple {
  remove: string
  set: object
}

const useQueryParams = () => {
  // const history = useHistory();
  // const location = useLocation();
  const router = useRouter()
  //@ts-ignore
  const queryParams = qs.parse(router.query, { ignoreQueryPrefix: true })
  const set = (key: string, value: string) => router.push({ query: qs.stringify({ ...queryParams, [key]: value }) })

  const setObj = (value: object) => router.push({ query: qs.stringify({ ...queryParams, ...value }) })
  const clear = () => router.push({ query: qs.stringify({}) })

  const append = (values: object) => router.push({ query: qs.stringify({ ...queryParams, ...values }) })

  const remove = (key: string) => {
    const newParams = { ...queryParams }
    if (newParams[key]) {
      delete newParams[key]
    }

    router.push({ query: qs.stringify(newParams) })
  }
  const multiple = ({ remove, set }: IMultiple) => {
    let newParams = { ...queryParams }
    const items = remove.split(',')
    items.forEach(k => {
      if (newParams[k]) {
        delete newParams[k]
      }
    })

    newParams = { ...newParams, ...set }

    router.push({
      query: qs.stringify(remove.includes('*') ? set : newParams)
    })
  }
  const removeMany = (...values: string[]) => {
    const newParams = { ...queryParams }
    values.forEach(k => {
      if (newParams[k] !== undefined) {
        delete newParams[k]
      }
    })

    router.push({ search: qs.stringify(newParams) })
  }

  const has = (key: string) => !!queryParams[key]
  const get = (key: string) => queryParams[key]
  const secureGet = (key: string) => queryParams[key] || ''
  const goBack = () => router.back()

  return {
    values: queryParams,
    set,
    remove,
    clear,
    append,
    has,
    get,
    goBack,
    multiple,
    secureGet,
    removeMany,
    setObj
  }
}

export default useQueryParams
