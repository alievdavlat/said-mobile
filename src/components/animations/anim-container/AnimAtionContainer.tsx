import React from 'react'
import { useGSAP } from "@gsap/react";
import gsap from "gsap";

type Props = {
  children: React.ReactNode
  animOne:any,
  animTwo:any
}

const AnimationContainer = (props: Props) => {

    // ** Hooks 
  
    useGSAP(() => {
      gsap.fromTo('#anim-container', props.animOne, props.animTwo)
    }, [])
  
    
  return (
    <div id='anim-container'>
      {props.children}
    </div>
  )
}

export default AnimationContainer