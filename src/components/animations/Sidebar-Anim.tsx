import React, { useLayoutEffect, useRef } from 'react'
import gsap from 'gsap'

interface AnimatedSidebarProps {
  children: React.ReactNode
}

const AnimatedSidebarWrapper: React.FC<AnimatedSidebarProps> = ({ children }) => {
  const sidebarRef = useRef<HTMLDivElement | null>(null)

  useLayoutEffect(() => {
    const sidebarItems = sidebarRef.current?.querySelectorAll('.nav-link')

    if (sidebarItems) {
      gsap.fromTo(
        sidebarItems,
        { x: -200, opacity: 0 },
        {
          x: 0,
          opacity: 1,
          duration: 0.8,
          ease: 'power3.inOut',
          stagger: {
            amount: 0.15, // Total time to stagger all animations
            from: 'center'
          }
        }
      )
    }
  }, [])

  return <div ref={sidebarRef}>{children}</div>
}

export default AnimatedSidebarWrapper
