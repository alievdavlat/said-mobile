import React from 'react'
import { CKEditor } from '@ckeditor/ckeditor5-react'

//@ts-ignore
import Editor from 'ckeditor5-custom-build/build/ckeditor'
import { API_URL, MEDIA_URL } from 'src/configs/req'
import { FormControl, FormHelperText, Typography } from '@mui/material'
import { Control, Controller, FieldErrors } from 'react-hook-form'
import { Box } from '@mui/system'

const UPLOAD_ENDPOINT = 'media_upload'

type CategoryInput = {
  name: string
  label: string
  control: Control
  error: FieldErrors
}

function MyEditor(props: CategoryInput) {
  function uploadAdapter(loader: any) {
    return {
      upload: () => {
        return new Promise((resolve, reject) => {
          const body = new FormData()
          loader.file.then((file: File) => {
            body.append('photo', file)
            body.append('directory', 'pages')
            console.log(body, file)
            fetch(`${API_URL}v1/${UPLOAD_ENDPOINT}`, {
              method: 'post',
              body: body,
              headers: {
                Authorization: `Bearer ${window.localStorage.getItem('accessToken')}`
              }
            })
              .then(res => res.json())
              .then(res => {
                resolve({
                  default: `${MEDIA_URL}/${res?.data}`
                })
              })
              .catch(err => {
                reject(err)
              })
          })
        })
      }
    }
  }
  function uploadPlugin(editor: any) {
    editor.plugins.get('FileRepository').createUploadAdapter = (loader: any) => {
      return uploadAdapter(loader)
    }
  }
  console.log(props.error)

  return (
    <Controller
      name={props.name}
      control={props.control}
      defaultValue=''
      render={({ field }) => (
        <FormControl error={!!props.error?.type} fullWidth>
          <Typography color={props.error ? 'error' : 'text'} mb={2}>
            {' '}
            {props.label}
          </Typography>
          <Box id='my-inputs' aria-describedby='my-helper-text'>
            <CKEditor
              config={{
                extraPlugins: [uploadPlugin]
              }}
              editor={Editor}
              data={field.value}
              onChange={(_, editor:any) => field.onChange(editor.getData())}
            />
          </Box>
          <FormHelperText id='my-helper-text'>{props.error?.message?.toString() || ''}</FormHelperText>
        </FormControl>
      )}
    />
  )
}

export default React.memo(MyEditor)
