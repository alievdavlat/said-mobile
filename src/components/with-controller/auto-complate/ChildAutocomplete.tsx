import React, { memo, useEffect, useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import Select from 'react-select'
import { FormHelperText, InputLabel, useTheme } from '@mui/material'

interface ChildProps {
  onChange: (item: any) => void
  options: any[]
  value: any
  ref: any
  multiple?: boolean
  error: any
  label: string
  isLoading: boolean
  disabled?: boolean
  getOptionLabel: (option: any) => string
  filterOption?: (option: any, inputValue: string) => boolean
  getValue: (option: any) => string
  search?: string
  setSearch?: React.Dispatch<React.SetStateAction<string>>
  onSelect?: (data: any) => void
  autoWidth?: boolean
  onlyArray?: boolean
}
const ChildAutocomplete = (props: ChildProps) => {
  const theme = useTheme()
  const { t } = useTranslation()

  const getValue = useMemo(() => {  
    const isArray = Array.isArray(props?.value);
  
    if (!props.isLoading) {
      if (props.multiple) {
        return isArray
          ? props.options?.filter((item: any) => props?.value?.some((el: any) => el == props.getValue(item)))
          : [];
      } else {
        return !!props?.value ? props.options.find(item => props.getValue(item) == props?.value) : null;
      }
    }
  }, [props]);
  


  useEffect(() => {
    if (!props.isLoading) {
      if (props.multiple) {
        if (!getValue) {
          props.onChange([])
        }
      } else {
        if (!getValue) {
          props.onChange(null)
        }
      }
    }
  }, [props.multiple, props.isLoading, getValue])

  const colorStyles = {
    control: (style: any, state: any) => ({
      ...style,
      alignItems: 'flex-start',
      padding: 0,
      borderRadius: 6,
      backgroundColor: 'transparent !important',
      borderColor: state.isFocused
        ? `rgba(${theme.palette.customColors.main}, 1)`
        : `rgba(${theme.palette.customColors.main}, 0.2)`,
      boxShadow: state.isFocused ? theme.shadows[2] : null,
      '&:hover': {
        borderColor: state.isFocused
          ? `rgba(${theme.palette.customColors.main}, 1)`
          : `rgba(${theme.palette.customColors.main}, 0.28)`
      }
    }),
    option: (style: any, state: any) => ({
      ...style,
      color: state.isSelected ? 'white' : 'black',
      backgroundColor: state.isSelected ? theme.palette.primary.main : null,
      '&:hover': {
        backgroundColor: state.isSelected ? theme.palette.primary.main : theme.palette.action.hover
      }
    }),
    placeholder: (style: any) => ({
      ...style,
      color: theme.palette.text.secondary
    }),
    singleValue: (style: any) => ({
      ...style,
      color: theme.palette.text.primary
    }),
    menu: (style: any) => ({
      ...style,
      borderRadius: 8,
      boxShadow: theme.shadows[3]
    }),
    multiValue: (style: any) => ({
      ...style,
      backgroundColor: theme.palette.action.selected,
      borderRadius: 6
    }),
    multiValueLabel: (style: any) => ({
      ...style,
      color: theme.palette.text.primary
    }),
    multiValueRemove: (style: any) => ({
      ...style,
      color: theme.palette.text.primary,
      '&:hover': {
        backgroundColor: theme.palette.error.main,
        color: 'white'
      }
    }),
    input: (style: any) => ({
      ...style,
      color: theme.palette.text.primary
    }),
    menuPortal: (base: any) => ({
      ...base,
      zIndex: 9999
    })
  }

  return (
    <form style={{ height: '100%' }}>
      <InputLabel id='aria-label' htmlFor='aria-example-input' sx={{mb:1}}>
        {t(props.label)}
      </InputLabel>
      <Select
        aria-labelledby='aria-label'
        inputId='aria-example-input'
        name='aria-live-color'
        classNamePrefix='select'
        defaultValue={props.value}
        menuPortalTarget={document.body}
        isDisabled={props.disabled}
        isLoading={props.isLoading}
        isMulti={props.multiple}
        inputValue={props.search}
        onInputChange={e => props.setSearch && props.setSearch(e)}
        value={getValue}
        getOptionValue={props.getValue}
        getOptionLabel={props.getOptionLabel}
        filterOption={props.filterOption}
        onChange={e => {
          if (e === null) {
            props.onChange(props.multiple ? [] : 0);
          } else {
            props.multiple
              ? props.onChange(e.map((item: any) => props.getValue(item)))
              : props.onChange(props.getValue(e));
          }
          if (props.onSelect) {
            props.onSelect(e);
          }
        }}
        isClearable
        isSearchable={true}
        options={props.options}
        styles={colorStyles}
      />
      {props.error && (
        <FormHelperText error id='my-helper-text'>
          {t(props.error?.message)}
        </FormHelperText>
      )}
    </form>
  )
}
export default memo(ChildAutocomplete)
