import React from 'react'
import { Control, Controller, FieldError, FieldErrors } from 'react-hook-form'
import { TextFieldProps } from '@mui/material/TextField'
import { useTranslation } from 'react-i18next'
import { styled } from '@mui/material/styles'
import MuiInputLabel, { InputLabelProps } from '@mui/material/InputLabel'
import { MuiTelInput, matchIsValidTel } from "mui-tel-input";


const InputLabel = styled(MuiInputLabel)<InputLabelProps>(({ theme }) => ({
  lineHeight: 1.154,
  maxWidth: 'max-content',
  marginBottom: theme.spacing(1),
  color: theme.palette.text.primary,
  fontSize: theme.typography.body2.fontSize
}))

type CategoryInput = {
  name: string
  label: string
  placeholder: string
  control?: Control
  error?: FieldError | FieldErrors
  inputProps?: TextFieldProps
  type?: string
  icon?: string
  disabled?: boolean
}

const ControllerMask: React.FC<CategoryInput> = ({ name, label, control }) => {
  const { t } = useTranslation()

  return (
    <Controller
      key={name}
      name={name}
      control={control}
      rules={{ validate: (value) => matchIsValidTel(value, { onlyCountries: ['FR'] }) }}
      render={({ field: { ref: fieldRef, value, ...fieldProps }, fieldState }) => (
        <>
        <InputLabel htmlFor='phone-number'>{t(label)}</InputLabel>
        <MuiTelInput
          {...fieldProps}
          value={value ?? ''}
          fullWidth
          defaultCountry='UZ'
          inputRef={fieldRef}
          onlyCountries={["UZ"]}
          helperText={fieldState.invalid ? t('Tel is invalid') : ""}
          error={fieldState.invalid}
        />
        </>
      )}
    />
  )
}

export default ControllerMask
