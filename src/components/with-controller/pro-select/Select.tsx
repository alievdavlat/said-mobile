//@ts-nocheck
import { FormControl, FormHelperText, MenuItem } from '@mui/material'
import React, { useEffect, useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import CustomTextField from 'src/@core/components/mui/text-field'

type Props = {
  onChange: (item: any) => void
  options: any[]
  value: any
  ref: any
  multiple?: boolean
  error?: any
  label: string
  isLoading: boolean
  disabled?: boolean
  getOptionLabel?: (option: any) => string
  getValue: (option: any) => string
  onSelect?: (data: any) => void
  multiline?: boolean
  getLabel?:(data:string) => string
  keyWord?:string
  name:string
}
const Select = (props: Props) => {
  const { t } = useTranslation();

  const getValue = useMemo(() => {
    const isArray = Array.isArray(props?.value);
    if (!props.isLoading) {
      if (props.multiple) {
        return isArray
          ? props.options?.filter((item: any) => props?.value?.some((el: any) => el === props.getValue(item)))
          : [];
      } else {
        return !!props?.value ? props.options.find((item) => props.getValue(item) === props?.value) : null;
      }
    }
  }, [props.value, props.isLoading, props.multiple, props.options, props.getValue]);

  useEffect(() => {
    if (!props.isLoading) {
      if (props.multiple) {
        if (!getValue) {
          props.onChange([]);
        }
      } else {
        if (!getValue) {
          props.onChange(null);
        }
      }
    }
  }, [props.multiple, props.isLoading, getValue]);

  const handleChange = (e: React.ChangeEvent<{ value: unknown }>) => {
    const value = e.target.value;
    if (props.multiple) {
      props.onChange(Array.isArray(value) ? value.map((item: any) => item.id) : []);
    } else {
      props.onChange(value === null ? null : props.getValue(value));
    }
    if (props.onSelect) {
      props.onSelect(value);
    }
  };

  

  return (
    <FormControl error={!!props.error?.type} fullWidth>
      <CustomTextField
        select
        fullWidth
        defaultValue={props?.value}
        disabled={props.disabled}
        label={t(props.label)}

        id={'select-controlled' + props.name}
        SelectProps={{
          value: getValue,
          multiline: props.multiline ?? false,
          multiple: props.multiple ?? false,
          onChange: handleChange,
        }}
      >
        {props.options?.map((el: any) => (
          <MenuItem key={props.getValue(el)} value={el}>
            {props.getLabel ? props.getLabel(el) : el.name || el}
          </MenuItem>
        ))}
      </CustomTextField>
      {!!props.error?.message && (
        <FormHelperText id="my-helper-text">{props.error?.message}</FormHelperText>
      )}
    </FormControl>
  );
};



export default Select
