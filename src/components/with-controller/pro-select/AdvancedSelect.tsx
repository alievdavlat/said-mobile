import React from 'react'
import { Controller } from 'react-hook-form'
import GetContainer from 'src/components/get-container'
import Select from './Select'

type Props = {
  control: any
  name: string
  label: any
  getOptionLabel?: (option: any) => string
  getValue: (option: any) => string
  multiple?: boolean
  error?: any
  disabled?: boolean
  url: string
  onSelect?: (data: any) => void
  params?: any
  multiline?: boolean
  getLabel?: (data: any) => string
  keyWord?: string
  exception?: string
  getData:(data:any) => any
}

const AdvancedSelect = (props: Props) => {

  const isObject = (value: any) => {
    if (typeof value === 'object' && !Array.isArray(value) && value !== null) {
      return true
    } else {
      return false
    }
  }

  const value = (data: any) => {
    if (props.multiple) {
      if (isObject(data?.[0])) {
        return data?.map((item: any) => {
          return props.getValue(item)
        })
      } else {
        return data
      }
    } else {
      if (isObject(data)) {
        return props.getValue(data)
      } else {
        return data
      }
    }
  }

  return (
    <GetContainer  url={props.url} params={props.params ? props.params : {}}>
      {({ data, isLoading }) => {
        
        return  (
        
          <Controller
            control={props.control}
            name={props.name}
            render={({ field: { ref, onChange, ...field } }) => {
              return (
                <Select
                  options={props.getData(data) || []}
                  isLoading={isLoading}
                  value={value(field?.value)}
                  onChange={onChange}
                  ref={ref}
                  disabled={props.disabled}
                  {...props}
                />
              )
            }}
          />
        )
      }}
    </GetContainer>
  )
}

export default AdvancedSelect
