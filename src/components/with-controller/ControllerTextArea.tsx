import React from 'react'
import { Control, Controller, FieldErrors, FieldError } from 'react-hook-form'
import CustomTextField from 'src/@core/components/mui/text-field'
import { TextFieldProps } from '@mui/material/TextField'
import { useTranslation } from 'react-i18next'

type TextAreaFieldProps = {
  name: string
  label: string
  placeholder: string
  control?: Control
  error?: FieldErrors  | FieldError
  inputProps?: TextFieldProps
  type?: string
  icon?:string
}
const ControllerTextArea = (props: TextAreaFieldProps) => {
  const {label, name, placeholder, control} = props
  const { t } = useTranslation()

  return (
    <Controller
      key={name}
      name={name}
      control={control}
      render={({ field }) => (
        <CustomTextField
        fullWidth
        multiline 
        rows={8}
        defaultValue={''}
        label={t(label)}
        value={field.value}
        placeholder={t(placeholder).toString()}
        onChange={(e) => field.onChange(e.target.value)}
        id='textarea-outlined-controlled'
      />
      )}
    />
  )
}

export default ControllerTextArea