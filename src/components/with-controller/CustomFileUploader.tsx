// ** React Imports
import { Fragment } from 'react'

// ** MUI Imports
import Box from '@mui/material/Box'
import Typography from '@mui/material/Typography'
import IconButton from '@mui/material/IconButton'

// ** Icon Imports
import Icon from 'src/@core/components/icon'

// ** Third Party Components
import toast from 'react-hot-toast'
import { useDropzone } from 'react-dropzone'
import { Stack } from '@mui/system'
import { useMutation } from '@tanstack/react-query'
import { MEDIA_URL, hanldeRequest } from 'src/configs/req'

interface IProps {
  value: string
  onChange: (val: string) => void
}

const CustomFileUploader = (props: IProps) => {
  // ** State
  const { mutate } = useMutation({
    mutationFn: async data => {
      const response = await hanldeRequest({
        url: 'files/upload',
        data: data,
        method: 'post'
      })

      return response.data
    },
    onSuccess: data => {
      props.onChange(data?.file)
      console.log(data)
    }
  })

  // ** Hooks
  const { getRootProps, getInputProps } = useDropzone({
    maxFiles: 1,
    maxSize: 2000000,
    accept: {
      'image/*': ['.png', '.jpg', '.jpeg', '.gif']
    },
    onDrop: (acceptedFiles: File[]) => {
      const data: any = new FormData()
      data.append('file', acceptedFiles?.[0])
      mutate(data)
    },
    onDropRejected: () => {
      toast.error('You can only upload 2 files & maximum size of 2 MB.', {
        duration: 2000
      })
    }
  })

  const renderFilePreview = () => {
    const formatFile = props.value?.split('.')?.[1]
    const fileName = props.value?.split('/')?.[props.value?.split('/')?.length - 1]
    if (formatFile === 'webp' || formatFile === 'svg' || formatFile === 'png' || formatFile === 'gpg') {
      return <img width={38} height={38} alt={fileName} src={MEDIA_URL + props.value} />
    } else {
      return <Icon icon='tabler:file-description' />
    }
  }

  const handleRemoveFile = () => {
    props.onChange('')
  }

  const fileList = () => {
    const fileName = props.value?.split('/')?.[props.value?.split('/')?.length - 1]
    console.log(props.value)

    return (
      <Stack direction={'row'} justifyContent={'space-between'} alignItems={'center'} sx={{ width: '100%' }}>
        <Box>
          <div>{renderFilePreview()}</div>
        </Box>
        <div>
          <Typography>{fileName}</Typography>
        </div>
        <IconButton onClick={handleRemoveFile}>
          <Icon icon='tabler:x' fontSize={20} />
        </IconButton>
      </Stack>
    )
  }

  return (
    <Fragment>
      <div {...getRootProps({ className: 'dropzone' })}>
        <input {...getInputProps()} />
        <Box sx={{ display: 'flex', textAlign: 'center', alignItems: 'center', flexDirection: 'column' }}>
          <Box
            sx={{
              mb: 8.75,
              width: 48,
              height: 48,
              display: 'flex',
              borderRadius: 1,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: theme => `rgba(${theme.palette.customColors.main}, 0.08)`
            }}
          >
            <Icon icon='tabler:upload' fontSize='1.75rem' />
          </Box>
          <Typography variant='h4' sx={{ mb: 2.5 }}>
            Drop files here or click to upload.
          </Typography>
          {/* <Typography sx={{ color: 'text.secondary' }}>Allowed *.jpeg, *.jpg, *.png, *.gif</Typography> */}
          {/* <Typography sx={{ color: 'text.secondary' }}>Max 2 files and max size of 2 MB</Typography> */}
        </Box>
      </div>
      {props.value && <Box width={'100%'}>{fileList()}</Box>}
    </Fragment>
  )
}

export default CustomFileUploader
