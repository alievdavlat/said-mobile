import React from 'react';
import { Control, Controller, FieldError, FieldErrors } from 'react-hook-form';
import { FormControl, FormControlLabel, FormHelperText, Radio, RadioGroup } from '@mui/material';
import { useTranslation } from 'react-i18next';

type IRadio = {
  name: string;
  label: string;
  control: Control;
  error?: FieldError | FieldErrors;
}

const ControllerRadio: React.FC<IRadio> = ({ name, control, error }) => {
  const {t} = useTranslation()

  return (
    <Controller
      name={name}
      control={control}
      render={({ field }) => (
        <FormControl
          sx={{ flexWrap: 'wrap', flexDirection: 'row', m: 3 }}
          required
          error={!!error?.type}
          component='fieldset'
          variant='standard'
        >
          <RadioGroup
            row
            value={field.value}
            onChange={(e) => field.onChange(e.target.value === 'true' ? true : false)} // Convert string to boolean
            name='sklad'
            aria-label='controlled'
          >
            <FormControlLabel value='true' control={<Radio />} label={t('Yes')} />
            <FormControlLabel value='false' control={<Radio />} label={t('No')} />
          </RadioGroup>
          <FormHelperText>{error?.message?.toString() || ''}</FormHelperText>
        </FormControl>
      )}
    />
  );
}

export default ControllerRadio;
