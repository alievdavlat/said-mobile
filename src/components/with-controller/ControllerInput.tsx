import React from 'react'
import { Control, Controller, FieldError, FieldErrors } from 'react-hook-form'
import CustomTextField from 'src/@core/components/mui/text-field'
import { TextFieldProps } from '@mui/material/TextField'
import { useTranslation } from 'react-i18next'
import { InputAdornment } from '@mui/material'
import Icon from 'src/@core/components/icon'

type CategoryInput = {
  name: string
  label: string
  placeholder: string
  control?: Control
  error?: FieldError | FieldErrors
  inputProps?: TextFieldProps
  type?: string
  icon?:string
  disabled?: boolean
}

const ControllerInput: React.FC<CategoryInput> = ({ name, label, placeholder, control, error, type, icon, disabled }) => {
  const { t } = useTranslation()

  return (
    <Controller
      key={name}
      name={name}
      control={control}
      
      render={({ field }) => (
        <CustomTextField
          fullWidth={true}
          label={t(label)}
          disabled={disabled}
          placeholder={t(placeholder).toString()}
          id={`form-props-full-width-${name}`}
          error={!!error?.type}
          type={type}
          InputLabelProps={{ shrink: true }}
          {...field}
          helperText={error?.message?.toString() || ''}
           InputProps={ icon ? {
             startAdornment: (
            <InputAdornment position='start'>
              <Icon icon={icon}/>
            </InputAdornment>
          )
        } : {}}
        />
      )}
    />
  )
}

export default ControllerInput
