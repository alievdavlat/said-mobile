import React from 'react'
import { Control, Controller, FieldErrors } from 'react-hook-form'
import { Checkbox, CheckboxProps, FormControl, FormControlLabel, FormHelperText } from '@mui/material'

type ICheckbox = {
  name: string
  label: string
  control: Control
  error?: FieldErrors
  checkboxProps?: CheckboxProps
}

const ControllerCheckbox: React.FC<ICheckbox> = ({ name, label, control, error }) => {
  return (
    <Controller
      name={name}
      control={control}
      render={({ field }) => (
        <FormControl required error={!!error?.type} component='fieldset' sx={{ m: 3 }} variant='standard'>
          <FormControlLabel
            control={<Checkbox checked={field.value} onChange={e => field.onChange(e.target.checked)} />}
            label={label}
          />
          <FormHelperText>{error?.message?.toString() || ''}</FormHelperText>
        </FormControl>
      )}
    />
  )
}

export default ControllerCheckbox
