import { FormControl, FormHelperText, Typography } from '@mui/material'
import React from 'react'
import { Control, Controller, FieldErrors } from 'react-hook-form'
import CustomFileUploader from './CustomFileUploader'

interface IProps {
  control: Control
  type: string
  isMedia?: boolean
  name: string
  error: FieldErrors
  label: string
}

function ControllerImage(props: IProps) {
  return (
    <Controller
      name={props.name}
      control={props.control}
      render={({ field }) => (
        <FormControl
          error={!!props.error?.type}
          fullWidth
          sx={{
            width: '100%',
            height: '100%'
          }}
        >
          <Typography color={props.error ? 'error' : 'text'} mb={2}>
            {' '}
            {props.label}
          </Typography>
          <CustomFileUploader
          value={field.value} onChange={e => field.onChange(e)}
          />
          <FormHelperText id='my-helper-text'>{props.error?.message?.toString() || ''}</FormHelperText>
        </FormControl>
      )}
    />
  )
}

export default ControllerImage
