import { useQuery } from '@tanstack/react-query'
import { ReactNode, useContext} from 'react'
import React from 'react'
import { Box } from '@mui/material'
import { hanldeRequest } from '../../configs/req'
import toast from 'react-hot-toast'
import Loader from '../../components/loader'
import { RandomContext } from 'src/context/RandomContext'

interface IProps {
  url: string
  disabled?: boolean
  params?: object
  children: (props: any) => ReactNode
  onSuccess?: (data: any) => void
  onError?: (data: any) => void
  name?: string
  hideLoading?: boolean
}

function GetContainer(props: IProps) {
  //@ts-ignore
  const { random } = useContext(RandomContext)


  const { data, isLoading, isSuccess, error, isError, isFetching, refetch } = useQuery(
    [props.url, random, ...(props?.params ? Object.values(props.params) : [])],
    async () => {
      const response: any = await hanldeRequest({
        url: props.url,
        params: { ...props.params },
        method: 'GET'
      })
      
      return response.data
    },
    {
      enabled: !!props.url && !props.disabled,
      onSuccess: (data: any) => {
        props.onSuccess && props.onSuccess(data)
      },
      onError(err: any) {
        toast.error(err.message)
      }
    }
  )

 
  
  return (
    <Box>
      {isLoading && !props.hideLoading ? (
        <Box
          style={{
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center',
            padding: 20
          }}
        >
          <Loader />
        </Box>
      ) : (
        props.children({
          data: data,
          isLoading,
          isFetching,
          isError,
          error,
          refetch,
          isSuccess
        })
      )}
    </Box>
  )
}

export default GetContainer
