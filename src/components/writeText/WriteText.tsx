import React from 'react'
import { useGSAP } from "@gsap/react";
import gsap from "gsap";
import { useTranslation } from 'react-i18next';


type Props = {
  text:string

}

const WriteText = (props: Props) => {
  
  // ** Props
  const {text} = props

  // ** Hooks 
  const {t} = useTranslation()

  useGSAP(() => {
    gsap.fromTo('#write-text', {
      opacity:0,
      x:-20,
    }, {
      opacity: 1,
      x:0,
      delay:0.2,
      stagger:0.2,
      transition:.2,
    })
  }, [])
  
  return (
    <div id='write-text'>
      {t(text)}
    </div>
  )
}

export default WriteText