import React from 'react'
import { useTheme } from '@mui/material/styles'
import { Box, useMediaQuery, Button, Typography } from '@mui/material'
import Icon from 'src/@core/components/icon'
import Translations from '../translations'
import ActionsDropDown from './ActionsDropDown'
import { Settings } from 'src/@core/context/settingsContext'
import { CustomModal } from 'src/layouts/custom-table/custom-modal'
import useQueryParams from 'src/hooks/useQueryParams'
import DebitForm from '../forms/DebitForm'
import CreditForm from '../forms/CreditForm'
import TransActionCreateFrom from '../forms/TransaActionCreateForm'
import { useAuth } from 'src/hooks/useAuth'

interface Props {
  settings: Settings
}
const TransActionsAndStockBalanceCreate = (props: Props) => {
  const theme = useTheme()
  const isMobile = useMediaQuery(theme.breakpoints.down('md'))
  const xs = useMediaQuery(theme.breakpoints.only('xs'))
  const query = useQueryParams()
  const {user} = useAuth()

  const handleOpen = (key: string) => {
    query.set(key, 'true')
  }

  console.log(user);
  

  return (
    <>
      {!xs ? (
        <Box sx={{ mr: 4 }}>
          <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center', gap: '.7rem' }}>
            <Button variant='outlined' color='success' onClick={() => handleOpen('debit')}>
              <Typography
                variant='body2'
                sx={{
                  fontSize: {
                    xs: '0.8rem',
                    md: '1rem',
                    xss: '0.5rem',
                    display: 'flex',
                    alignItems: 'center',
                    gap: '.3rem'
                  }
                }}
              >
                {!isMobile && <Icon fontSize={isMobile ? '1rem' : '1.2rem'} icon='majesticons:creditcard-plus-line' />}{' '}
                <Translations text={`Debit`} />
              </Typography>
            </Button>
            <Button variant='outlined' color='error' onClick={() => handleOpen('credit')}>
              <Typography
                variant='body2'
                sx={{
                  fontSize: {
                    xs: '0.8rem',
                    md: '1rem',
                    xss: '0.5rem',
                    display: 'flex',
                    alignItems: 'center',
                    gap: '.3rem'
                  }
                }}
              >
                {!isMobile && <Icon fontSize={isMobile ? '1rem' : '1.2rem'} icon='tdesign:undertake-transaction' />}{' '}
                <Translations text={`Credit`} />
              </Typography>
            </Button>
            <Button variant='outlined' color='info' onClick={() => handleOpen('transaction')}>
              <Typography
                variant='body2'
                sx={{
                  fontSize: {
                    xs: '0.8rem',
                    md: '1rem',
                    xss: '0.5rem',
                    display: 'flex',
                    alignItems: 'center',
                    gap: '.3rem'
                  }
                }}
              >
                {!isMobile && <Icon fontSize={isMobile ? '1rem' : '1.2rem'} icon='grommet-icons:transaction' />}{' '}
                <Translations text={`Transaction`} />
              </Typography>
            </Button>
          </Box>
        </Box>
      ) : (
        <ActionsDropDown settings={props.settings} handleOpen={handleOpen} />
      )}

      <CustomModal
        title={query.has('debit') && 'Debit' || query.has('credit') && 'Credit' || query.has('transaction') && 'Transaction'}
        open={query.has('debit') || query.has('credit') || query.has('transaction') }
        onClose={() => {
          if (query.has('debit')) {
            query.remove('debit')
          }
          if (query.has('credit')) {
            query.remove('credit')
          }
          if (query.has('transaction')) {
            query.remove('transaction')
          }
        }}
      >
        {query.has('debit') && <DebitForm/>}
        {query.has('credit') && <CreditForm/>}
        {query.has('transaction') && <TransActionCreateFrom/>}
      </CustomModal>
    </>
  )
}

export default TransActionsAndStockBalanceCreate
