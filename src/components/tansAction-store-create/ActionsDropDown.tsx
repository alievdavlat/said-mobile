// ** React Imports
import { useState, SyntheticEvent, Fragment } from 'react'

// ** Next Import
import { useRouter } from 'next/router'

// ** MUI Imports
import Box from '@mui/material/Box'
import Menu from '@mui/material/Menu'
import Badge from '@mui/material/Badge'
import Divider from '@mui/material/Divider'
import { styled } from '@mui/material/styles'
import MenuItem, { MenuItemProps } from '@mui/material/MenuItem'

// ** Icon Imports
import Icon from 'src/@core/components/icon'

// ** Context

// ** Type Imports
import { Settings } from 'src/@core/context/settingsContext'
import Translations from 'src/components/translations'

interface Props {
  settings: Settings
  handleOpen:(key:string) => void
}


const MenuItemStyled = styled(MenuItem)<MenuItemProps>(({ theme }) => ({
  '&:hover .MuiBox-root, &:hover .MuiBox-root svg': {
    color: theme.palette.primary.main
  }
}))

const ActionsDropDown = (props: Props) => {
  const { settings, handleOpen } = props

  const [anchorEl, setAnchorEl] = useState<Element | null>(null)
  const router = useRouter()
  const { direction } = settings

  const handleDropdownOpen = (event: SyntheticEvent) => {
    setAnchorEl(event.currentTarget)
  }

  const handleDropdownClose = (url?: string) => {
    if (url) {
      router.push(url)
    }
    setAnchorEl(null)
  }

  const styles = {
    px: 4,
    py: 1.75,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    color: 'text.primary',
    textDecoration: 'none',
    '& svg': {
      mr: 2.5,
      fontSize: '1.5rem',
      color: 'text.secondary'
    }
  }


  return (
     
        <Fragment>
          <Badge
            overlap='circular'
            onClick={handleDropdownOpen}
            sx={{ ml: 2, cursor: 'pointer' }}
            badgeContent={<></>}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'right'
            }}
          >
            <Icon fontSize='1.5rem' icon='mingcute:grid-line' />
          </Badge>
          <Menu
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            onClose={() => handleDropdownClose()}
            sx={{ '& .MuiMenu-paper': { width: 230, mt: 4.75 } }}
            anchorOrigin={{ vertical: 'bottom', horizontal: direction === 'ltr' ? 'right' : 'left' }}
            transformOrigin={{ vertical: 'top', horizontal: direction === 'ltr' ? 'right' : 'left' }}
          >
            <MenuItemStyled sx={{ p: 0 }}  onClick={() => handleOpen('debit')}>
              <Box sx={styles}>
              <Icon fontSize='1.5rem' icon='majesticons:creditcard-plus-line' />
              <Translations text={`Debit`} />
              </Box>
            </MenuItemStyled>
            <Divider sx={{ my: theme => `${theme.spacing(2)} !important` }} />

            <MenuItemStyled sx={{ p: 0 }} onClick={() => handleOpen('credit')}>
              <Box sx={styles}>
              <Icon fontSize='1.5rem' icon='tdesign:undertake-transaction' />
            <Translations text={`Credit`} /> 

              </Box>
            </MenuItemStyled>

            <Divider sx={{ my: theme => `${theme.spacing(2)} !important` }} />

            <MenuItemStyled sx={{ p: 0 }} onClick={() => handleOpen('transaction')}>
              <Box sx={styles}>
              <Icon fontSize='1.5rem' icon='grommet-icons:transaction' />
              <Translations text={`Transaction`} />
              </Box>
            </MenuItemStyled>
          </Menu>
        </Fragment>
  )
}

export default ActionsDropDown
