// ** MUI Imports
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'

// ** Custom Component Import
import CustomTextField from 'src/@core/components/mui/text-field'

// mui
import { IconButton } from '@mui/material'
import Icon from 'src/@core/components/icon'

interface TableHeaderProps {
  value: string
  toggle: () => void
  handleFilter: (val: string) => void
  addBtnTitle: string
  handleGet: (e: any) => void
}

const TableHeader = (props: TableHeaderProps) => {
  // ** Props
  const { handleFilter, toggle, value, addBtnTitle, handleGet } = props

  return (
    <Box
      sx={{
        py: 4,
        px: 6,
        rowGap: 2,
        columnGap: 4,
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'space-between'
      }}
    >
      <div></div>
      <Box sx={{ rowGap: 4, display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
        <CustomTextField
          value={value}
          placeholder='Search…'
          onChange={e => handleFilter(e.target.value)}
          InputProps={{
            startAdornment: (
              <IconButton sx={{ mr: 2, display: 'flex' }} onClick={handleGet}>
                <Icon fontSize='1.25rem' icon='tabler:search' />
              </IconButton>
            ),
            endAdornment: (
              <IconButton size='small' title='Clear' aria-label='Clear' onClick={() => handleFilter('')}>
                <Icon fontSize='1.25rem' icon='tabler:x' />
              </IconButton>
            )
          }}
          sx={{
            marginRight: 4,
            width: {
              xs: 1,
              sm: 'auto'
            },
            '& .MuiInputBase-root > svg': {
              mr: 2
            }
          }}
        />

        <Button onClick={toggle} variant='contained' sx={{ '& svg': { mr: 2 } }}>
          <Icon fontSize='1.125rem' icon='tabler:plus' />
          {addBtnTitle}
        </Button>
      </Box>
    </Box>
  )
}

export default TableHeader
