// ** React Imports
import React, { ReactElement, Ref, forwardRef } from 'react'

// ** MUI Imports
import Button from '@mui/material/Button'
import { styled } from '@mui/material/styles'
import IconButton from '@mui/material/IconButton'
import {
  Card,
  Dialog,
  DialogActions,
  DialogContent,
  Fade,
  FadeProps,
  IconButtonProps
} from '@mui/material'


// ** Icon Imports
import Icon from 'src/@core/components/icon'

// ** Store Imports

interface addFieldProps {
  title?: string
  open: boolean
  toggle: () => void
  children: React.ReactNode
  handleCreate: (e: any) => any
}

const Transition = forwardRef(function Transition(
  props: FadeProps & { children?: ReactElement<any, any> },
  ref: Ref<unknown>
) {
  return <Fade ref={ref} {...props} />
})

const CustomCloseButton = styled(IconButton)<IconButtonProps>(({ theme }) => ({
  top: 0,
  right: 0,
  color: 'grey.500',
  position: 'absolute',
  boxShadow: theme.shadows[2],
  transform: 'translate(10px, -10px)',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: `${theme.palette.background.paper} !important`,
  transition: 'transform 0.25s ease-in-out, box-shadow 0.25s ease-in-out',
  '&:hover': {
    transform: 'translate(7px, -5px)'
  }
}))

const AddField = (props: addFieldProps) => {
  // ** Props
  const { open, toggle, children, handleCreate } = props

  const handleClose = () => {
    toggle()
  }

  return (
    <>
      <Card>
        <Dialog
          fullWidth
          open={open}
          maxWidth='md'
          scroll='body'
          onClose={handleClose}
          TransitionComponent={Transition}
          onBackdropClick={handleClose}
          sx={{ '& .MuiDialog-paper': { overflow: 'visible' } }}
        >
            <DialogContent
              sx={{
                pb: theme => `${theme.spacing(8)} !important`,
                px: theme => [`${theme.spacing(5)} !important`, `${theme.spacing(15)} !important`],
                pt: theme => [`${theme.spacing(8)} !important`, `${theme.spacing(12.5)} !important`]
              }}
            >
              <CustomCloseButton onClick={handleClose}>
                <Icon icon='tabler:x' fontSize='1.25rem' />
              </CustomCloseButton>
              {children}
            </DialogContent>
            <DialogActions
              sx={{
                justifyContent: 'center',
                px: theme => [`${theme.spacing(5)} !important`, `${theme.spacing(15)} !important`],
                pb: theme => [`${theme.spacing(8)} !important`, `${theme.spacing(12.5)} !important`]
              }}
            >
              <Button variant='contained' type='submit' sx={{ mr: 1 }} onClick={(e) => {
                handleCreate(e)
                handleClose()
              }}>
                Submit
              </Button>
              <Button variant='tonal' color='secondary' onClick={handleClose}>
                Discard
              </Button>
            </DialogActions>
        </Dialog>
      </Card>
    </>
  )
}

export default AddField
