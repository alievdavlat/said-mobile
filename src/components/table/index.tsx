// ** React Imports
import React, { useState } from 'react'

// ** Next Imports

// ** MUI Imports
import Card from '@mui/material/Card'
import Grid from '@mui/material/Grid'
import Divider from '@mui/material/Divider'
import CardHeader from '@mui/material/CardHeader'

import { DataGrid, GridColDef } from '@mui/x-data-grid'

// ** Custom Table Components Imports
import TableHeader from './list/TableHeader'
import AddField from './list/AddDrawer'
import { Box } from '@mui/material'



interface ITableProps {
  columns: GridColDef[]
  addBtnTitle: string
  tableTitle: string
  children: React.ReactNode
  rows: any[]
  handleCreate: (e: any) => any
  search: string
  setSearch: (search: string) => void
  handleGet: (e: any) => void
  loading: boolean
  page: number
  setPage: (page: number) => void
  pageSize: number
  total: number
}

const UserList: React.FC<ITableProps> = ({
  addBtnTitle,
  children,
  tableTitle,
  rows: tableRows,
  handleCreate,
  search,
  setSearch,
  handleGet,
  columns,
  loading,
  page,
  setPage,
  pageSize,
  total
}) => {
  // ** State
  const [addBrand, setAddBrand] = useState<boolean>(false)

 

  // functions
  const handleFilter = (val: string) => {
    setSearch(val)
  }

  const toggleAddBrand = () => setAddBrand(!addBrand)

  return (
    <Grid container spacing={6.5}>
      <Grid item xs={12}>
        <Card>
          <CardHeader title={tableTitle} />

          <Divider sx={{ m: '20px 0px !important' }} />
          <TableHeader
            value={search}
            handleFilter={handleFilter}
            toggle={toggleAddBrand}
            addBtnTitle={addBtnTitle}
            handleGet={handleGet}
          />
          <DataGrid
            autoHeight
            rowHeight={62}
            rows={tableRows || []}
            columns={columns || []}
            disableRowSelectionOnClick
            loading={loading}
            hideFooter
          />

          <Box sx={{ m: 6, display: 'flex', justifyContent: 'flex-end' }}>
          </Box>
        </Card>
      </Grid>

      <AddField title='add' open={addBrand} toggle={toggleAddBrand} handleCreate={handleCreate}>
        {children}
      </AddField>
    </Grid>
  )
}

export default UserList
