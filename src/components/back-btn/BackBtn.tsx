import { Button } from '@mui/material'
import Link from 'next/link'
import React from 'react'
import Translations from '../translations'
import Icon from 'src/@core/components/icon'

type Props = {
  href:string
  btnText?:string
  showIcon?:boolean
  icon?:string
}

const BackBtn = (props: Props) => {
  const {href, btnText,showIcon, icon} = props
  
  return (
    <Link href={href}>
      <Button>
        {showIcon && <Icon fontSize={'1.5rem'} icon={icon ? icon : 'iconamoon:arrow-left-2-thin'}/>}
        <Translations text={btnText ? btnText : 'Back'}/>
      </Button>
    </Link>
  )
}

export default BackBtn