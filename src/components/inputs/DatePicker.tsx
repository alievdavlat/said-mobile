import { forwardRef, useState, useEffect } from 'react';
import DatePicker, { ReactDatePickerProps, registerLocale } from 'react-datepicker';
import { useTheme } from '@mui/material/styles';
import CustomTextField from 'src/@core/components/mui/text-field';
import DatePickerWrapper from 'src/@core/styles/libs/react-datepicker';
import { Locale } from 'date-fns';
import uz from 'date-fns/locale/uz';
import ru from 'date-fns/locale/ru';
import en from 'date-fns/locale/en-US';
import { useTranslation } from 'react-i18next';
import 'react-datepicker/dist/react-datepicker.css';

interface PickerProps {
  label: string;
  readOnly?: boolean;
}

interface DatePickerInputProps extends ReactDatePickerProps {
  label: string;
  readOnly?: boolean;
  language: 'uz' | 'ru' | 'en' | string;
  dateFormat?: string;
  setValue: (value: any) => void;
  value: any;
}

const PickersComponent = forwardRef(({ ...props }: PickerProps, ref) => {
  const { label, readOnly } = props;

  return (
    <CustomTextField
      {...props}
      inputRef={ref}
      label={label || ''}
      {...(readOnly && { inputProps: { readOnly: true } })}
    />
  );
});

const DatePickerInput = (props: DatePickerInputProps) => {
  const theme = useTheme();
  const { i18n, t } = useTranslation();

  const { direction } = theme;
  const popperPlacement: ReactDatePickerProps['popperPlacement'] = direction === 'ltr' ? 'bottom-start' : 'bottom-end';

  const {
    label,
    setValue,
    readOnly,
    language,
    dateFormat,
    value,
    isClearable,
    showYearDropdown,
    showMonthDropdown,
    shouldCloseOnSelect,
    id,
    openToDate,
  } = props;

  const langObj: { [key: string]: Locale } = { uz, ru, en };

  const selectedLanguage = language || i18n.language;
  registerLocale(selectedLanguage, langObj[selectedLanguage]);

  const [selectedDate, setSelectedDate] = useState<Date | null>(null);

  useEffect(() => {
    if (value) {
      setSelectedDate(new Date(value));
    }
  }, [value]);

  const handleChange = (date: Date | null) => {
    if (date) {
      setSelectedDate(date);
      setValue(date.toISOString().split('T')[0]); // Sana formatini to'g'ri belgilang: 'yyyy-MM-dd'
    } else {
      setSelectedDate(null);
      setValue(null);
    }
  };

  return (
    <DatePickerWrapper>
      <DatePicker
        showIcon
        showYearDropdown={showYearDropdown ?? true}
        showMonthDropdown={showMonthDropdown ?? true}
        shouldCloseOnSelect={shouldCloseOnSelect ?? true}
        isClearable={isClearable ?? false}
        selected={selectedDate}
        popperPlacement={popperPlacement}
        id={id}
        dateFormat={dateFormat || 'yyyy-MM-dd'}
        openToDate={openToDate}
        onChange={handleChange} // HandleChange funksiyasini onChange xususiyati sifatida uzating
        locale={selectedLanguage}
        customInput={<PickersComponent label={t(label)} readOnly={readOnly ?? false} />}
      />
    </DatePickerWrapper>
  );
};

export default DatePickerInput;
