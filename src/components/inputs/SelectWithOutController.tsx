import { MenuItem } from '@mui/material'
import React from 'react'
import { useTranslation } from 'react-i18next'
import CustomTextField from 'src/@core/components/mui/text-field'

type Props = {
  value: any
  setValue: (e: any) => void
  data: any
  label: string
  getLabel: (data: any) => string
  getvalue: (data: any) => string
}

const SelectWithOutController = (props: Props) => {
  const { t } = useTranslation()
  
  return (
    <CustomTextField
      select
      fullWidth
      defaultValue=''
      label={t(props.label)}
      id={'select-controlled'}
      SelectProps={{ value: props.value, onChange: e => props.setValue(e.target.value) }}
    >
      {props.data?.length &&
        props?.data?.map((el: any) => (
          <MenuItem key={!!props.getvalue ?  props.getvalue(el) : el} value={!!props.getvalue ? props.getvalue(el) : el}>
            {!!props.getLabel ? props.getLabel(el) : el}
          </MenuItem>
        ))}
    </CustomTextField>
  )
}

export default SelectWithOutController
