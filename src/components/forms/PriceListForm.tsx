import { Grid,  } from '@mui/material'
import React from 'react'
import Translations from 'src/layouts/components/Translations'
import { LoadingButton } from '@mui/lab'
import Form from '../form'
import * as yup from 'yup'
import useQueryParams from 'src/hooks/useQueryParams'
import ControllerInput from '../with-controller/ControllerInput'
import AsyncAutoComplate from '../with-controller/auto-complate/AsyncAutoComplate'
import AsyncControllerSelect from '../with-controller/AsyncControllerSelect'

const validationSchema = yup.object().shape<any>({
  perpercentage: yup.number().required('Toldirilishi Shart Bolgan Maydon'),
  product: yup.number().required('Toldirilishi Shart Bolgan Maydon'),
  store: yup.number().required('Toldirilishi Shart Bolgan Maydon'),
 
})

function UserForm() {
  const query = useQueryParams()

  const initialValues = {
    perpercentage: '',
    product: '',
    store: ''
  }

  return (
    <Form
      getUrl={query?.has('id') ? '/stock/pricelist-info/' + query?.get('id') : ''}
      url={query?.has('id') ? '/stock/pricelist-update' : '/stock/pricelist-create'}
      validationSchema={validationSchema}
      initialValues={initialValues}
    >
      {({ handleFinish, createInfo, form }) => {
        const {
          control,
          handleSubmit,
          formState: { errors }
        } = form

        return (
          <Grid container spacing={6} p={10}>
            <Grid item md={6} xs={12}>
              <ControllerInput
                control={control}
                error={errors.perpercentage}
                name={'perpercentage'}
                label={'perpercentage'}
                placeholder=''
                type='number'
              />
            </Grid>

            <Grid item md={6} xs={12}>
                <AsyncAutoComplate
                label='Store'
                name='store'
                url={'/stock/list'}
                getOptionLabel={data => data?.name}
                getValue={data => data?.id}
                control={control}
                error={errors.store}
                getData={data =>  data?.results}
                filterOption={(option, search) => option?.data?.name?.toLowerCase().includes(search.toLowerCase())}
                />
              </Grid>

              <Grid item md={6} xs={12}>
              <AsyncControllerSelect
                url='/product/list'
                control={control}
                error={errors.product}
                name={'product'}
                label={'Product'}
                keyWord='id'
                getLabel={label => label.name}
                placeholder=''
              />
            </Grid>

            <Grid item xs={12}>
              <LoadingButton
                variant='contained'
                loading={createInfo?.isLoading}
                fullWidth
                onClick={handleSubmit(async (data: any) => {
                  await handleFinish({ ...data })
                  query.removeMany('id', 'onAdd')
                })}
              >
                <Translations text='Submit' />
              </LoadingButton>
            </Grid>
          </Grid>
        )
      }}
    </Form>
  )
}

export default UserForm
