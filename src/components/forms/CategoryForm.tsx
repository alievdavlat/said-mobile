import { Grid } from '@mui/material'
import React from 'react'
import Translations from 'src/layouts/components/Translations'
import { LoadingButton } from '@mui/lab'
import * as yup from 'yup'
import useQueryParams from 'src/hooks/useQueryParams'
import { useRouter } from 'next/router'
import ControllerInput from '../with-controller/ControllerInput'
import AsyncControllerSelect from '../with-controller/AsyncControllerSelect'
import Form from  '../form'

const validationSchema = yup.object().shape<any>({
  name: yup.string().required('This field is required'),
  parent: yup.string()
})

function CategoryForm() {
  const query = useQueryParams()
  const route = useRouter()
  const { id } = route.query


  const initialValues = {
    name: '',
    parent: ''
  }


  return (
    <Form
      getUrl={query?.has('id') ? '/product/category/info/' + query?.get('id') : ''}
      url={query?.has('id') ? '/product/category/update' : '/product/category/create'}
      validationSchema={validationSchema}
      initialValues={initialValues}
    >
      {({ handleFinish, createInfo, form }) => {
        const {
          control,
          handleSubmit,
          formState: { errors }
        } = form

        return (
          <Grid container spacing={6} p={10}>
            <Grid item sm={12}>
              <ControllerInput control={control} error={errors?.name} name={'name'} label={'Category Name'} placeholder='' />
            </Grid>

            <Grid item sm={12} xs={12} spacing={6}>
              <AsyncControllerSelect
                control={control}
                label='Parent'
                name='parent'
                url='/product/category/list'
                keyWord='id'
                error={errors.parent}
                getLabel={data => data?.name}      
                />
            </Grid>

            <Grid item sm={4}>
              <LoadingButton
                variant='contained'
                loading={createInfo?.isLoading}
                fullWidth
                onClick={handleSubmit(async (data: any) => {
                  await handleFinish({ ...data, id: Number(id) })
                  query.removeMany('id', 'onAdd')
 
                })}
              >
                <Translations text='Submit' />
              </LoadingButton>
            </Grid>
          </Grid>
        )
      }}
    </Form>
  )
}

export default CategoryForm
