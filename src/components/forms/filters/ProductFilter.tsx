import { Grid } from '@mui/material'
import React from 'react'
import { useTranslation } from 'react-i18next'
import GetContainer from 'src/components/get-container'
import SelectWithOutController from 'src/components/inputs/SelectWithOutController'

interface Props {
  category: string
  setCategory: (e: any) => void
  brand: string
  setBrand: (e: any) => void
  type: string
  setType: (e: any) => void
}

const ProductFilter = (props: Props) => {
  const { brand, category, setBrand, setCategory, setType, type } = props
  const { t } = useTranslation()

  return (
    <Grid container spacing={6}>
      <Grid item xs={6}>
        <GetContainer url='/product/brand/list'>
          {({ data }) => (
            <SelectWithOutController
              label={t('Brand')}
              data={data.results}
              getLabel={label => label.name}
              getvalue={value => value.id}
              setValue={setBrand}
              value={brand}
            />
          )}
        </GetContainer>
      </Grid>

      <Grid item xs={6}>
        <GetContainer url='/product/type/list'>
          {({ data }) => (
            <SelectWithOutController
              label={t('Type')}
              data={data.results}
              getLabel={label => label.name}
              getvalue={value => value.id}
              setValue={setType}
              value={type}
            />
          )}
        </GetContainer>
      </Grid>

      <Grid item xs={12}>
        <GetContainer url='/product/category/list'>
          {({ data }) => (
            <SelectWithOutController
              label={t('Category')}
              data={data.results}
              getLabel={label => label.name}
              getvalue={value => value.id}
              setValue={setCategory}
              value={category}
            />
          )}
        </GetContainer>
      </Grid>
    </Grid>
  )
}

export default ProductFilter
