//@ts-nocheck 
import { Box, Grid } from '@mui/material'
import React from 'react'
import { useTranslation } from 'react-i18next'
import GetContainer from 'src/components/get-container'
import DatePickerInput from 'src/components/inputs/DatePicker'
import SelectWithOutController from 'src/components/inputs/SelectWithOutController'

interface CargoProductsFilterProps {
  startdate: string
  setStartDate: (date: string) => void
  enddate: string
  setEndDate: (date: string) => void
  product: string
  setProduct: (e: any) => void
  brand: string
  setBrand: (e: any) => void
  type: string
  setType: (e: any) => void
}

const CargoProductsFilter = (props: CargoProductsFilterProps) => {
  const { startdate, enddate, setEndDate, setStartDate, product, setProduct, brand, setBrand, setType, type } = props

  const { t } = useTranslation()

  return (
    <Box sx={{ p: 30, pt: 0 }}>
      <Grid container spacing={6}>
        <Grid item xs={6}>
          <DatePickerInput
            label='Start Date'
            language='uz'
            setValue={setStartDate}
            value={startdate}
            dateFormat='yyyy-MM-dd' // Sana formatini to'g'ri belgilang
            shouldCloseOnSelect
            showMonthDropdown
          />
        </Grid>

        <Grid item xs={6}>
          <DatePickerInput
            label='End Date'
            language='uz'
            setValue={setEndDate}
            value={enddate}
            dateFormat='yyyy-MM-dd' // Update the date format here
            shouldCloseOnSelect
            showMonthDropdown
          />
        </Grid>

        <Grid item xs={6}>
          <GetContainer url='/cargo/product-list'>
            {({ data }) => (
              <SelectWithOutController
                label={t('Product')}
                data={data.results}
                getLabel={label => label.product.name}
                getvalue={value => value.product.id}
                setValue={setProduct}
                value={product}
              />
            )}
          </GetContainer>
        </Grid>

        <Grid item xs={6}>
          <GetContainer url='/product/brand/list'>
            {({ data }) => (
              <SelectWithOutController
                label={t('Brand')}
                data={data.results}
                getLabel={label => label.name}
                getvalue={value => value.id}
                setValue={setBrand}
                value={brand}
              />
            )}
          </GetContainer>
        </Grid>

        <Grid item xs={12}>
          <GetContainer url='/product/type/list'>
            {({ data }) => (
              <SelectWithOutController
                label={t('Type')}
                data={data.results}
                getLabel={label => label.name}
                getvalue={value => value.id}
                setValue={setType}
                value={type}
              />
            )}
          </GetContainer>
        </Grid>
      </Grid>
    </Box>
  )
}

export default CargoProductsFilter
