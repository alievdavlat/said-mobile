import { Button, Grid } from '@mui/material'
import React from 'react'
import { useTranslation } from 'react-i18next'
import SelectWithOutController from 'src/components/inputs/SelectWithOutController'

interface Props {
  sklad: string | boolean
  setSklad: (e: any) => void
  sell: boolean | string
  setSell: (e: any) => void
  part: boolean | string
  setPart: (e: any) => void
}

const StockFilter = (props: Props) => {
  const { part, sell, setPart, setSell, setSklad, sklad } = props
  const { t } = useTranslation()
  
 
  const selectdata:  any[] =  [
    {
      id: 1, 
      name: t('Yes'),
      value: true,
    },
    {
      id: 2, 
      name: t('No'),
      value: false,
    },
    {
      id: 3, 
      name: t('All'),
      value: 0,
    },
    
  ]

  const handleReset = () => {
    setPart(0)
    setSell(0)
    setSklad(0)
  }

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <SelectWithOutController
          label={t('Sell')}
          data={selectdata}
          getLabel={label => label?.name}
          getvalue={data => data?.value}
          setValue={setSell}
          value={sell}
        />
      </Grid>

      <Grid item xs={12}>
        <SelectWithOutController
          label={t('Part')}
          data={selectdata}
          getLabel={label => label?.name}
          getvalue={data => data?.value}
          setValue={setPart}
          value={part}
        />
      </Grid>

      <Grid item xs={12}>
        <SelectWithOutController
          label={t('Sklad')}
          data={selectdata}
          getLabel={label => label?.name}
          getvalue={data => data?.value}
          setValue={setSklad}
          value={sklad}
        />
      </Grid>

      <Grid item  md={6} xs={12}>
        <Button variant='outlined' onClick={handleReset}>
        {t('Reset')}
        </Button>
      </Grid>
      
    </Grid>
  )
}

export default StockFilter
