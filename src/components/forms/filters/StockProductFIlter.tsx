import { Box, Grid } from '@mui/material'
import React from 'react'
import { useTranslation } from 'react-i18next'
import GetContainer from 'src/components/get-container'
import DatePickerInput from 'src/components/inputs/DatePicker'
import SelectWithOutController from 'src/components/inputs/SelectWithOutController'

interface StockProductsFilterProps {
  startdate: string
  setStartDate: (date: string) => void
  enddate: string
  setEndDate: (date: string) => void
  brand: string
  setBrand: (e: any) => void
  type: string
  setType: (e: any) => void
  category:string,
  setCategory: (e: any) => void
  region: string
  setRegion: (e: any) => void
}

const StockProductsFilter = (props: StockProductsFilterProps) => {
  const { startdate, enddate, setEndDate, setStartDate, brand, setBrand, setType, type, category, region, setCategory, setRegion } = props

  const { t } = useTranslation()
  
  return (
    <Box sx={{ p: 30, pt: 0 }}>
      <Grid container spacing={6}>
        <Grid item xs={6}>
          <DatePickerInput
            label='Start Date'
            language='uz'
            setValue={setStartDate}
            value={startdate}
            dateFormat='yyyy-MM-dd' // Update the date format here
            shouldCloseOnSelect
            showMonthDropdown
            onChange={() => {}}
          />
        </Grid>
        <Grid item xs={6}>
          <DatePickerInput
            label='End Date'
            language='uz'
            setValue={setEndDate}
            value={enddate}
            dateFormat='yyyy-MM-dd' // Update the date format here
            shouldCloseOnSelect
            showMonthDropdown
            onChange={() => {}}
          />
        </Grid>


        <Grid item md={6} xs={12}>
          <GetContainer url='/product/brand/list'>
            {({ data }) => (
              <SelectWithOutController
                label={t('Brand')}
                data={data.results}
                getLabel={label => label.name}
                getvalue={value => value.id}
                setValue={setBrand}
                value={brand}
              />
            )}
          </GetContainer>
        </Grid>
            
        <Grid item md={6} xs={12}>
          <GetContainer url='/product/category/list'>
            {({ data }) => (
              <SelectWithOutController
                label={t('Category')}
                data={data.results}
                getLabel={label => label.name}
                getvalue={value => value.id}
                setValue={setCategory}
                value={category}
              />
            )}
          </GetContainer>
        </Grid>

        
        <Grid item md={6} xs={12}>
          <GetContainer url='/product/region/list'>
            {({ data }) => (
              <SelectWithOutController
                label={t('Region')}
                data={data.results}
                getLabel={label => label.name}
                getvalue={value => value.id}
                setValue={setRegion}
                value={region}
              />
            )}
          </GetContainer>
        </Grid>


        <Grid item md={6} xs={12}>
          <GetContainer url='/product/type/list'>
            {({ data }) => (
              <SelectWithOutController
                label={t('Type')}
                data={data.results}
                getLabel={label => label.name}
                getvalue={value => value.id}
                setValue={setType}
                value={type}
              />
            )}
          </GetContainer>
        </Grid>
      </Grid>
    </Box>
  )
}

export default StockProductsFilter
