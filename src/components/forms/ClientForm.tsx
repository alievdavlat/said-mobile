import { Grid } from '@mui/material'
import React from 'react'
import Translations from 'src/layouts/components/Translations'
import { LoadingButton } from '@mui/lab'
import Form from '../form'
import * as yup from 'yup'
import useQueryParams from 'src/hooks/useQueryParams'
import ControllerInput from '../with-controller/ControllerInput'
import ControllerMask from '../with-controller/InputMask'
import ControllerSelect from '../with-controller/ControllerSelect'

const validationSchema = yup.object().shape<any>({
  first_name: yup.string().required('Toldirilishi Shart Bolgan Maydon'),
  last_name: yup.string().required('Toldirilishi Shart Bolgan Maydon'),
  phone_number: yup.string().required('Toldirilishi Shart Bolgan Maydon'),
  note: yup.string().required('Toldirilishi Shart Bolgan Maydon'),
  address: yup.string().required('Toldirilishi Shart Bolgan Maydon'),
  type: yup.string().required('Toldirilishi Shart Bolgan Maydon')
})

const supplierData = [
  {
    id:1,
    name:'Client',
    value:1
  },
  {
    id:2,
    name:'Supplier',
    value:2
  },
  {
    id:3,
    name:'Investor',
    value:3
  },
]


function UserForm() {
  const query = useQueryParams()

  const initialValues = {
    first_name: '',
    last_name: '',
    phone_number: '',
    note: '',
    address: '',
    type:''
  }

  return (
    <Form
      getUrl={query?.has('id') ? '/client/info/' + query?.get('id') : ''}
      url={query?.has('id') ? '/client/update' : '/client/create'}
      validationSchema={validationSchema}
      initialValues={initialValues}
    >
      {({ handleFinish, createInfo, form }) => {
        const {
          control,
          handleSubmit,
          formState: { errors }
        } = form

        return (
          <Grid container spacing={6} p={10}>
            <Grid item md={6} xs={12}>
              <ControllerInput
                control={control}
                error={errors.first_name}
                name={'first_name'}
                label={'First Name'}
                placeholder=''
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <ControllerInput
                control={control}
                error={errors.last_name}
                name={'last_name'}
                label={'Last Name'}
                placeholder=''
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <ControllerInput control={control} error={errors.note} name={'note'} label={'Note'} placeholder='' />
            </Grid>

            <Grid item md={6} xs={12}>
              <ControllerInput
                control={control}
                name={'address'}
                label={'Address'}
                error={errors.address}
                placeholder=''
              />
            </Grid>

        

            <Grid item md={12} xs={12}>
            <ControllerSelect
              control={control}
              data={supplierData}
              getLabel={data => data?.name}
              label='Type'
              name='type'
              keyWord='value'
              placeholder=''
            />
            </Grid>
            <Grid item md={6} xs={12}>
              <ControllerMask
                control={control}
                error={errors.phone_number}
                name={'phone_number'}
                label={'Phone Number'}
                placeholder=''
              />
            </Grid>
            <Grid item md={12}>
              <LoadingButton
                variant='contained'
                loading={createInfo?.isLoading}
                fullWidth
                onClick={handleSubmit(async (data: any) => {
                  await handleFinish({ ...data })
                  query.removeMany('id', 'onAdd')
                })}
              >
                <Translations text='Submit' />
              </LoadingButton>
            </Grid>
          </Grid>
        )
      }}
    </Form>
  )
}

export default UserForm
