import React, { SyntheticEvent, useState } from 'react'
import { Accordion, AccordionDetails, AccordionSummary, Button, Grid, Typography } from '@mui/material'
import { Control, FieldErrors, useFieldArray } from 'react-hook-form'
import Icon from 'src/@core/components/icon'
import Translations from 'src/layouts/components/Translations'
import ControllerInput from '../../with-controller/ControllerInput'
import AsyncControllerSelect from '../../with-controller/AsyncControllerSelect'

type Props = {
  control: Control
  errors: FieldErrors
}
interface CodesProps {
  control: Control
  index: number
}


const CargoProductArrayForm = (props: Props) => {
  const { control } = props
  const [expanded, setExpanded] = useState<number | false>(0)

  const handleChange = (panel: number) => (event: SyntheticEvent, isExpanded: boolean) => {
    setExpanded(isExpanded ? panel : false)
  }

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'cargo_products'
  })



  const handleAddNewField = () => {
    append({
      product: 0,
      quantity: '',
      price: '',
    })
  }



  const handleRemove = (idx: any) => {
    remove(idx)
  }

  

  return (
    <>
     {fields.map((item, index:number) => (
        <Accordion key={item.id} expanded={expanded === index} onChange={handleChange(index)}>
          <AccordionSummary
            id='controlled-panel-header-1'
            aria-controls='controlled-panel-content-1'
            expandIcon={<Icon fontSize='1.25rem' icon='tabler:chevron-down' />}
          >
            <Typography sx={{ my: 4 }}>
              <Translations text='Cargo' /> <Translations text='Products' /> {index + 1}
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Grid container spacing={6} mt={6} key={item.id}>
              <Grid item xs={12} sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                <Button color='error' variant='outlined' sx={{ mt: 3, ml: 2 }} onClick={() => handleRemove(index)}>
                  <Icon fontSize='1.5rem' icon='tabler:trash' /> {'  '}
                </Button>
              </Grid>

              <Grid item xs={12}>
                <AsyncControllerSelect
                  control={control}
                  label='Product'
                  name={`cargo_products.${index}.product`}
                  url='/product/list'
                  keyWord='id'
                  getLabel={(data) => data?.name}
                  placeholder=''
                />
              </Grid>

              <Grid item xs={6}>
                <ControllerInput
                  control={control}
                  name={`cargo_products.${index}.quantity`}
                  type='number'
                  label={'Product quantity'}
                  placeholder=''
                />
              </Grid>
              <Grid item xs={6}>
                <ControllerInput
                  control={control}
                  name={`cargo_products.${index}.price`}
                  type='number'
                  label={'Price'}
                  placeholder=''
                />
              </Grid>


            </Grid>
          </AccordionDetails>
        </Accordion>
      ))}

      <Button onClick={handleAddNewField} variant='text' sx={{ mt: 12 }}>
        <Icon fontSize='1.5rem' icon='tabler:plus' />
        <Translations text='Add Product' />
      </Button>
    </>
  )
}

export default CargoProductArrayForm
