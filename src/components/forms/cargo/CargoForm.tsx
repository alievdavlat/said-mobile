import { Button, Grid } from '@mui/material'
import React from 'react'
import Translations from 'src/layouts/components/Translations'
import { LoadingButton } from '@mui/lab'
import Form from '../../form'
import * as yup from 'yup'
import useQueryParams from 'src/hooks/useQueryParams'
import { useRouter } from 'next/router'
import ControllerInput from '../../with-controller/ControllerInput'
import Icon from 'src/@core/components/icon'
import ControllerFile from '../../with-controller/ControllerFile'
import dynamic from 'next/dynamic'
import AsyncControllerSelect from 'src/components/with-controller/AsyncControllerSelect'

const CargoProductArrayForm = dynamic(() => import('./CargoProductArrayForm'), { ssr: false })

const validationSchema = yup.object().shape<any>({
  invoice: yup.string().required('This field is required'),
  file: yup.string().optional(),
  supplier: yup.string(),
  cargo_products: yup.array().of(
    yup.object().shape({
      product: yup.number().required('Product is required'),
      quantity: yup.string().required('Quantity is required'),
      price: yup.string().required('Price is required')
    })
  )
})

function CargoForm() {
  //@ts-nocheck
  const [isSetCargoProducts, setIsCargoProducts] = React.useState(false)

  const query = useQueryParams()
  const route = useRouter()
  const { id } = route.query

  const initialValues = {
    invoice: '',
    file: '',
    supplier: '',
    cargo_products: [
      {
        product: 0,
        quantity: '',
        price: ''
      }
    ]
  }

  return (
    <Form
      getUrl={query?.has('id') ? '/cargo/info-id/' + query?.get('id') : ''}
      url={ query?.has('id') ? `/cargo/edit` : '/cargo/create'}
      validationSchema={validationSchema}
      initialValues={initialValues}
    >
      {({ handleFinish, createInfo, form }) => {
        const {
          control,
          formState: { errors },
          handleSubmit
        } = form

        return (
          <Grid container spacing={6} p={10}>
            <Grid item md={12} xs={12} >
              <ControllerInput
                control={control}
                error={errors.invoice}
                name={'invoice'}
                label={'Invoice'}
                placeholder='invoice..'
              />
            </Grid>

            
            <Grid item md={12} xs={12}>
              <AsyncControllerSelect
                control={control}
                label='Supplier'
                name='supplier'
                placeholder=''
                getLabel={(data) => data.first_name + ' ' + data?.last_name}
                keyWord='id'
                url='/client/list?type=2'
              />
            </Grid>

            <Grid item md={12} xs={12}>
              {!isSetCargoProducts && !query.has('id') ? (
                <Button
                  onClick={() => {
                    query.set('onAdd', 'true')
                    setIsCargoProducts(!isSetCargoProducts)
                  }}
                  variant='text'
                >
                  <Icon fontSize='1.5rem' icon='tabler:plus' />
                  <Translations text='Add Product' />
                </Button>
              ) : (
                <>
                  <CargoProductArrayForm control={control} errors={errors} />
                </>
              )}
            </Grid>

            {query.has('id') ? null : (
              <Grid item sm={12} xs={12} >
                <ControllerFile name='file' control={control} error={errors.file} label='Product Image' />
              </Grid>
            )}

            <Grid item sm={12}>
              <LoadingButton
                variant='contained'
                loading={createInfo?.isLoading}
                fullWidth
                onClick={handleSubmit(async (data: any) => {
                  await handleFinish({ ...data })
                  query.removeMany('id', 'onAdd')
                })}
              >
                <Translations text='Submit' />
              </LoadingButton>
            </Grid>
          </Grid>
        )
      }}
    </Form>
  )
}

export default CargoForm
