import {  Grid } from '@mui/material'
import React from 'react'
import Translations from 'src/layouts/components/Translations'
import { LoadingButton } from '@mui/lab'
import Form from '../form'
import * as yup from 'yup'
import useQueryParams from 'src/hooks/useQueryParams'
import ControllerInput from '../with-controller/ControllerInput'
import AsyncControllerSelect from 'src/components/with-controller/AsyncControllerSelect'
import ControllerTextArea from '../with-controller/ControllerTextArea'
import ControllerSelect from '../with-controller/ControllerSelect'
import AsyncAutoComplate from '../with-controller/auto-complate/AsyncAutoComplate'

const typeData = [
  {
    id:1,
    name:"DEFAULT"
  },
  {
    id:2,
    name:"NUMBER"
  },
  {
    id:3,
    name:"TOTAL_PER"
  },
]

const validationSchema = yup.object().shape<any>({
  name: yup.string().notRequired(),
  price: yup.string().notRequired(),
  type: yup.string().notRequired(),
  description: yup.string().notRequired(),
  client:yup.string().notRequired(),
  cargo:yup.string().notRequired(),
  products: yup.array()
})

function CostForm() {

  const query = useQueryParams()

  const initialValues = {
    name: '',
    price: '',
    type: '',
    description: '',
    client:'',
    cargo:'',
    products: []
  }



  return (
    <Form
      getUrl={query?.has('id') ? '/cargo/cost-info/' + query?.get('id') : ''}
      url={query?.has('id') ? '/cargo/cost-edit' : '/cargo/cost-create'}
      validationSchema={validationSchema}
      initialValues={initialValues}
    >
      {({ handleFinish, createInfo, form }) => {
        const {
          control,
          formState: { errors },
          handleSubmit,
          watch
        } = form

        return (
          <Grid container spacing={6} p={10}>
               <Grid item md={12} xs={12} >
              <ControllerInput
                control={control}
                error={errors.name}
                name={'name'}
                label={'Name'}
                placeholder='name..'
              />
            </Grid>

            <Grid item md={6} xs={12} >
              <ControllerInput
                control={control}
                name={'price'}
                type='number'
                label={'price'}
                placeholder='900'
                error={errors.price}
              />
            </Grid>

            <Grid item md={6} xs={12} >
              <ControllerSelect
                control={control}
                data={typeData}
                getLabel={data => data.name}
                label='Type'
                name='type'
                error={errors.type}
                keyWord='id'
                placeholder='Общий'
              />
            </Grid>


            <Grid item md={4} xs={12}>
              <AsyncControllerSelect
                control={control}
                label='Contractor'
                name='client'
                placeholder=''
                getLabel={data => data.first_name + ' ' + data?.last_name}
                keyWord='id'
                url='/client/list?type=2'
                error={errors.client}
              />
            </Grid>

            <Grid item md={4} xs={12}>
              <AsyncControllerSelect
                control={control}
                label='Invoice'
                name='cargo'
                placeholder=''
                getLabel={data => data.invoice}
                keyWord='id'
                url='/cargo/list'
                error={errors.cargo}
              />
            </Grid>


            <Grid item md={4} xs={12}>
              <AsyncAutoComplate
                control={control}
                label='Products'
                name='products'
                url='/cargo/product-list'
                getData={data => data?.results}
                getOptionLabel={data => data?.product?.name}
                getValue={data => data?.id}
                filterOption={(option, search) => option?.data?.product.name?.toLowerCase().includes(search.toLowerCase())}
                error={errors.products}
                multiple
                params={{
                  cargo: watch('cargo')
                }}
              />
            </Grid>

            <Grid item md={12} xs={12} >
              <ControllerTextArea
                control={control}
                name={'description'}
                label={'Description'}
                placeholder='.....'
                error={errors.description}
              />
            </Grid>

            <Grid item sm={12}>
              <LoadingButton
                variant='contained'
                loading={createInfo?.isLoading}
                fullWidth
                onClick={handleSubmit(async (data: any) => {

                  await handleFinish({ ...data })
                  query.removeMany('id', 'onAdd')
                })}
              >
                <Translations text='Submit' />
              </LoadingButton>
            </Grid>
          </Grid>
        )
      }}
    </Form>
  )
}

export default CostForm
