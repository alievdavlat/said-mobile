import { Grid } from '@mui/material'
import React from 'react'
import Translations from 'src/layouts/components/Translations'
import { LoadingButton } from '@mui/lab'
import Form from '../form'
import * as yup from 'yup'
import { useRouter } from 'next/router'
import ControllerInput from '../with-controller/ControllerInput'
import AsyncControllerSelect from '../with-controller/AsyncControllerSelect'
import { useAuth } from 'src/hooks/useAuth'
import ControllerTextArea from '../with-controller/ControllerTextArea'
import useQueryParams from 'src/hooks/useQueryParams'
import toast from 'react-hot-toast'
import { useTranslation } from 'react-i18next'
import dynamic from 'next/dynamic'


const Loader = dynamic(() => import('../loader'), {ssr:false})

const validationSchema = yup.object().shape<any>({
  debit: yup.number().notRequired(),
  credit: yup.number().required(),
  description: yup.string(),
  store: yup.number()
})

function CreditForm() {

  const auth = useAuth()
  const route = useRouter()
  const { id } = route.query
  const query = useQueryParams()
  const { t } = useTranslation()

  const initialValues = {
    debit: 0,
    credit: 0,
    description: '',
    store:auth?.user?.store
  }


  return (
    <Form
      getUrl={''}
      url={'/stock/balance/create'}
      validationSchema={validationSchema}
      initialValues={initialValues}
    >
      {({ handleFinish, createInfo, form , isError, isLoading, isSuccess}) => {
       
       if (isSuccess && !isLoading) {
        toast.success(t('Successfully Commpleted'))
        query.remove('credit')
      }

      if (isError) {
        toast.success(t('Something Went Wrong'))
        query.remove('credit')
      }
      
       const {
          control,
          handleSubmit,
          formState: { errors }
        } = form

        return (
          <>
          {
            isLoading ? 
              <Grid container spacing={10} sx={{width:"50vw", height:"50vh"}}>
                <Grid item xs={12}>
                  <Loader/>
                </Grid>
              </Grid> :
            <Grid container spacing={6} p={10}>
            <Grid item xs={6}>
              <ControllerInput control={control} error={errors.debit} name={'debit'} type='number' label={'Debit'} placeholder='' disabled />
            </Grid>

            <Grid item xs={6}>
              <ControllerInput control={control} error={errors.credit}  name={'credit'} type='number' label={'Credit'} placeholder='' />
            </Grid>

            <Grid item xs={12}>
              <ControllerTextArea
              control={control}
              name={'description'}
              label={'Description'}
              placeholder='Description'
              error={errors.description} 
              />
            </Grid>


            {
              auth?.user?.is_superuser && 
            <Grid item xs={12}>
            <AsyncControllerSelect
            control={control}
            label='Stock'
            name='store'
            url='/stock/list'
            keyWord='id'
            getLabel={data => data?.name}
            error={errors.store}
            />
          </Grid>
            }

            


            <Grid item xs={12}>
              <LoadingButton
                variant='contained'
                loading={createInfo?.isLoading}
                fullWidth
                onClick={handleSubmit(async (data: any) => {
                  await handleFinish({ ...data, id: Number(id) })
                  query.removeMany('id', 'onAdd')
                 })}
              >
                <Translations text='Submit' />
              </LoadingButton>
            </Grid>
          </Grid>
          }
          </>
        )
      }}
    </Form>
  )
}

export default CreditForm
