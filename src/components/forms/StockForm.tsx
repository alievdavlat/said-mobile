import { Grid, Typography } from '@mui/material'
import React from 'react'
import Translations from 'src/layouts/components/Translations'
import { LoadingButton } from '@mui/lab'
import Form from '../form'
import * as yup from 'yup'
import useQueryParams from 'src/hooks/useQueryParams'
import ControllerInput from '../with-controller/ControllerInput'
import ControllerRadio from '../with-controller/ControllerRadio'
import { client } from 'src/configs/req'
import AsyncAutoComplate from '../with-controller/auto-complate/AsyncAutoComplate'

const validationSchema = yup.object().shape<any>({
  name: yup.string().required('This field is required'),
  address: yup.string().required('This field is required'),
  part: yup.boolean(),
  sell: yup.boolean(),
  sklad: yup.boolean(),
  swope: yup.array()
})

function StockForm() {
  const query = useQueryParams()
  const [store, setStore] = React.useState([])


  const initialValues = {
    name: '',
    address: '',
    sell: false,
    sklad: false,
    part: false,
    swope: []
  }

  const handleGetSelectData = async () => {
    try {
      const stocks = await client('/stock/list')
      setStore(stocks.data.results)

      
    } catch (err) {
      console.log(err)
    }
  }

  React.useEffect(() => {
    handleGetSelectData()
  }, [])

  const transformData = (data: any) => {
    if (
      data.swope &&
      Array.isArray(data.swope) &&
      typeof data.swope[0] !== 'number' &&
      typeof data.swope[0] !== 'string'
    ) {
      data.swope = data.swope.map((item: any) => item.id)
    }

    return data
  }


  return (
    <Form
      getUrl={query?.has('id') ? '/stock/info/' + query?.get('id') : ''}
      url={query?.has('id') ? '/stock/update' : '/stock/create'}
      validationSchema={validationSchema}
      initialValues={initialValues}
    >
      {({ handleFinish, createInfo, form }) => {
        const { control, handleSubmit, formState: { errors } } = form

        return (
          <Grid container spacing={6} p={10}>
            <Grid item sm={6}>
              <ControllerInput control={control} error={errors.name} name={'name'} label={'Stock Name'} placeholder='' />
            </Grid>

            <Grid item sm={6}>
              <ControllerInput control={control} error={errors.address} label='Address' name='address' placeholder='' />
            </Grid>
            {store.length > 0 && (
              <Grid item xs={12}>
                <AsyncAutoComplate
                label='Swope'
                name='swope'
                url={'/stock/list'}
                getOptionLabel={data => data?.name}
                getValue={data => data?.id}
                multiple
                control={control}
                error={errors.swope}
                getData={data =>  data?.results}
                filterOption={(option, search) => option?.data?.name?.toLowerCase().includes(search.toLowerCase())}
                
                />
              </Grid>
            )}
            <Grid item sm={4} xs={12} spacing={6}>
              <Typography variant='h3' sx={{ my: 3 }}>
                <Translations text='Sklad' />
              </Typography>
              <ControllerRadio control={control} label='Sklad' name='sklad' />
            </Grid>
            <Grid item sm={4} xs={12} spacing={6}>
              <Typography variant='h3' sx={{ my: 3 }}>
                <Translations text='Sell' />
              </Typography>
              <ControllerRadio control={control} label='Sell' name='sell' />
            </Grid>
            <Grid item sm={4} xs={12} spacing={6}>
              <Typography variant='h3' sx={{ my: 3 }}>
                <Translations text='Part' />
              </Typography>
              <ControllerRadio control={control} label='Part' name='part' />
            </Grid>

            <Grid item sm={12}>
              <LoadingButton
                variant='contained'
                loading={createInfo?.isLoading}
                fullWidth
                onClick={handleSubmit(async (data: any) => {
                  
                  const transformedData = transformData(data)

                  await handleFinish({ ...transformedData})
                  query.removeMany('id', 'onAdd')
                 })}
              >
                <Translations text='Submit' />
              </LoadingButton>
            </Grid>
          </Grid>
        )
      }}
    </Form>
  )
}

export default StockForm
