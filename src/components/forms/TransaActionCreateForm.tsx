import { Grid } from '@mui/material'
import React  from 'react'
import Translations from 'src/layouts/components/Translations'
import { LoadingButton } from '@mui/lab'
import Form from '../form'
import * as yup from 'yup'
import { useRouter } from 'next/router'
import ControllerInput from '../with-controller/ControllerInput'
import AsyncControllerSelect from '../with-controller/AsyncControllerSelect'
import { useAuth } from 'src/hooks/useAuth'
import useQueryParams from 'src/hooks/useQueryParams'
import AdvancedSelect from '../with-controller/pro-select/AdvancedSelect'
import toast from 'react-hot-toast'
import { useTranslation } from 'react-i18next'
import { client } from 'src/configs/req'

const validationSchema = yup.object().shape<any>({
  price: yup.number().test(
    'maxDigitsBeforeDecimal',
    '8 xonadan ortiq raqamlar bo`lishi mumkin emas',
    (value) => {
      if (value === undefined || value === null) return true;
      const stringValue = value.toString();
      const [integerPart] = stringValue.split('.')
      
      return integerPart.length <= 8;
    }
  ).max(Number.MAX_SAFE_INTEGER).required(),
  from_store: yup.number(),
  to_store: yup.number().required(),
});


function TransActionCreateFrom() {
  const auth = useAuth()
  const route = useRouter()
  const { id } = route.query
  const query = useQueryParams()
  const { t } = useTranslation()


  const [swope, setSwope] = React.useState([])

  const initialValues = {
    price: '',
    from_store: auth.user?.store,
    to_store:0, 
  }

  
    const handleGetSelectData = async () => {
      try {
        const stocks = await client(`/stock/info/${auth.user?.store}`)
        setSwope(stocks?.data?.swope)
      } catch (err) {
        console.log(err)
      }
    }
  
    React.useEffect(() => {
      handleGetSelectData()
    }, [])
  

    

  return (
    <Form
      getUrl={''}
      url={'/stock/transaction-create'}
      validationSchema={validationSchema}
      initialValues={initialValues}
    >
      {({ handleFinish, createInfo, form, isError, isLoading, isSuccess }) => {
        const { control, handleSubmit, formState:{errors} } = form
        if (isSuccess && !isLoading) {
          toast.success(t('Successfully Commpleted'))
          query.remove('transaction')
        }
  
        if (isError) {
          toast.success(t('Something Went Wrong'))
          query.remove('transaction')
        }
        
        return (
          <>
          {
            <Grid container spacing={6} p={10}>
            <Grid item xs={12}>
            <ControllerInput control={control} error={errors.price} name={'price'} label={'Price'} type='number' placeholder='' />
            </Grid>

            {auth?.user?.is_superuser && (
              <Grid item xs={6}>
                <AsyncControllerSelect
                  control={control}
                  label='From Store'
                  name='from_store'
                  url='/stock/list'
                  keyWord='id'
                  getLabel={data => data?.name}
                  error={errors.from_store}
                />
              </Grid>
            )}
            {auth?.user?.is_superuser && (
              <Grid item xs={6}>
                <AsyncControllerSelect
                  control={control}
                  label='To Store'
                  name='to_store'
                  url='/stock/list'
                  keyWord='id'
                  getLabel={data => data?.name}
                  error={errors.to_store}
                />
              </Grid>
            )}

            {!auth?.user?.is_superuser && (
              <Grid item xs={12}>
                      <AdvancedSelect
                        control={control}
                        label='To Store'
                        name='to_store'
                        url={`/stock/list`}
                        keyWord='id'
                        getLabel={data => data.name}
                        getValue={data => data.id}
                        getData={data => data.results?.filter((i:any) =>  swope.some((s:any) => i.id === s.id))}
                        error={errors.to_store}
                      />
              </Grid>
            )}

            <Grid item xs={12}>
              <LoadingButton
                variant='contained'
                loading={createInfo?.isLoading}
                fullWidth
                onClick={handleSubmit(async (data: any) => {
                  const newData = { ...data, id: Number(id) }                  
                  await handleFinish(newData)
                  query.removeMany('id', 'onAdd')
                })}
              >

                <Translations text='Submit' />
              </LoadingButton>
            </Grid>
          </Grid>
          }
          </>
        )
      }}
    </Form>
  )
}

export default TransActionCreateFrom
