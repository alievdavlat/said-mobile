import { Grid } from '@mui/material'
import React from 'react'
import Translations from 'src/layouts/components/Translations'
import { LoadingButton } from '@mui/lab'
import Form from '../form'
import * as yup from 'yup'
import useQueryParams from 'src/hooks/useQueryParams'
import { useRouter } from 'next/router'
import ControllerInput from '../with-controller/ControllerInput'
import AsyncControllerSelect from '../with-controller/AsyncControllerSelect'

const validationSchema = yup.object().shape<any>({
  name: yup.string().required('This field is required'),
  brand: yup.string().required('This field is required'),
  category: yup.string().required('This field is required'),
  type: yup.string().required('This field is required')
})

function ProductForm() {
  const query = useQueryParams()
  const route = useRouter()
  const { id } = route.query

  const initialValues = {
    name: '',
    brand: '',
    category: '',
    type: ''
  }

  return (
    <Form
      getUrl={query?.has('id') ? '/product/info/' + query?.get('id') : ''}
      url={query?.has('id') ? '/product/update' : '/product/create'}
      validationSchema={validationSchema}
      initialValues={initialValues}
    >
      {({ handleFinish, createInfo, form }) => {
        const {
          control,
          formState: { errors },
          handleSubmit
        } = form

        return (
          <Grid container spacing={6} p={10}>
            <Grid item sm={12}>
              <ControllerInput control={control} error={errors.name} name={'name'} label={'Product Name'} placeholder='' />
            </Grid>

            <Grid item sm={4} xs={12} spacing={6}>
              <AsyncControllerSelect
                control={control}
                label='Brand'
                name='brand'
                url='/product/brand/list'
                keyWord='id'
                getLabel={data => data?.name}
                error={errors.brand}

              />
            </Grid>
            <Grid item sm={4} xs={12} spacing={6}>
              <AsyncControllerSelect
                control={control}
                label='Category'
                name='category'
                url='/product/category/list'
                keyWord='id'
                getLabel={data => data?.name}
                error={errors.category}

              />
            </Grid>
            <Grid item sm={4} xs={12} spacing={6}>
              <AsyncControllerSelect
                control={control}
                label='Type'
                name='type'
                url='/product/type/list'
                keyWord='id'
                getLabel={data => data?.name}
                error={errors.type}

              />
            </Grid>

            <Grid item sm={4}>
              <LoadingButton
                variant='contained'
                loading={createInfo?.isLoading}
                fullWidth
                onClick={handleSubmit(async (data: any) => {
                  await handleFinish({ ...data, id: Number(id) })
                  query.removeMany('id', 'onAdd')
                 })}
              >
                <Translations text='Submit' />
              </LoadingButton>
            </Grid>
          </Grid>
        )
      }}
    </Form>
  )
}

export default ProductForm
