import {  Grid, Typography } from '@mui/material'
import React from 'react'
import Translations from 'src/layouts/components/Translations'
import { LoadingButton } from '@mui/lab'
import Form from '../form'
import * as yup from 'yup'
import useQueryParams from 'src/hooks/useQueryParams'
import { useRouter } from 'next/router'
import ControllerInput from '../with-controller/ControllerInput'
import { API_URL, client } from 'src/configs/req'
import ControllerRadio from '../with-controller/ControllerRadio'
import toast from 'react-hot-toast'
import AsyncControllerSelect from '../with-controller/AsyncControllerSelect'
import AsyncAutoComplate from '../with-controller/auto-complate/AsyncAutoComplate'
import ControllerFile from '../with-controller/ControllerFile'

const validationSchema = yup.object().shape<any>({
  firstname: yup.string().required('Toldirilishi Shart Bolgan Maydon'),
  lastname: yup.string().required('Toldirilishi Shart Bolgan Maydon'),
  surname: yup.string().required('Toldirilishi Shart Bolgan Maydon'),
  password: yup.string().required('Toldirilishi Shart Bolgan Maydon'),
  login: yup.string().required('Toldirilishi Shart Bolgan Maydon'),
  role: yup
    .mixed()
    .required('Toldirilishi Shart Bolgan Maydon')
    .test(
      'role-type',
      'Role must be an object or a number',
      value => typeof value === 'object' || typeof value === 'number'
    ),
  permission: yup.array().required('Toldirilishi Shart Bolgan Maydon'),
  rent: yup.boolean().required('Toldirilishi Shart Bolgan Maydon'),
  rent_percentage: yup.number().required('Toldirilishi Shart Bolgan Maydon'),
  bonus: yup.boolean().required('Toldirilishi Shart Bolgan Maydon'),
  currency: yup.number().required('Toldirilishi Shart Bolgan Maydon'),
  photo: yup.string(),
  store: yup.number().required('Toldirilishi Shart Bolgan Maydon')
})

const validationSchema2 = yup.object().shape<any>({
  firstname: yup.string().required('Toldirilishi Shart Bolgan Maydon'),
  lastname: yup.string().required('Toldirilishi Shart Bolgan Maydon'),
  surname: yup.string().required('Toldirilishi Shart Bolgan Maydon'),
  role: yup
    .mixed()
    .required('Toldirilishi Shart Bolgan Maydon')
    .test(
      'role-type',
      'Role must be an object or a number',
      value => typeof value === 'object' || typeof value === 'number'
    ),
  permission: yup.array().required('Toldirilishi Shart Bolgan Maydon'),
  rent: yup.boolean().required('Toldirilishi Shart Bolgan Maydon'),
  rent_percentage: yup.number().required('Toldirilishi Shart Bolgan Maydon'),
  bonus: yup.boolean().required('Toldirilishi Shart Bolgan Maydon'),
  currency: yup.number().required('Toldirilishi Shart Bolgan Maydon'),
  photo: yup.string(),
  store: yup.number().required('Toldirilishi Shart Bolgan Maydon')
})

function UserForm() {
  const query = useQueryParams()
  const route = useRouter()
  const { id } = route.query
  const [files, setFiles] = React.useState([])
  const [photoUrl, setPhotoUrl] = React.useState('')

  const initialValues = {
    firstname: '',
    lastname: '',
    surname: '',
    password: '',
    login: '',
    role: '',
    permission: [],
    rent: true,
    rent_percentage: 1,
    bonus: true,
    currency: 0,
    photo: '',
    store: 0
  }

  const initialValues2 = {
    firstname: '',
    lastname: '',
    surname: '',
    role: '',
    permission: [],
    rent: true,
    rent_percentage: 1,
    bonus: true,
    currency: 0,
    store: 0
  }

  const handleUploadImage = async () => {
    const fm = new FormData()

    //@ts-ignore
    fm.append('file', files[0])

    try {
      const res = await client.post('/files/upload', fm, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      })

      if (res.status >= 200 && res.status < 300) {
        toast.success('Request successful')
        console.log(res.data)
        setPhotoUrl(res?.data?.file)
      } else {
        toast.error('somthing went error while upload image failed with status ' + res.status)
      }
    } catch (err: any) {
      toast.error(err)
    }
  }

  const transformData = (data: any) => {
    if (data.role && typeof data.role === 'object') {
      data.role = data.role.id
    }

    if (
      data.permission &&
      Array.isArray(data.permission) &&
      typeof data.permission[0] !== 'number' &&
      typeof data.permission[0] !== 'string'
    ) {
      data.permission = data.permission.map((item: any) => item.id)
    }

    return data
  }

  return (
    <Form
      getUrl={query?.has('id') ? '/user/info/' + query?.get('id') : ''}
      url={query?.has('id') ? '/user/update' : '/user/create'}
      validationSchema={query.has('id') ? validationSchema2 : validationSchema}
      initialValues={query.has('id') ? initialValues2 : initialValues}
    >
      {({ handleFinish, createInfo, form }) => {
        const {
          control,
          handleSubmit,
          formState: { errors }
        } = form
        
        return (
          <Grid container spacing={6} p={10}>
            <Grid item md={6} xs={12}>
              <ControllerInput
                control={control}
                error={errors.firstname}
                name={'firstname'}
                label={'First Name'}
                placeholder='first name..'
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <ControllerInput
                control={control}
                error={errors.lastname}
                name={'lastname'}
                label={'Last Name'}
                placeholder='last name..'
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <ControllerInput control={control} name={'surname'} label={'Surname'} placeholder='surname..' />
            </Grid>
            {query?.has('id') ? null : (
              <Grid item md={6} xs={12}>
                <ControllerInput
                  control={control}
                  error={errors.login}
                  name={'login'}
                  label={'Login'}
                  placeholder='login..'
                />
              </Grid>
            )}
            {query.has('id') ? null : (
              <Grid item md={6} xs={12}>
                <ControllerInput
                  control={control}
                  error={errors.password}
                  name={'password'}
                  label={'Password'}
                  placeholder='password..'
                />
              </Grid>
            )}

            <Grid item md={6} xs={12}>
              <AsyncAutoComplate
                name='role'
                label='Role'
                url='/user/role-list'
                control={control}
                error={errors?.role}
                getOptionLabel={data => data?.name}
                getValue={data => data?.id}
                filterOption={(option, search) => option?.data?.name?.toLowerCase().includes(search.toLowerCase())}
                getData={data => data}
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <AsyncControllerSelect
                control={control}
                label='Store'
                name='store'
                url='/stock/list'
                keyWord='id'
                error={errors.store}
                getLabel={data => data?.name}
                placeholder=''
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <AsyncAutoComplate
                label='Permissions'
                name='permission'
                url='/user/permission-list'
                control={control}
                error={errors?.permission}
                getOptionLabel={data => data?.name}
                getValue={data => data?.id}
                filterOption={(option, search) => option?.data?.name?.toLowerCase().includes(search.toLowerCase())}
                getData={data => data}
                multiple
              />
            </Grid>

            <Grid item xs={12}>
              <AsyncControllerSelect
                control={control}
                name={'currency'}
                label={'Currency'}
                error={errors.currency}
                url='/currency/list'
                keyWord='id'
                getLabel={data => data?.name}
                placeholder=''
              />
            </Grid>

            <Grid item xs={12}>
              <ControllerInput
                control={control}
                name={'rent_percentage'}
                type='number'
                error={errors.rent_percentage}
                label={'Rent Percentage'}
                placeholder='rent_percentage..'
              />
            </Grid>

            <Grid item md={6} xs={12} spacing={6}>
              <Typography variant='h4' sx={{ my: 3 }}>
                <Translations text='Bonus' />
              </Typography>
              <ControllerRadio error={errors.Bonus} control={control} label='Bonus' name='bonus' />
            </Grid>
            <Grid item md={6} xs={12} spacing={6}>
              <Typography variant='h4' sx={{ my: 3 }}>
                <Translations text='Rent' />
              </Typography>
              <ControllerRadio error={errors.Rent} control={control} label='Rent' name='rent' />
            </Grid>


            {query.has('id') ? null : (
              <Grid item xs={12}>
              <ControllerFile control={control} label='Avatar' name='photo' error={errors?.photo}/>
                
              </Grid>
            )}

            <Grid item xs={12}>
              <LoadingButton
                variant='contained'
                loading={createInfo?.isLoading}
                fullWidth
                onClick={handleSubmit(async (data: any) => {
                  const newData = query.has('id')
                    ? { ...data, id: Number(id) }
                    : { ...data, photo: API_URL + photoUrl, id: Number(id) }

                  const transformedData = transformData(newData)

                  await handleFinish(transformedData)
                  query.removeMany('id', 'onAdd')
                 })}
              >
                <Translations text='Submit' />
              </LoadingButton>
            </Grid>
          </Grid>
        )
      }}
    </Form>
  )
}

export default UserForm
