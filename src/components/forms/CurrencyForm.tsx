import { Grid } from '@mui/material'
import React from 'react'
import Translations from 'src/layouts/components/Translations'
import { LoadingButton } from '@mui/lab'
import Form from '../form'
import * as yup from 'yup'
import useQueryParams from 'src/hooks/useQueryParams'
import { useRouter } from 'next/router'
import ControllerInput from '../with-controller/ControllerInput'
import ControllerSelect from '../with-controller/ControllerSelect'

const validationSchema = yup.object().shape<any>({
  name: yup.string().required('This field is required'),
  symbol: yup.string().required('This field is required'),
  exchange_rate: yup.number().required('This field is required')
})

function CurrencyForm() {
  const query = useQueryParams()
  const route = useRouter()
  const { id } = route.query

  const initialValues = {
    name: '',
    symbol: '',
    exchange_rate: 1260
  }

  const symbolList = [
    { id: 1, name: 'Ruble', symbol: '₽' },
    { id: 2, name: 'Yen', symbol: '¥' },
    { id: 3, name: 'Euro', symbol: '€' },
    { id: 4, name: 'Pound', symbol: '£' },
    { id: 5, name: 'Dollar', symbol: '$' },
    { id: 6, name: 'Bitcoin', symbol: '₿' },
    { id: 7, name: 'Rupee', symbol: '₹' }, 
    { id: 8, name: 'Won', symbol: '₩' }, // xitoy won 
    { id: 9, name: 'Uzbek Som', symbol: 'soʻm' }, // Uzbek som
    { id: 10, name: 'Tajik Somoni', symbol: 'SM' }, // Tajik somoni
    { id: 11, name: 'Turkish Lira', symbol: '₺' }, // Turkish lira
    { id: 12, name: 'Kazakhstani Tenge', symbol: '₸' }, // Kazakhstani tenge
    { id: 13, name: 'Kyrgyzstani Som', symbol: 'с' }, // Kyrgyzstani som
    { id: 14, name: 'Turkmenistani Manat', symbol: 'm' } // Turkmenistani manat
  ];

  return (
    <Form
      getUrl={query?.has('id') ? '/currency/info/' + query?.get('id') : ''}
      url={query?.has('id') ? '/currency/update' : '/currency/create'}
      validationSchema={validationSchema}
      initialValues={initialValues}
    >
      {({ handleFinish, createInfo, form }) => {
        const { control, handleSubmit ,  formState: { errors }} = form

        return (
          <Grid container spacing={6} p={10}>
            <Grid item sm={6}>
              <ControllerInput control={control} name={'name'} label={'Currency Name'} placeholder='' />
            </Grid>

            <Grid item sm={6}>
              <ControllerSelect
                control={control}
                name={'symbol'}
                label={'Symbol'}
                data={symbolList}
                getLabel={(data: any) => data?.name + ': ' + data?.symbol}
                error={errors?.symbol}
                placeholder=''
                keyWord='id'
              />
            </Grid>

            <Grid item sm={12}>
              <ControllerInput
                control={control}
                name={'exchange_rate'}
                type='number'
                label={'Exchange Rate'}
                error={errors.exchange_rate}
                placeholder=''
              />
            </Grid>

            <Grid item sm={12}>
              <LoadingButton
                variant='contained'
                loading={createInfo?.isLoading}
                fullWidth
                onClick={handleSubmit(async (data: any) => {
                  await handleFinish({ ...data, id: Number(id) })
                  query.removeMany('id', 'onAdd')
                 })}
              >
                <Translations text='Submit' />
              </LoadingButton>
            </Grid>
          </Grid>
        )
      }}
    </Form>
  )
}

export default CurrencyForm
