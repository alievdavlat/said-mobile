import { Grid } from '@mui/material'
import React from 'react'
import Translations from 'src/layouts/components/Translations'
import { LoadingButton } from '@mui/lab'
import Form from '../form'
import * as yup from 'yup'
import useQueryParams from 'src/hooks/useQueryParams'
import { useRouter } from 'next/router'
import ControllerInput from '../with-controller/ControllerInput'

const validationSchema = yup.object().shape<any>({
  name: yup.string().required('This field is required'),
})

function TypeForm() {
  const query = useQueryParams()
  const route = useRouter()
  const { id } = route.query

  const initialValues = {
    name: ''
  }

  return (
    <Form
      getUrl={query?.has('id') ? '/product/type/info/' + query?.get('id') : ''}
      url={query?.has('id') ? '/product/type/update' : '/product/type/create'}
      validationSchema={validationSchema}
      initialValues={initialValues}
>
      {({ handleFinish, createInfo, form }) => {
        const {
          control,
          handleSubmit,
          formState: { errors }
        } = form

        return (
          <Grid container   spacing={6} p={10}>
           
            <Grid item sm={12}>
              <ControllerInput control={control} error={errors.name} name={'name'} label={'Type Name'} placeholder=''  />
            </Grid>

            
            <Grid item sm={4}>
              <LoadingButton
                variant='contained'
                loading={createInfo?.isLoading}
                fullWidth
                onClick={handleSubmit(async (data: any) => {
                  await handleFinish({ ...data, id: Number(id) })
                  query.removeMany('id', 'onAdd')
                })}
              >
                <Translations text='Submit' />
              </LoadingButton>

            </Grid>
          </Grid>
        )
      }}
    </Form>
  )
}

export default TypeForm
