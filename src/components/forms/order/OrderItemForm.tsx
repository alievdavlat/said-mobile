import React from 'react'
import { Control, FieldErrors, FieldError, useFieldArray } from 'react-hook-form'
import { Accordion, AccordionDetails, AccordionSummary, Button, Grid, Typography } from '@mui/material'
import Translations from 'src/components/translations'
import Icon from 'src/@core/components/icon'
import ControllerInput from 'src/components/with-controller/ControllerInput'
import AsyncAutoComplate from 'src/components/with-controller/auto-complate/AsyncAutoComplate'
import { useAuth } from 'src/hooks/useAuth'
import useQueryParams from 'src/hooks/useQueryParams'

type Props = {
  control: Control
  errors: FieldErrors | FieldError
  setValue:any
  watch:any
}

const OrderItemForm = (props: Props) => {
  const { control, setValue , watch} = props
  const [expanded, setExpanded] = React.useState<number | false>(0)

  const auth = useAuth()
  const query = useQueryParams()

  const handleChange = (panel: number) => (event: React.SyntheticEvent, isExpanded: boolean) => {
    setExpanded(isExpanded ? panel : false)
  }

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'order_item'
  })

  const handleAddNewField = () => {
    append({
      product: 0,
      price: '',
      code: ''
    })
  }
  const handleRemove = (idx: any) => {
    remove(idx)
  }

  

  return (
    <>
      {fields.map((item, index: number) => (
        <Accordion key={item.id} expanded={expanded === index} onChange={handleChange(index)}>
          <AccordionSummary
            id='controlled-panel-header-1'
            aria-controls='controlled-panel-content-1'
            expandIcon={<Icon fontSize='1.25rem' icon='tabler:chevron-down' />}
          >
            <Typography sx={{ my: 4 }}>
              <Translations text='Order' /> {index + 1}
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Grid container spacing={6} mt={6} key={item.id}>
              <Grid item xs={12} sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                <Button color='error' variant='outlined' sx={{ mt: 3, ml: 2 }} onClick={() => handleRemove(index)}>
                  <Icon fontSize='1.5rem' icon='tabler:trash' /> {'  '}
                </Button>
              </Grid>

              <Grid item xs={6}>
                <AsyncAutoComplate
                  control={control}
                  label='Product'
                  name={`order_item.${index}.product`}
                  url='/stock/product-list'
                  getData={(data:any) => data?.results}
                  getOptionLabel={(data:any) => data?.product?.name}
                  getValue={(data:any) => data?.product?.id}
                  error={null}
                  disabled={query.has('id')}
                  filterOption={option => option}
                  params={auth?.user?.store || watch('store') ? { store: auth?.user?.store || watch('store') } : {}}
                  onSelect={(data:any) => {
                    //@ts-ignore
                    setValue(`order_item.${index}.price`, data?.product?.price)
                  }}
                  onSearchChange={(e:any) => !!e && setValue(`order_item.${index}.code`, e)}
                />
              </Grid>

              <Grid item xs={6}>
                <ControllerInput
                  control={control}
                  name={`order_item.${index}.price`}
                  label={'Price'}
                  placeholder=''
                />
              </Grid>
              <Grid item xs={12}>
                <ControllerInput
                  control={control}
                  name={`order_item.${index}.code`}
                  type='number'
                  label={'Code'}
                  placeholder=''
                />
              </Grid>
            </Grid>
          </AccordionDetails>
        </Accordion>
      ))}

      <Button onClick={handleAddNewField} variant='text' sx={{ mt: 12 }}>
        <Icon fontSize='1.5rem' icon='tabler:plus' />
        <Translations text='Add Order' />
      </Button>
    </>
  )
}

export default OrderItemForm
