import { Button, Grid } from '@mui/material'
import React from 'react'
import Translations from 'src/layouts/components/Translations'
import { LoadingButton } from '@mui/lab'
import Form from '../../form'
import * as yup from 'yup'
import useQueryParams from 'src/hooks/useQueryParams'
import { useRouter } from 'next/router'
import ControllerInput from '../../with-controller/ControllerInput'
import AsyncControllerSelect from '../../with-controller/AsyncControllerSelect'
import ControllerTextArea from 'src/components/with-controller/ControllerTextArea'
import Icon from 'src/@core/components/icon'
import OrderItemForm from './OrderItemForm'
import { useTranslation } from 'react-i18next'
import ControllerSelect from 'src/components/with-controller/ControllerSelect'


const validationSchema = yup.object().shape<any>({
  client: yup.number().required('This field is required'),
  discount_amount: yup.string().required('This field is required'),
  price_type: yup.number().required('This field is required'),
  currency: yup.number().required('This field is required'),
  user: yup.number().required('This field is required'),
  store: yup.number().required('This field is required'),
  status: yup.number().required('This field is required'),
  comment: yup.string().required('This field is required'),
  seller: yup.number().required('This field is required'),
  order_item: yup.array().of(
    yup.object().shape({
      product: yup.number().required('Product is required'),
      price: yup.string().required('Price is required'),
      codes: yup.array().of(yup.string())
    })
  )
})

function OrderForm() {
  const query = useQueryParams()
  const route = useRouter()
  const { id } = route.query
  const [isOrderItemSet, setIsOrderItemSet] = React.useState(false)
  const {t} = useTranslation()

  const initialValues = {
    client: 0,
    discount_amount: '',
    price_type: 0,
    currency: 0,
    user: 0,
    store: 0,
    status: 0,
    comment: '',
    seller: 0,
    order_item: [
      {
        product: 0,
        price: '',
        code: ''
      }
    ]
  }

  const price_type = [
    {
      id: 1, 
      title:t('PAID')
    },
    {
      id: 2, 
      title:t('TERMINAL')
    },
    {
      id: 3, 
      title:t('TRANSFER')
    }
  ]

  const statusData = [
    {
      id: 1, 
      title:t('Paid')
    },
    {
      id: 2, 
      title:t('PAYMENT')
    },
    {
      id: 3, 
      title:t('CLOSED')
    }
  ]



  return (
    <Form
      url={'/order/create'}
      validationSchema={validationSchema}
      initialValues={initialValues}
    >
      {({ handleFinish, createInfo, form }) => {
        const {
          control,
          formState: { errors },
          handleSubmit,
          watch,
          setValue
        } = form

        return (
          <Grid container spacing={6} p={10}>
            <Grid item md={6} xs={12}>
              <AsyncControllerSelect
                control={control}
                label='Clients'
                name='client'
                url='/client/list'
                keyWord='id'
                getLabel={data => data?.first_name + ' ' + data?.last_name}
                error={errors.client}
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <AsyncControllerSelect
                control={control}
                label='User'
                name='user'
                url='/user/list'
                keyWord='id'
                getLabel={data => data?.is_superuser ? 'Super Admin' :  data?.firstname + ' ' + data?.lastname}
                error={errors.user}
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <AsyncControllerSelect
                control={control}
                label='Store'
                name='store'
                url='/stock/list'
                keyWord='id'
                getLabel={data => data?.name}
                error={errors.store}
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <AsyncControllerSelect
                control={control}
                label='Currency'
                name='currency'
                url='/currency/list'
                keyWord='id'
                getLabel={data => data?.name}
                error={errors.currency}
              />
            </Grid>

            <Grid item md={6} xs={12}>
            <AsyncControllerSelect
                control={control}
                name={'seller'}
                label={'Seller'}
                url='/user/list'
                keyWord='id'
                getLabel={data => data?.is_superuser ? 'Super Admin' :  data?.firstname + ' ' + data?.lastname}
                error={errors.seller}
              />
            </Grid>

            <Grid item md={6} xs={12}>
               <ControllerSelect
                control={control}
                error={errors.price_type}
                name={'price_type'}
                label={'Price Type'}
                data={price_type}
                getLabel={(data: any) => data?.title}
                placeholder=''
                keyWord='id'
              />
            </Grid>

            <Grid item md={6} xs={12}>
               <ControllerSelect
                control={control}
                error={errors.price_type}
                name={'status'}
                label={'Status'}
                data={statusData}
                getLabel={(data: any) => data?.title}
                placeholder=''
                keyWord='id'
              />
            </Grid>


            <Grid item md={6}  xs={12}>
              <ControllerInput
                control={control}
                error={errors.discount_amount}
                name={'discount_amount'}
                label={'Discount Amount'}
                placeholder=''
                type='number'
              />
            </Grid>

            <Grid item xs={12}>
              <ControllerTextArea
                control={control}
                error={errors.comment}
                name={'comment'}
                label={'Comment'}
                placeholder=''
              />
            </Grid>

           

            <Grid item sm={12}>
                  {!isOrderItemSet && !query.has('id') ? (
                    <Button
                      onClick={() => {
                        query.set('onAdd', 'true')
                        setIsOrderItemSet(!isOrderItemSet)
                      }}
                      variant='text'
                    >
                      <Icon fontSize='1.5rem' icon='tabler:plus' />
                      <Translations text='Add Order' />
                    </Button>
                  ) : (
                    <>
                      <OrderItemForm setValue={setValue} watch={watch} control={control} errors={errors} />
                    </>
                  )}
            </Grid>

            <Grid item sm={4}>
              <LoadingButton
                variant='contained'
                loading={createInfo?.isLoading}
                fullWidth
                onClick={handleSubmit(async (data: any) => {
                  await handleFinish({ ...data, id: Number(id) })
                  query.removeMany('id', 'onAdd')
                })}
              >
                <Translations text='Submit' />
              </LoadingButton>
            </Grid>
          </Grid>
        )
      }}
    </Form>
  )
}

export default OrderForm
