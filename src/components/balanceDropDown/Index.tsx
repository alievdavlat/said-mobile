// ** React Imports
import { useState, SyntheticEvent, Fragment } from 'react'

// ** Next Import
import { useRouter } from 'next/router'

// ** MUI Imports
import Box from '@mui/material/Box'
import Menu from '@mui/material/Menu'
import Badge from '@mui/material/Badge'
import Divider from '@mui/material/Divider'
import { styled } from '@mui/material/styles'
import MenuItem, { MenuItemProps } from '@mui/material/MenuItem'
import { useTheme } from '@mui/material/styles'
import { Typography, useMediaQuery } from '@mui/material'

// ** Icon Imports
import Icon from 'src/@core/components/icon'

// ** Context
import { useAuth } from 'src/hooks/useAuth'

// ** Type Imports
import { Settings } from 'src/@core/context/settingsContext'
import Translations from 'src/components/translations'

interface Props {
  settings: Settings
}


const MenuItemStyled = styled(MenuItem)<MenuItemProps>(({ theme }) => ({
  '&:hover .MuiBox-root, &:hover .MuiBox-root svg': {
    color: theme.palette.primary.main
  }
}))

const BalanceDropDown = (props: Props) => {
  const { settings } = props

  const auth = useAuth()
  const [anchorEl, setAnchorEl] = useState<Element | null>(null)
  const theme = useTheme()
  const isMobile = useMediaQuery(theme.breakpoints.down('md'))
  const router = useRouter()
  const { direction } = settings

  const handleDropdownOpen = (event: SyntheticEvent) => {
    setAnchorEl(event.currentTarget)
  }

  const handleDropdownClose = (url?: string) => {
    if (url) {
      router.push(url)
    }
    setAnchorEl(null)
  }

  const styles = {
    px: 4,
    py: 1.75,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    color: 'text.primary',
    textDecoration: 'none',
    '& svg': {
      mr: 2.5,
      fontSize: '1.5rem',
      color: 'text.secondary'
    }
  }


  return (
    <>
      {!isMobile ? (
        <Box sx={{ display: 'flex',pl:2, alignItems:'center', gap: '.8rem', flexDirection: 'row' }}>
          <Typography
            variant='body2'
            sx={{
              fontSize: { xs: '0.8rem', md: '1rem', xss: '0.5rem', display: 'flex', alignItems: 'center', gap: '.3rem' }
            }}
          >
            <Icon fontSize='1.5rem' icon='material-symbols-light:store-outline' />
            <Translations text={`Store Balance`} /> : {'  '} {auth.storeBalance.balance}
          </Typography>
          {!auth.user?.is_superuser && (
            <Typography
              variant='body2'
              sx={{
                fontSize: {
                  xs: '0.8rem',
                  md: '1rem',
                  xss: '0.5rem',
                  display: 'flex',
                  alignItems: 'center',
                  gap: '.3rem'
                }
              }}
            >
              <Icon fontSize='1.5rem' icon='mdi:user-outline' />
              <Translations text={`User Balance`} /> : {'  '} {auth.userBalance.detail}
            </Typography>
          )}
        </Box>
      ) : (
        <Fragment>
          <Badge
            overlap='circular'
            onClick={handleDropdownOpen}
            sx={{ ml: 2, cursor: 'pointer' }}
            badgeContent={<></>}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'right'
            }}
          >
            <Icon fontSize='1.5rem' icon='solar:chat-round-money-line-duotone' />
          </Badge>
          <Menu
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            onClose={() => handleDropdownClose()}
            sx={{ '& .MuiMenu-paper': { width: 230, mt: 4.75 } }}
            anchorOrigin={{ vertical: 'bottom', horizontal: direction === 'ltr' ? 'right' : 'left' }}
            transformOrigin={{ vertical: 'top', horizontal: direction === 'ltr' ? 'right' : 'left' }}
          >
            <MenuItemStyled sx={{ p: 0 }}>
              <Box sx={styles}>
              <Icon fontSize='1.5rem' icon='material-symbols-light:store-outline' />
            <Translations text={`Balance`} /> : {'  '} {auth.storeBalance.balance}

              </Box>
            </MenuItemStyled>

            <Divider sx={{ my: theme => `${theme.spacing(2)} !important` }} />

            {!auth.user?.is_superuser && <MenuItemStyled sx={{ p: 0 }}>
              <Box sx={styles}>
              <Icon fontSize='1.5rem' icon='mdi:user-outline' />
              <Translations text={`Balance`} /> : {'  '} {auth.userBalance.detail}
              </Box>
            </MenuItemStyled>}
          </Menu>
        </Fragment>
      )}
    </>
  )
}

export default BalanceDropDown
