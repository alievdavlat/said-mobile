import { Dispatch, ReactNode, SetStateAction, createContext, useState } from 'react'

type RandomContextType = {
  random: number
  setRandom: Dispatch<SetStateAction<number>>
} | null

export const RandomContext = createContext<RandomContextType>(null)

export const RandomProvider = ({ children }: { children: ReactNode }) => {
  const [random, setRandom] = useState(Math.random())
  const value = {
    random,
    setRandom
  }
  
  return <RandomContext.Provider value={value}>{children}</RandomContext.Provider>
}
