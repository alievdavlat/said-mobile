//@ts-nocheck
// ** React Imports
import { createContext, useEffect, useState, ReactNode } from 'react'

// ** Next Import
import { useRouter } from 'next/router'

// ** Axios
import axios from 'axios'

// ** Config
import authConfig from 'src/configs/auth'

// ** Types
import { AuthValuesType, LoginParams, ErrCallbackType, UserDataType } from './types'
import { client } from 'src/configs/req'
import storage from 'src/helpers'

// ** Defaults
const defaultProvider: AuthValuesType = {
  user: null,
  loading: false,
  storeBalance: { balance: '' },
  userBalance:{detail:""},
  setUser: () => null,
  setLoading: () => Boolean,
  login: () => Promise.resolve(),
  logout: () => Promise.resolve()
}

const AuthContext = createContext(defaultProvider)

type Props = {
  children: ReactNode
}

const AuthProvider = ({ children }: Props) => {
  // ** States
  const [user, setUser] = useState<UserDataType | null>(defaultProvider.user)
  const [loading, setLoading] = useState<boolean>(defaultProvider.loading)
  //@ts-ignore
  const [balance, setBalance] = useState<{ balance: string }>({ balance: '0' })
  const [detail, setDetail] = useState<{ detail: string }>({ detail:'0'})
  const router = useRouter()

  const handleLogin = (params: LoginParams, errorCallback?: ErrCallbackType) => {
    axios
      .post(authConfig.loginEndpoint, params)
      .then(async response => {
         window.localStorage.setItem(authConfig.storageTokenKeyName, response.data.access)
         window.localStorage.setItem(authConfig.onTokenExpiration, response.data.refresh)
         const returnUrl = router.query.returnUrl
        
        setUser({ ...response.data.user })
        storage.set('storeId', response.data.user.store)
        storage.set('is_superuser', response.data.user.is_superuser)
        window.localStorage.setItem('userData', JSON.stringify(response.data.user))

        const redirectURL = returnUrl && returnUrl !== '/' ? returnUrl : '/home'

        router.replace(redirectURL as string)

        const timeoutId = setTimeout(async () => {
          await fetchInitialData();
        }, 1000);
  
        return () => clearTimeout(timeoutId);
      })


      .catch(err => {
        if (errorCallback) errorCallback(err)
      })
  }

  const handleLogout = () => {
    setUser(null)
    window.localStorage.removeItem('userData')
    window.localStorage.removeItem(authConfig.storageTokenKeyName)
    storage.remove('storeId')
    storage.remove('is_superuser')
    router.push('/login')
  }


  const fetchInitialData = async () => {
    const user = JSON.parse(window.localStorage.getItem('userData') as any);
  
    

    const getStoreBalances = async (): Promise<void> => {
      if (user.store || user?.is_superuser) {
        const res = await client(`/stock/balance/${user?.is_superuser ? 0 : user.store}`);
        setBalance(res.data);
      }
    };
  

    // const getUserBalance = async () => {
    //   if (user?.id) {
    //     const resUser = await client(`/user/balance/${user?.id}`);
    //     setDetail(resUser.data);
    //   }
    // };
  
    if (user) {
      await getStoreBalances();
      if (!user?.is_superuser) {
        // await getUserBalance();
      }
    }
  };


  const initAuth = async (): Promise<void> => {
    const storedToken = window.localStorage.getItem(authConfig.storageTokenKeyName)!;
    const userId = JSON.parse(window.localStorage.getItem('userData') as any);
  
    if (storedToken && userId) {
      setLoading(true);
      const res = await client(`/user/info/${userId.id}`);
      setUser(res.data);
      storage.set('storeId', res?.data?.store);
      storage.set('is_superuser', res?.data?.is_superuser);
      setLoading(false);
    } else {
      setLoading(false);
    }
  };


  useEffect(() => {
    const initAuthAndFetchData = async () => {
      await initAuth();
      const timeoutId = setTimeout(async () => {
        await fetchInitialData();
      }, 1000);
  
      return () => clearTimeout(timeoutId);
    };
  
    initAuthAndFetchData();
  }, []);

  


  const values = {
    user,
    loading,
    setUser,
    storeBalance: balance,
    userBalance:detail,
    setLoading,
    login: handleLogin,
    logout: handleLogout
  }

  return <AuthContext.Provider value={values}>{children}</AuthContext.Provider>
}

export { AuthContext, AuthProvider }
