// ** React Imports
import { createContext, useState, ReactNode, SetStateAction, Dispatch } from 'react'

// ** Types
import { MRT_ColumnFiltersState, MRT_PaginationState, MRT_SortingState } from 'material-react-table'

// ** Defaults

export interface TableFilterType {
  columnFilters: MRT_ColumnFiltersState
  globalFilter: string
  sorting: MRT_SortingState
  pagination: MRT_PaginationState
  rowCount: number | string | any 
  value: string
  id:number | string | any
  setId:any, 
  totalData:number | string | any,
  currentPage:number | string | any,
  pageSize:number | string | any,
  totalPages:number | string | any,
  rowSelection:any,
  setRowSelection:any
  setTotalData:() => any,
  setCurrentPage:() => any,
  setPageSize:() => any,
  setTotalPages:() => any,
  setValue: Dispatch<SetStateAction<string>>
  setColumnFilters: Dispatch<SetStateAction<MRT_ColumnFiltersState>>
  setGlobalFilter: Dispatch<SetStateAction<string>>
  setSorting: Dispatch<SetStateAction<MRT_SortingState>>
  setPagination: Dispatch<SetStateAction<MRT_PaginationState>>
  setRowCount: any
  isOpenCreateModal: boolean
  setIsOpenCreateModal: Dispatch<SetStateAction<boolean>>
}

const defaultProvider = {
  columnFilters: [],
  globalFilter: '',
  sorting: [],
  pagination: {
    pageIndex: 0,
    pageSize: 10
  },
  rowCount: 1,
  value: '',
  id:'', 
  totalData:1,
  currentPage:1,
  pageSize:10,
  totalPages:1,
  rowSelection:{},
  setRowSelection:() => [],
  setTotalData:() => 1,
  setCurrentPage:() => 1,
  setPageSize:() => 1,
  setTotalPages:() => 1,
  setValue: () => '',
  setId: () => '', 
  setColumnFilters: () => [],
  setGlobalFilter: () => '',
  setSorting: () => [],
  setPagination: () => {
    pageIndex: 0
    pageSize: 10
  },
  setRowCount: () => 1,
  isOpenCreateModal: false,
  setIsOpenCreateModal: () => false
}
const TableFilterContext = createContext<TableFilterType>(defaultProvider)

type Props = {
  children: ReactNode
}

const TableFilterProvider = ({ children }: Props) => {
  // ** States
  const [columnFilters, setColumnFilters] = useState<MRT_ColumnFiltersState>([])
  const [globalFilter, setGlobalFilter] = useState('')
  const [sorting, setSorting] = useState<MRT_SortingState>([])
  const [id, setId] = useState('')
  const [rowSelection, setRowSelection] = useState({})
  const [totalData, setTotalData] = useState(1)
  const [currentPage, setCurrentPage] = useState(1)
  const [pageSize, setPageSize] = useState(10)
  const [totalPages, setTotalPages] = useState(1)
  const [pagination, setPagination] = useState<MRT_PaginationState>({
    pageIndex: 0,
    pageSize: 10
  })
  const [rowCount, setRowCount] = useState(1)
  const [value, setValue] = useState('')
  const [isOpenCreateModal, setIsOpenCreateModal] = useState(false)

  // ** Hooks
  // const router = useRouter()

  const values = {
    columnFilters,
    globalFilter,
    sorting,
    pagination,
    rowCount,
    id,
    totalData, 
    currentPage, 
    pageSize, 
    totalPages,
    setTotalData, 
    setCurrentPage, 
    setPageSize, 
    setTotalPages,
    setId,
    setColumnFilters,
    setGlobalFilter,
    setSorting,
    setPagination,
    setRowCount,
    value,
    setValue,
    isOpenCreateModal,
    setIsOpenCreateModal,
    rowSelection,
    setRowSelection
  }
  //@ts-ignore

  return <TableFilterContext.Provider value={values}>{children}</TableFilterContext.Provider>
}

export { TableFilterContext, TableFilterProvider }
