
export type ErrCallbackType = (err: { [key: string]: string }) => void

export type LoginParams = {
  login: string
  password: string
  rememberMe?: boolean
}

export type UserRole  = {
  id: number
  name: string
  created_at: string
}

export interface PermissionsProps {
  id:number
  name: string
  slug:string
  created_at: string
}

export type UserDataType = {
  id: number
  access_urls: string[]
  bonus?:boolean | null
  currency?:number | string
  firstname?: string
  lastname?: string
  surname?: string
  login?:{
    id:number
    name:string
    created_at:string
  }
  permission: PermissionsProps[]
  photo?: string | null
  rent?: boolean
  rent_percentage?:number | string
  role: UserRole
  store?: number
  joined_date?:string,
  is_superuser?:boolean
}

export type AuthValuesType = {
  loading: boolean
  logout: () => void
  user: UserDataType | null
  storeBalance:{balance: string}
  userBalance:{detail: string}
  setLoading: (value: boolean) => void
  setUser: (value: UserDataType | null) => void
  login: (params: LoginParams, errorCallback?: ErrCallbackType) => void
}
