/* eslint-disable */

// NotificationProvider.tsx
import { Avatar, Box, Typography, useTheme } from '@mui/material'
import { useMutation, useQuery } from '@tanstack/react-query'
import { createContext, ReactNode, useCallback, useEffect, useState, useRef, FC } from 'react'
import toast from 'react-hot-toast'
import { useTranslation } from 'react-i18next'
import { hanldeRequest } from 'src/configs/req'
import { useAuth } from 'src/hooks/useAuth'
import Loader from 'src/components/loader' // Import your Loader component

// ** Defaults

interface NotificationType {
  sendNotification: (key?: string) => void;
  transactionRefetch: () => void;
  transferRefetch: () => void;
  transactionMutate: (id: any) => void;
  transferMutate: (id: any) => void;
  transactionData: any; // `any` o'rniga mos keladigan turini yozish kerak
  transferData: any; // `any` o'rniga mos keladigan turini yozish kerak
  transactionSuccess: boolean;
  transferSuccess: boolean;
  noteLength:number
}

const defaultProvider: NotificationType = {
  sendNotification: () => {}, // Default empty function
  transactionRefetch: () => {}, // Default empty function
  transferRefetch: () => {}, // Default empty function
  transactionMutate: () => {}, // Default empty function
  transferMutate: () => {}, // Default empty function
  transactionData: null, // Default to null or an empty object
  transferData: null, // Default to null or an empty object
  transactionSuccess: false, // Default to false
  transferSuccess: false, // Default to false
  noteLength:0
};

const NotificationContext = createContext<NotificationType>(defaultProvider);

type Props = {
  children: ReactNode;
}

const NotificationProvider: FC<Props> = ({ children }) => {
  // ** State
  const [audio, setAudio] = useState<HTMLAudioElement | null>(null);
  const [isFirstLoad, setIsFirstLoad] = useState(true);


  // ** Hooks
  const theme = useTheme();
  const auth = useAuth();
  const { t } = useTranslation();

  // ** Ref
  const transactionPreviousDataLength = useRef(0);
  const transferPreviousDataLength = useRef(0);

  // ** Variables
  let transaction_url: string | undefined;
  let transfer_url: string | undefined;

  const queryOptions = {
    refetchInterval: 300000, // Refetch every 5 minute (adjust as needed)
  }


  if (auth.user?.store) {
    transaction_url = `/stock/transaction/${auth.user.store}`;
    transfer_url = `/stock/transfer/${auth.user.store}`;
  }


  // ** audio play

  useEffect(() => {
    const newAudio = new Audio(
      'https://firebasestorage.googleapis.com/v0/b/learning-platfrom-9b485.appspot.com/o/livechat-129007.mp3?alt=media&token=c3238efc-15b2-4c62-91e3-c0cd809a1eba&_gl=1*zfx25t*_ga*MTU4NjE2MDk5Ny4xNjk2NzU0MDMw*_ga_CW55HF8NVT*MTY5ODY0MzU2MC4zMy4xLjE2OTg2NDM3NjMuNTIuMC4w'
    );
    setAudio(newAudio);
  }, []);


  // ** Handlers

  const playNotificationSound = (key?: string) => {
    
    const message =
      key && key === 'transfer'
        ? 'You have a new Transfer!!'
        : key && key === 'transaction'
        ? 'You have a new Transaction!!'
        : 'New Notification!';

    toast.custom(() => (
      <Box display={'flex'} alignItems={'center'} justifyContent={'space-between'} gap={5} border={`1px solid ${theme.palette.primary.main}`} p={3} borderRadius={'10px'}>
        <Box display={'flex'} alignItems={'center'} justifyContent={'space-between'} gap={4}>
          <Avatar alt='Said Mobile' src='/images/notification-icon.webp' />
          <Typography variant='body1'>{message}</Typography>
        </Box>
        {/* <Divider orientation='vertical' variant='middle' /> */}
        {/* <Button variant='tonal' onClick={() => toast.dismiss(t.id)} >
          
          <Translations text='Close'/>
        </Button> */}
      </Box>
    ));

    audio?.play();
  }

  // **  send Note 

  const sendNotification = (key?: string) => {
    const message =
      key && key === 'transfer'
        ? 'You have a new Transfer!!'
        : key && key === 'transaction'
        ? 'You have a new Transaction!!'
        : 'New Notification!';

    if ('Notification' in window && Notification.permission === 'granted') {
      new Notification('Said Mobile!', {
        body: message,
        icon: '/images/notification-icon.webp'
      });
        playNotificationSound(key);
    }

  }

  const requestNotificationPermission = useCallback((key:string) => {
    if ('Notification' in window) {
      Notification.requestPermission().then((permission) => {
        if (permission === 'granted') {
          sendNotification(key);
        }
      });
    }
  }, [sendNotification]);



  // ** Notification Get
  const { data: transferData, isSuccess: transferSuccess, refetch: transferRefetch } = useQuery(
    ['transfer', transfer_url],
    async () => {
      const response: any = await hanldeRequest({
        url: transfer_url,
        method: 'GET'
      });
      return response.data;
    },
    {
      enabled: !!auth.user?.store, // auth.user?.store mavjud bo'lgandagina ishga tushadi
      onSuccess: () => {
        toast.success(t('Successfully Completed'));
      },
      onError: (err: any) => {
        toast.error(err.message);
      },
      ...queryOptions
    }
  );

  const { data: transactionData, isSuccess: transactionSuccess, refetch: transactionRefetch } = useQuery(
    ['transaction', transaction_url],
    async () => {
      const response: any = await hanldeRequest({
        url: transaction_url,
        method: 'GET'
      });
      return response.data;
    },
    {
      enabled: !!auth.user?.store, // auth.user?.store mavjud bo'lgandagina ishga tushadi
      onSuccess: () => {
        toast.success(t('Successfully Completed'));
      },
      onError: (err: any) => {
        toast.error(err.message);
      },
      ...queryOptions

    }
  );

  // ** Notification Accept

  const { mutate: transactionMutate } = useMutation({
    mutationFn: async (id: any) => {
      const response = await hanldeRequest({
        url: `/stock/transaction-accept/${id}`,
        method: 'PUT',
        data: { status: true }
      });
      return response?.data;
    },
    onSuccess: () => {
      toast.success(t('Successfully Completed'));
    },
    onError: (error: any) => {
      toast.error(t('Something Went Wrong'), error);
    }
  });

  const { mutate: transferMutate } = useMutation({
    mutationFn: async (id: any) => {
      const response = await hanldeRequest({
        url: `/stock/transfer-accept/${id}`,
        method: 'PUT',
        data: { status: true }
      });
      return response?.data;
    },
    onSuccess: () => {
      toast.success(t('Successfully Completed'));
    },
    onError: (error: any) => {
      toast.error(t('Something Went Wrong'), error);
    }
  });

  // ** useEffect logics


  useEffect(() => {
    if (transactionSuccess && transactionData?.results) {
      if (transactionData.results.length > transactionPreviousDataLength.current) {
        sendNotification('transaction'); // Send notification for new transaction
      }
      if (isFirstLoad) {
        setIsFirstLoad(false);
      }
      transactionPreviousDataLength.current = transactionData.results.length;
    }
  }, [transactionData, transactionSuccess, isFirstLoad, sendNotification]);

  useEffect(() => {
    if (transferSuccess && transferData?.results) {
      if ( transferData.results.length > transferPreviousDataLength.current) {
        sendNotification('transfer'); // Send notification for new transfer
      }
      if (isFirstLoad) {
        setIsFirstLoad(false);
      }
      transferPreviousDataLength.current = transferData.results.length;
    }
  }, [transferData, transferSuccess, isFirstLoad, sendNotification]);
  

  let dataLength:number  = 0
  
  if (transactionData && transferData && transferSuccess && transactionSuccess) {
    dataLength = transactionData?.results.length + transferData?.results?.length
  }

  // ** Values
  const values: NotificationType = {
    sendNotification,
    transactionRefetch,
    transferRefetch,
    transactionMutate,
    transferMutate,
    transactionData,
    transferData,
    transactionSuccess,
    transferSuccess,
    noteLength:dataLength

  };

  // Render loader if authentication is still loading
  if (auth.loading) {
    return <Loader />;
  }

  return (
    <NotificationContext.Provider value={values}>
      {children}
    </NotificationContext.Provider>
  );
}

export { NotificationContext, NotificationProvider }
