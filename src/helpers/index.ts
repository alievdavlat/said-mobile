

const storage = {
  get: (key:string) => {
    if (
      Boolean(
        typeof window !== "undefined" &&
          window.document &&
          window.document.createElement
      )
    ) {
      return localStorage.getItem(key) || null;
    }
  },

  set: (key:string, value:any) => {
    if (!value || value.length <= 0) {
      return;
    }
    if (
      Boolean(
        typeof window !== "undefined" &&
          window.document &&
          window.document.createElement
      )
    ) {
      window.localStorage.setItem(key, value);
    }
  },

  remove: (key:string) => {
    if (window.localStorage && window.localStorage[key]) {
      window.localStorage.removeItem(key);
      
      return true;
    }
  },
};

export default storage;
