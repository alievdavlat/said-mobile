import React from 'react';
import { MenuItem, Button, Menu } from '@mui/material';
import { saveAs } from 'file-saver';
import * as XLSX from 'xlsx';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import Icon from 'src/@core/components/icon'
import { useTranslation } from 'react-i18next';


interface TableExportComponentProps {
  columns: Array<{ accessorKey: string, header: string }>;
  data: Array<Record<string, any>>;
  exportOptions?: {
    csv?: boolean;
    excel?: boolean;
    pdf?: boolean;
    word?: boolean;
  };
}

const TableExportComponent: React.FC<TableExportComponentProps> = ({ columns, data, exportOptions = {} }) => {

  const {t} = useTranslation()
  const exportToCSV = () => {
    const worksheet = XLSX.utils.json_to_sheet(data);
    const csv = XLSX.utils.sheet_to_csv(worksheet);
    const blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
    saveAs(blob, 'data.csv');
  };

  const exportToExcel = () => {
    const worksheet = XLSX.utils.json_to_sheet(data);
    const workbook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workbook, worksheet, 'Sheet1');
    const excelBuffer = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    const blob = new Blob([excelBuffer], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8;' });
    saveAs(blob, 'data.xlsx');
  };

  const exportToPDF = () => {
    const doc = new jsPDF();
    const tableColumn = columns.map((col) => col.header);
    const tableRows: any[] = [];
    data.forEach((row) => {
      const rowData = columns.map((col) => row[col.accessorKey]);
      tableRows.push(rowData);
    });
    doc.autoTable({
      head: [tableColumn],
      body: tableRows,
    });
    doc.save('data.pdf');
  };

  const exportToWord = () => {
    const header = columns.map((col) => col.header).join('\t') + '\n';
    const rows = data.map((row) => columns.map((col) => row[col.accessorKey]).join('\t')).join('\n');
    const blob = new Blob([header + rows], { type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document;charset=utf-8;' });
    saveAs(blob, 'data.docx');
  };

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  
  return (
    <>
      <Button onClick={handleClick} variant='outlined' sx={{display:'flex', alignItems:"center", justifyContent:"center", gap:2}}>
        <Icon fontSize={'1.2rem'} icon="game-icons:save-arrow"/>
        Export
      </Button>
      <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={handleClose}>
        {exportOptions.csv && <MenuItem onClick={() => { exportToCSV(); handleClose(); }}>{t('Export to CSV')}</MenuItem>}
        {exportOptions.excel && <MenuItem onClick={() => { exportToExcel(); handleClose(); }}>{t('Export to Excel')}</MenuItem>}
        {exportOptions.pdf && <MenuItem onClick={() => { exportToPDF(); handleClose() }}>{t("Export to PDF")}</MenuItem>}
        {exportOptions.word && <MenuItem onClick={() => { exportToWord(); handleClose() }}>{t("Export to Word")}</MenuItem>}
      </Menu>
    </>
  );
};

export default TableExportComponent;
