import React, { useContext } from 'react'
import GetContainer from '../../components/get-container'
import { TableFilterContext } from 'src/context/TableFilterContext'
import { MRT_ExpandedState, MaterialReactTable, useMaterialReactTable } from 'material-react-table'
import useQueryParams from 'src/hooks/useQueryParams'
import TableExportComponent from './TableExportComponent'
import { Box } from '@mui/material'

interface IProps {
  url: string
  columns: any
  params?: any
  onClick?: (row: any) => void
  disablePagination?: boolean
  rowIdKey: string
  disableBodyClick?: boolean
  disableRowSelection?: boolean
  columnVisibilityData?: any
  enableExpanding?: boolean
  showExport?: boolean
  exportOptions?:any
}

const Table = (props: {
  data: any
  columns: any
  disablePagination: boolean
  rowIdKey?: string
  isError: boolean
  isRefetching: boolean
  isLoading: boolean
  onClick?: (row: any) => void
  rowCount: number
  disableBodyClick?: boolean
  disableRowSelection?: boolean
  columnVisibilityData?: any
  enableExpanding?: boolean
}) => {
  const filter = useContext(TableFilterContext)
  const query = useQueryParams()
  const [columnVisibility, setColumnVisibility] = React.useState(props.columnVisibilityData)
  const [expanded, setExpanded] = React.useState<MRT_ExpandedState>({})

  const {
    columnFilters,
    globalFilter,
    pagination,
    setColumnFilters,
    setGlobalFilter,
    setPagination,
    rowSelection,
    setRowSelection
  } = filter

  React.useEffect(() => {
    setColumnVisibility(props.columnVisibilityData) //programmatically show firstName column
  }, [props.columnVisibilityData])

  const table = useMaterialReactTable({
    columns: props.columns || [],
    data: props?.data,
    selectAllMode: 'all',
    enableSelectAll:false,
    enableRowSelection: true,
    enablePagination: !props.disablePagination,
    muiTableBodyRowProps: ({ row }) => ({
      onClick: () => {
        props.onClick ? props.onClick(row.original) : !props.disableBodyClick && query.set('id', row.original?.id)
      },
      sx: {
        cursor: 'pointer' //you might want to change the cursor too when adding an onClick
      }
    }),
    defaultColumn: {
      maxSize: 400,
      minSize: 80,
      size: 150 //default size is usually 180
    },
    enableRowActions: true,
    enableColumnResizing: true,
    columnResizeMode: 'onChange',
    manualFiltering: true,
    enableExpanding: props.enableExpanding,
    enableExpandAll: false,
    filterFromLeafRows: true,
    manualPagination: true,
    getRowId(originalRow) {
      return props.rowIdKey ? originalRow?.[props.rowIdKey] : originalRow.id
    },
    muiToolbarAlertBannerProps: props.isError
      ? {
          color: 'error',
          children: 'Error loading data'
        }
      : undefined,
    state: {
      columnFilters,
      globalFilter,
      rowSelection: rowSelection,
      isLoading: props.isLoading,
      pagination,
      showAlertBanner: props.isError,
      showProgressBars: props.isRefetching,
      columnVisibility,
      expanded,
    },
    enableFullScreenToggle: false,
    positionToolbarAlertBanner:"bottom",
    rowCount: props.rowCount,
    getSubRows: originalRow => originalRow?.children,
    onPaginationChange: setPagination,
    onGlobalFilterChange: setGlobalFilter,
    onColumnFiltersChange: setColumnFilters,
    onRowSelectionChange: setRowSelection,
    onColumnVisibilityChange: setColumnVisibility,
    onExpandedChange:setExpanded
  })

  return <MaterialReactTable table={table} />
}

function CustomTable(props: IProps) {
  const filter = useContext(TableFilterContext)
  const { globalFilter, pagination } = filter

  return (
    <GetContainer
      url={props.url}
      hideLoading
      params={{
        search: globalFilter,
        page: pagination.pageIndex + 1,
        size: pagination.pageSize,
        ...props.params
      }}
    >
      {({ data, isError, isLoading, isFetching }) => {
        return (
          <Box position={'relative'}>
            {props.showExport && (
              <Box width={'100%'} display={'flex'} alignItems={'flex-start'} py={'1rem'} px={'1rem'} justifyContent={'flex-start'} position={"absolute"}  zIndex={99}>
                <TableExportComponent
                  columns={props.columns}
                  data={data?.results}
                  exportOptions={props.exportOptions ? props.exportOptions : {
                    csv: true,
                    excel: true,
                    pdf: true,
                    word: true
                  }}
                />
              </Box>
            )}
            <Table
              isError={isError}
              isLoading={isLoading}
              isRefetching={isFetching}
              data={data?.results || []}
              columns={props.columns}
              disablePagination={!!props.disablePagination}
              onClick={props.onClick}
              rowCount={data?.total}
              disableBodyClick={props.disableBodyClick}
              columnVisibilityData={props.columnVisibilityData ? props.columnVisibilityData : {}}
              enableExpanding={props.enableExpanding}
            />
          </Box>
        )
      }}
    </GetContainer>
  )
}

export default CustomTable
