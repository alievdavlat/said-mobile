import React, { ReactNode } from 'react'
import { Button, Modal } from '@mui/material'
import { styled, css } from '@mui/system'
import { grey } from '@mui/material/colors'
import { Icon } from '@iconify/react'
import Translations from 'src/components/translations'

export const CustomModal = (props: { open: boolean; onClose: () => void; children: ReactNode; title?: any}) => {
  return (
    <Modal
      open={props.open}
      disableRestoreFocus
      disableAutoFocus
      disableEnforceFocus
      onClose={props.onClose}
      sx={{ flex: 1, display: 'flex', alignItems: 'center', justifyContent: 'center' }}
    >
      <ModalContent sx={{ width: 'auto' }}>
        <ModalCloseWrapper>
          <Button variant='text' onClick={props.onClose}>
            <Icon fontSize={'1.5rem'} icon={'material-symbols:close'} />
          </Button>
        </ModalCloseWrapper>
        
        <ModalTitle>{props.title ? <Translations text={props.title}/> : ''}</ModalTitle>
        {props.children}
      </ModalContent>
    </Modal>
  )
}
const ModalContent = styled('div')(
  ({ theme }) => css`
    font-family: 'IBM Plex Sans', sans-serif;
    font-weight: 500;
    text-align: start;
    position: relative;
    display: flex;
    flex-direction: column;
    gap: 8px;
    overflow: hidden;
    background-color: ${theme.palette.mode === 'dark' ? '#262a3d' : '#fff'};
    border-radius: 8px;
    box-shadow: 0 4px 12px ${theme.palette.mode === 'dark' ? 'rgb(0 0 0 / 0.5)' : 'rgb(0 0 0 / 0.2)'};
    padding: 24px;
    color: ${theme.palette.mode === 'dark' ? grey[50] : grey[900]};
  `
)

const ModalTitle = styled('h2')(
  ({ theme }) => css`
    font-family: 'IBM Plex Sans', sans-serif;
    font-weight: 600;
    font-size:1.5rem;
    display: flex;
    text-align: start;
    align-items:center;
    justify-content:center;
    color: ${theme.palette.mode === 'dark' ? grey[50] : grey[900]};
  `
)

const ModalCloseWrapper = styled('div')(
  ({ theme }) => css`
    font-family: 'IBM Plex Sans', sans-serif;
    font-weight: 500;
    position: absolute;
    top: 1rem;
    right: 1rem;
    color: ${theme.palette.mode === 'dark' ? grey[50] : grey[900]};
  `
)
