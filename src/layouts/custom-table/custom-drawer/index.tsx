import { Drawer } from "@mui/material";
import React, { ReactNode } from "react";

export const CustomDrawer = (props: { open: boolean; onClose: () => void; children: ReactNode }) => {
  return (
    <Drawer
      open={props.open}
      anchor='right'
      onClose={props.onClose}
      ModalProps={{ keepMounted: false, disableEnforceFocus: true }}
      sx={{ '& .MuiDrawer-paper': { width: { xs: '85%', sm: '85%', md:'50%' } } }}
    >
      {props.children}
    </Drawer>
  )
}
