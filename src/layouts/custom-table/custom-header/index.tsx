import { Delete } from '@mui/icons-material'
import { Button, Stack, Typography } from '@mui/material'
import React, { ReactNode, useContext, useRef } from 'react'
import useQueryParams from 'src/hooks/useQueryParams'
import Translations from 'src/layouts/components/Translations'
import FileDownloadIcon from '@mui/icons-material/FileDownload'
import { TableFilterContext } from 'src/context/TableFilterContext'
import { useMutation } from '@tanstack/react-query'
import { hanldeRequest } from '../../../configs/req'
import { RandomContext } from '../../../context/RandomContext'
import Icon from 'src/@core/components/icon'

interface IProps {
  leftButtons?: ReactNode
  rightButtons?: ReactNode
  pageName: string
  handleExport?: () => void
  handleImport?: () => void
  hideAdd?: boolean
  deleteUrl?: string
  hideDelete?: boolean
}

function CustomHeader(props: IProps) {
  const query = useQueryParams()
  const inputFile = useRef<HTMLInputElement>(null)
  // @ts-ignore
  const { setRandom } = useContext(RandomContext)
  const { rowSelection, setRowSelection } = useContext(TableFilterContext)
  
  const { mutate } = useMutation(
    async () => {
      const response = await hanldeRequest({
        url: props.deleteUrl,
        method: 'DELETE',
        data: {ids:Object.keys(rowSelection || {})}
      })
      
      return response?.data
    },
    {
      onSuccess() {
        setRandom(Math.random())
        //@ts-ignore
        setRowSelection({})
      }
    }
  )

  return (
    <Stack spacing={6} direction={'row'} flexWrap={'wrap'} columnGap={5} alignItems={'center'} justifyContent={'space-between'} mb={2}>
      <Stack direction={'row'} spacing={2}>
        <Typography typography={'h4'}>
          <Translations text={props.pageName || ''} />
        </Typography>
        {props.leftButtons}
      </Stack>

      <Stack direction={'row'} spacing={2}  flexWrap={'wrap'} rowGap={5} alignItems={'center'} >
        {!!props?.rightButtons && props.rightButtons}
        {props.handleExport && (
          <Button
            color='info'
            onClick={props.handleExport}
            startIcon={<FileDownloadIcon />}
            variant='outlined'
          >
            <Translations text='Export' />
          </Button>
        )}
        {props.handleImport && (
          <>
            <input
              type='file'
              id='file'
              ref={inputFile}
              onChange={e => {
                e.preventDefault()
                //@ts-ignore
                props.handleImport(e?.target?.files?.[0])
              }}
              style={{ display: 'none' }}
            />
            <Button
              color='info'
              onClick={() => inputFile?.current?.click()}
              startIcon={<FileDownloadIcon />}
              variant='contained'
            >
              
              <Translations text='Import' />
            </Button>
          </>
        )}
        {!props.hideAdd && (
          <Button
            onClick={() => {
              query.set('onAdd', 'true')
            }}
            variant='outlined'
          >
             <Icon fontSize='1.125rem' icon='tabler:plus' />
            <Translations text='Add' />
          </Button>
        )}
        {props.deleteUrl  && (
          <Button
            color='error'
            onClick={() => mutate()}
            startIcon={<Delete />}
            variant='outlined'
          >
            <Translations text='Delete' />
          </Button>
        )}
      </Stack>
    </Stack>
  )
}

export default CustomHeader
