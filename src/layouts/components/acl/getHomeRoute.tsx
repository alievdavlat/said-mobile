
const getHomeRoute = (role: string) => {
  if (role === 'client') return '/acl'
  else return '/home'
}

export default getHomeRoute
