import { ReactNode } from 'react'
import { NavLink } from 'src/@core/layouts/types'
import { useAuth } from 'src/hooks/useAuth'

interface Props {
  navLink?: NavLink
  children: ReactNode
}

const CanViewNavLink = (props: Props) => {
  const { children, navLink } = props
  const auth = useAuth()

  // Agar foydalanuvchi superuser bo'lsa, children ni qaytarish
  if (auth.user?.is_superuser) {
    return <>{children}</>
  }

  if (navLink && navLink.auth === false) {
    return <>{children}</>
  } else {
    return navLink && auth?.user?.access_urls?.some(item => item === navLink?.subject) ? (
      <>{children}</>
    ) : auth?.user?.is_superuser ? (
      <>{children}</>
    ) : null
  }
}

export default CanViewNavLink
