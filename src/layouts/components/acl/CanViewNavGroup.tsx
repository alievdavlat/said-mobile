// ** React Imports
import { ReactNode } from 'react'


// ** Types
import { NavGroup, NavLink } from 'src/@core/layouts/types'
import { PermissionsProps } from 'src/context/types'
import { useAuth } from 'src/hooks/useAuth'

interface Props {
  navGroup?: NavGroup
  children: ReactNode
  perrmissonItem?:PermissionsProps
}

const CanViewNavGroup = (props: Props) => {
  const { children, navGroup } = props
  const auth  = useAuth()

  const checkForVisibleChild = (arr: NavLink[] | NavGroup[]): boolean => {
    return arr.some((i: NavGroup) => {
      if (i.children) {
        return checkForVisibleChild(i.children)
      } else {
        return auth?.user?.access_urls?.some(url => url === i?.subject)
      }
    })
  }

  const canViewMenuGroup = (item: NavGroup) => {
    const hasAnyVisibleChild = item.children && checkForVisibleChild(item.children)


    return item && auth?.user?.access_urls?.some(url => url === item?.subject) && hasAnyVisibleChild
  }

    return navGroup && canViewMenuGroup(navGroup) ? <>{children}</> : null
}

export default CanViewNavGroup
