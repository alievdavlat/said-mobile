import { MRT_ColumnDef } from 'material-react-table'
import React, { useMemo, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Box, Button, Typography } from '@mui/material'
import Translations from 'src/layouts/components/Translations'
import CustomHeader from '../../layouts/custom-table/custom-header'
import CustomTable from '../../layouts/custom-table'
import { CustomDrawer } from '../../layouts/custom-table/custom-drawer'
import useQueryParams from 'src/hooks/useQueryParams'
import moment from 'moment'
import Icon from 'src/@core/components/icon'
import dynamic from 'next/dynamic'
import { CustomModal } from 'src/layouts/custom-table/custom-modal'
import AnimationContainer from 'src/components/animations/anim-container/AnimAtionContainer'

const StockFilter = dynamic(() => import('src/components/forms/filters/StockFilter'), { ssr: false })
const StockForm = dynamic(() => import('src/components/forms/StockForm'), { ssr: false })

type dataProps = {
  name: string
  address: string
  part: boolean
  sell: boolean
  sklad: boolean
}

function StocksList() {
  const { t } = useTranslation()
  const query = useQueryParams()

  const selectionOption = ['Yes', 'No']

  const [sell, setSell] = useState<boolean | string>('')
  const [part, setPart] = useState<boolean | string>('')
  const [sklad, setSklad] = useState<boolean | string>('')

  const columns = useMemo<MRT_ColumnDef<dataProps>[]>(
    () => [
      {
        accessorKey: 'name',
        header: t('Store Name'),
        size: 250,
        enableGlobalFilter: true
      },
      {
        accessorKey: 'address',
        header: t('Address'),
        size: 250,
        enableGlobalFilter: true
      },
      {
        accessorKey: 'sklad',
        header: t('Sklad'),
        enableGlobalFilter: true,
        filterVariant: 'select',
        filterSelectOptions: selectionOption,
        size: 200,
        Cell: ({ cell }) => <Typography>{String(cell.getValue() === true ? t('Yes') : t('No'))}</Typography>
      },
      {
        accessorKey: 'sell',
        header: t('Sell'),
        enableGlobalFilter: true,
        filterVariant: 'select',
        filterSelectOptions: selectionOption,
        size: 200,
        Cell: ({ cell }) => <Typography>{String(cell.getValue() === true ? t('Yes') : t('No'))}</Typography>
      },
      {
        accessorKey: 'part',
        header: t('Part'),
        enableGlobalFilter: true,
        filterVariant: 'select',
        filterSelectOptions: selectionOption,
        size: 200,
        Cell: ({ cell }) => <Typography>{String(cell.getValue() === true ? t('Yes') : t('No'))}</Typography>
      },
      {
        accessorKey: 'created_at',
        header: t('Created at'),
        size: 200,
        Cell: ({ cell }) => <Typography>{moment(String(cell.getValue())).format('LLL')}</Typography>
      },
      {
        accessorKey: 'id',
        header: t('Actions'),
        enableColumnFilter: false,
        enableGlobalFilter: false,
        enableSorting: false,
        Cell: () => (
          <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: 2 }}>
            <Button variant='text' color='primary'>
              <Icon icon='tabler:edit' fontSize={20} />
              <Translations text='Edit' />
            </Button>
          </Box>
        ),
        size: 150 //decrease the width of this column
      }
    ],
    [t]
  )
  const RightButtons = () => {
    return (
      <Button variant='contained' onClick={() => query.set('filter', 'true')}>
        {t('Filter')}
      </Button>
    )
  }

  const animOne = {
    x: -100,
    opacity: 0
  }

  const animTwo = {
    opacity: 1,
    x: 0,
    transition: 0.2,
    delay: 0.2,
    stagger: 2
  }

  return (
    <AnimationContainer animOne={animOne} animTwo={animTwo}>
      <Box>
        <CustomHeader pageName='Stocks' deleteUrl='/stock/delete' rightButtons={<RightButtons />} />
        <CustomTable
          url='/stock/list'
          columns={columns}
          rowIdKey='id'
          params={{
            sell,
            part,
            sklad
          }}
        />
        <CustomDrawer
          open={query.has('id') || query.has('onAdd')}
          onClose={() => {
            if (query.has('id')) {
              query.remove('id')
            } else if (query.has('onAdd')) {
              query.remove('onAdd')
            }
          }}
        >
          <StockForm />
        </CustomDrawer>

        <CustomModal
          open={query.has('filter')}
          onClose={() => {
            if (query.has('filter')) {
              query.remove('filter')
            }
          }}
        >
          <StockFilter part={part} sell={sell} setPart={setPart} setSell={setSell} setSklad={setSklad} sklad={sklad} />
        </CustomModal>
      </Box>
    </AnimationContainer>
  )
}

export default StocksList
