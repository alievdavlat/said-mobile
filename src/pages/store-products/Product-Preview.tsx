// ** MUI Imports
import Grid from '@mui/material/Grid'
import Card from '@mui/material/Card'
import Table from '@mui/material/Table'
import Divider from '@mui/material/Divider'
import TableRow from '@mui/material/TableRow'
import TableBody from '@mui/material/TableBody'
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box'
import CardContent from '@mui/material/CardContent'
import { styled, useTheme } from '@mui/material/styles'
import TableCell, { TableCellBaseProps } from '@mui/material/TableCell'

// ** Configs
import themeConfig from 'src/configs/themeConfig'

// ** Types
import GetContainer from 'src/components/get-container'
import useQueryParams from 'src/hooks/useQueryParams'
import Translations from 'src/components/translations'

const MUITableCell = styled(TableCell)<TableCellBaseProps>(({ theme }) => ({
  borderBottom: 0,
  paddingLeft: '0 !important',
  paddingRight: '0 !important',
  '&:not(:last-child)': {
    paddingRight: `${theme.spacing(2)} !important`
  }
}))

type dataProps = {
  id: number
  product: {
    id: number
    name: string
    brand: {
      id: number
      name: string
    }
    category: {
      id: number
      name: string
    }
    type: {
      id: number
      name: string
    }
  }
  store: {
    id: number
    name: string
  }
  created_at: string
  updated_at: string
  quantity: number
  average_price: string
  bonus: string
}

const ProductPreviewCard = () => {
  // ** Hook
  const theme = useTheme()
  const query = useQueryParams()

  return (
    <GetContainer url={`/stock/product-info/${query.get('id')}`}>
      {({ data, isError, isLoading }: { data: dataProps; isError: any; isLoading: any }) => {
        if (data && !isLoading) {
          return (
            <Card>
              <CardContent sx={{ p: [`${theme.spacing(6)} !important`, `${theme.spacing(10)} !important`] }}>
                <Grid container>
                  <Grid item sm={6} xs={12} sx={{ mb: { md: 8, xs: 4 } }}>
                    <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                      <Typography variant='h4' sx={{fontWeight: 700, lineHeight: '24px' }}>
                        {themeConfig.templateName}
                      </Typography>
                    </Box>
                  </Grid>
              
                  <Divider />

                  <Grid item xs={12}>
                    <Table>
                      <TableBody sx={{ '& .MuiTableCell-root': { py: `${theme.spacing(1.5)} !important` } }}>
                        <TableRow>
                          <MUITableCell>
                            <Typography variant='h4'>
                              <Translations text='Product' />
                            </Typography>
                          </MUITableCell>
                        </TableRow>
                        <br />
                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Product' /> :{' '}{`${data.product.name}`}
                          </Typography>
                        </TableRow>
                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Brand Name' /> : {' '}{data.product.brand.name} 
                          </Typography>
                        </TableRow>
                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Type Name' /> : {data.product.type.name}
                          </Typography>
                        </TableRow>

                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Category' /> : {data.product.category.name}
                          </Typography>
                        </TableRow>
                      </TableBody>
                    </Table>
                  </Grid>
                </Grid>
              </CardContent>

              <Divider />

              <CardContent sx={{ p: [`${theme.spacing(6)} !important`, `${theme.spacing(10)} !important`] }}>
                <Grid container>
                  <Grid item xs={12} md={12} sx={{ mb: { lg: 0, xs: 4 } }}>
                    <Typography variant='h4' sx={{ mb: 6 }}>
                      <Translations text='Info' />
                    </Typography>
                    <Typography sx={{ mb: 1.5, color: 'text.secondary' }}>
                      <Translations text='Store Name' /> : {data.store.name}
                    </Typography>
                    <Typography sx={{ mb: 1.5, color: 'text.secondary' }}>
                      {' '}
                      <Translations text='Average Price' /> : {data.average_price}
                    </Typography>
                    <Typography sx={{ mb: 1.5, color: 'text.secondary' }}>
                      <Translations text='Bonus' /> : <Translations text={data.bonus ? 'Yes' : 'No'} />
                    </Typography>
                    <Typography sx={{ mb: 1.5, color: 'text.secondary' }}>
                      <Translations text='Quantity' /> : {data.quantity}
                    </Typography>
                    <Typography sx={{ mb: 1.5, color: 'text.secondary' }}>
                      <Translations text='Created at' /> : {data.created_at}
                    </Typography>
                    <Typography sx={{ mb: 1.5, color: 'text.secondary' }}>
                      <Translations text='Updated At' /> : {data.updated_at}
                    </Typography>
                  </Grid>
                </Grid>
              </CardContent>

            </Card>
          )
        } else if (isError) {
          return (
            <Typography>
              <Translations text='Something Went Wrong' />
            </Typography>
          )
        } else {
          return (
            <Typography>
              <Translations text='Data Not Found!' />
            </Typography>
          )
        }
      }}
    </GetContainer>
  )
}

export default ProductPreviewCard
