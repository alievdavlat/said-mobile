import { MRT_ColumnDef } from 'material-react-table'
import React, { useMemo, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Box, Button } from '@mui/material'
import CustomHeader from '../../layouts/custom-table/custom-header'
import CustomTable from '../../layouts/custom-table'
import moment from 'moment'
import useQueryParams from 'src/hooks/useQueryParams'
import { CustomDrawer } from 'src/layouts/custom-table/custom-drawer'
import { useAuth } from 'src/hooks/useAuth'
import { CustomModal } from 'src/layouts/custom-table/custom-modal'
import dynamic from 'next/dynamic'
import StockProductsFilter from 'src/components/forms/filters/StockProductFIlter'
import AnimationContainer from 'src/components/animations/anim-container/AnimAtionContainer'
import Icon from 'src/@core/components/icon'
import ProductPreviewCard from './Product-Preview'


const TransFerCreateForm = dynamic(() => import('src/components/forms/TransferCreateForm'), { ssr: false })

type dataProps = {
  id: number
  product: {
    id: number
    name: string
    brand: {
      id: number
      name: string
    }
    category: {
      id: number
      name: string
    }
    type: {
      id: number
      name: string
    }
  }
  store: {
    id: number
    name: string
  }
  created_at: string
  updated_at: string
  quantity: number
  average_price: string
  bonus: string
}

function StoreProducts() {
  const { t } = useTranslation()
  const query = useQueryParams()
  const [brand, setBrand] = useState('')
  const [enddate, setEndDate] = useState('')
  const [category, setCategory] = useState('')
  const [region, setRegion] = useState('')
  const [startdate, setStartDate] = useState('')
  const [type, setType] = useState('')

  const auth = useAuth()

  const RightButtons = () => {
    return (
      <Box sx={{ display: 'flex', alignItems: 'center', gap: '1.2rem' }}>
        <Button variant='contained' onClick={() => query.set('filter', 'true')}>
          {t('Filter')}
        </Button>
        <Button variant='contained' onClick={() => query.set('addSwope', 'true')}>
          {t('Transfer')}
        </Button>
      </Box>
    )
  }

  

  const columns = useMemo<MRT_ColumnDef<dataProps>[]>(
    () => [
      {
        accessorKey: 'id',
        header: t('Actions'),
        enableColumnFilter: false,
        enableGlobalFilter: false,
        enableSorting: false,
        Cell: () => (
          <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center', gap: 2 }}>
              <Button variant='outlined' color='info' >
                <Icon icon='ph:eye' fontSize={20} /> 
                {t("View")}
              </Button>
          </Box>
        ),
        size: 150
      },
      {
        accessorFn: row => `${row.store.name}`,
        accessorKey: 'store_name',
        header: t('Store'),
        enableGlobalFilter: true,
        size: 150,
        Cell: ({ renderedCellValue }: { renderedCellValue: any }) => (
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: '1rem'
            }}
          >
            <span>{renderedCellValue}</span>
          </Box>
        )
      },
      {
        accessorFn: row => `${row.product?.name}`,
        accessorKey: 'name',
        header: t('Product'),
        enableGlobalFilter: true,
        size: 150,
        Cell: ({ renderedCellValue }: { renderedCellValue: any }) => (
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: '1rem'
            }}
          >
            <span>{renderedCellValue}</span>
          </Box>
        )
      },
      {
        accessorFn: row => `${row.product.brand.name}`,
        accessorKey: 'brand_name',
        header: t('Brand'),
        enableGlobalFilter: true,
        size: 150,
        Cell: ({ renderedCellValue }: { renderedCellValue: any }) => (
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: '1rem'
            }}
          >
            <span>{renderedCellValue}</span>
          </Box>
        )
      },
      {
        accessorFn: row => `${row.product.category.name}`,
        accessorKey: 'category_name',
        header: t('Category'),
        enableGlobalFilter: true,
        size: 150,
        Cell: ({ renderedCellValue }: { renderedCellValue: any }) => (
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: '1rem'
            }}
          >
            <span>{renderedCellValue}</span>
          </Box>
        )
      },

      {
        accessorFn: row => `${row.product.type.name}`,
        accessorKey: 'type_name',
        header: t('Type'),
        enableGlobalFilter: true,
        size: 150,
        Cell: ({ renderedCellValue }: { renderedCellValue: any }) => (
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: '1rem'
            }}
          >
            <span>{renderedCellValue}</span>
          </Box>
        )
      },

      {
        accessorFn: row => `${row.average_price}`,
        accessorKey: 'average_price',
        header: t('Average Price'),
        enableGlobalFilter: true,
        size: 150,
        Cell: ({ renderedCellValue }: { renderedCellValue: any }) => (
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: '1rem'
            }}
          >
            <span>{renderedCellValue === 'null' ? 'No Average' : renderedCellValue}</span>
          </Box>
        )
      },
      {
        accessorFn: row => `${row.bonus}`,
        accessorKey: 'bonus',
        header: t('Bonus'),
        enableGlobalFilter: true,
        size: 150,
        Cell: ({ renderedCellValue }: { renderedCellValue: any }) => (
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: '1rem'
            }}
          >
            <span>{renderedCellValue === 'null' ? 'No Bonus' : renderedCellValue}</span>
          </Box>
        )
      },

      {
        accessorFn: row => `${row.quantity}`,
        accessorKey: 'quantity',
        header: t('Quantity'),
        enableGlobalFilter: true,
        size: 150,
        Cell: ({ renderedCellValue }: { renderedCellValue: any }) => (
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: '1rem'
            }}
          >
            <span>{renderedCellValue}</span>
          </Box>
        )
      },
      {
        accessorFn: row => `${row.created_at}`,
        accessorKey: 'created_at',
        header: t('Created at'),
        enableGlobalFilter: true,
        size: 150,
        Cell: ({ renderedCellValue }: { renderedCellValue: any }) => (
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: '1rem'
            }}
          >
            <span>{moment(renderedCellValue).format('ll')}</span>
          </Box>
        )
      },
      
    ],
    [t]
  )

  const animOne = {
    x: -100,
    opacity: 0
  }

  const animTwo = {
    opacity: 1,
    x: 0,
    transition: 0.2,
    delay: 0.2,
    stagger: 2
  }

  return (
    <AnimationContainer animOne={animOne} animTwo={animTwo}>
      <Box>
        <CustomHeader pageName='Store Products' hideAdd rightButtons={<RightButtons />} />
        <CustomTable
          url='/stock/product-list'
          columns={columns}
          rowIdKey='id'
          params={{
            end_date: enddate,
            start_date: startdate,
            product__brand: brand,
            product__type: type,
            product__category: category,
            product__region: region,
            store:auth.user?.store
          }}
        />
        <CustomDrawer
          open={query.has('addSwope')}
          onClose={() => {
            if (query.has('addSwope')) {
              query.remove('addSwope')
            }
          }}
        >
          <TransFerCreateForm />
        </CustomDrawer>

        <CustomModal
          open={query.has('filter') || query.has('id')}
          onClose={() => {
            if (query.has('filter')) {
              query.remove('filter')
            } 

            if (query.has('id')) {
              query.remove('id')
            }
          }}
        >
          {
            query.has('id') && <ProductPreviewCard/>
          }
         {query.has('filter') &&  <StockProductsFilter
            brand={brand}
            enddate={enddate}
            setBrand={setBrand}
            setEndDate={setEndDate}
            setStartDate={setStartDate}
            setType={setType}
            startdate={startdate}
            type={type}
            category={category}
            setCategory={setCategory}
            region={region}
            setRegion={setRegion}
          />}
        </CustomModal>
      </Box>
    </AnimationContainer>
  )
}

export default StoreProducts
