import { Grid } from '@mui/material'
import React from 'react'
import LineChart2 from '../components/charts/LineChart2'
import AreaChart from '../components/charts/AreaChart'

const AnalyticsSection = () => {
  return (
    <Grid container spacing={6}>
      {/* chart 1 */}
      <Grid item xs={12}>
        <LineChart2/>
      </Grid>

      {/* chart 2*/}
      <Grid item xs={12}>
        <AreaChart/>
      </Grid>

      
    </Grid>
  )
}

export default AnalyticsSection