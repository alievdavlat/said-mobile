import { Grid } from '@mui/material'
import React from 'react'
import CountsCards from '../components/CountsCards/CountsCards'
import UserCard from '../components/UserCard'
import AnalyticsSlider from '../components/AnalyticsSlider'
import TransactionView from '../components/TransactionView'
import TransferView from '../components/TransferView'
import { useAuth } from 'src/hooks/useAuth'

const InfoSection = () => {
  const auth = useAuth()

  

  return (
    <Grid container spacing={6}>
      {/* Counts */}
      <Grid item xs={12}>
        <CountsCards />
      </Grid>

      {/* Uswr and basic analytics */}
      <Grid item xs={12} sm={6}>
        <UserCard />
      </Grid>

      <Grid item xs={12} sm={6}>
        <AnalyticsSlider />
      </Grid>
     
      {!auth.user?.is_superuser && (
        <Grid item xs={12} sm={6}>
          <TransactionView />
        </Grid>
      )}
      {!auth.user?.is_superuser && (
        <Grid item xs={12} sm={6}>
          <TransferView />
        </Grid>
      )}
    </Grid>
  )
}

export default InfoSection
