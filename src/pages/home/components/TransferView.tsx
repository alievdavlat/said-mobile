// ** MUI Imports
import Card from '@mui/material/Card'
import Table from '@mui/material/Table'
import TableRow from '@mui/material/TableRow'
import TableBody from '@mui/material/TableBody'
import TableHead from '@mui/material/TableHead'
import TableCell from '@mui/material/TableCell'
import Typography from '@mui/material/Typography'
import CardHeader from '@mui/material/CardHeader'
import TableContainer from '@mui/material/TableContainer'


// ** Custom Components Imports
import GetContainer from 'src/components/get-container'
import { useAuth } from 'src/hooks/useAuth'
import { useTranslation } from 'react-i18next'


type NotificationsType2 = {
  id: number
  product: {
    id: number
    name: string
    brand: {
      id: number
      name: string
    }
    category: {
      id: number
      name: string
    }
    type: {
      id: number
      name: string
    }
  }
  status: boolean
  count: number
  from_store: {
    id: number
    name: string
  }
  to_store: {
    id: number
    name: string
  }

}

const TransferView = () => {
  const auth = useAuth()
  const {t} = useTranslation()
  
  return (
    <GetContainer url={`/stock/transfer/${auth.user?.is_superuser ? 0 : auth.user?.store}`}>
      {({data,  isSuccess }) => {
        return (
          <Card>
            <CardHeader
              title={t('Transfers')}           

            />
            <TableContainer>
              <Table>
                <TableHead>
                  <TableRow
                    sx={{ '& .MuiTableCell-root': { py: 2, borderTop: theme => `1px solid ${theme.palette.divider}` } }}
                  >
                    <TableCell>{t('Brand Name')}</TableCell>
                    <TableCell>{t('Product Name')}</TableCell>
                    <TableCell>{t('Count')}</TableCell>
                    <TableCell>{t('Category Name')}</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {isSuccess &&  data?.results?.map((row: NotificationsType2) => {
                    return (
                      <TableRow
                        key={row.id}
                        sx={{
                          '&:last-child .MuiTableCell-root': { pb: theme => `${theme.spacing(6)} !important` },
                          '& .MuiTableCell-root': { border: 0, py: theme => `${theme.spacing(2.25)} !important` },
                          '&:first-of-type .MuiTableCell-root': { pt: theme => `${theme.spacing(4.5)} !important` }
                        }}
                      >
                        <TableCell>
                              <Typography noWrap sx={{ fontWeight: 500, color: 'text.secondary' }}>
                                {row.product.brand.name}
                              </Typography>
                              
                        </TableCell>
                        <TableCell>
                              <Typography noWrap variant='body2' sx={{ color: 'text.disabled' }}>
                                {row.product.name}
                              </Typography>
                        </TableCell>
                        <TableCell>
                            <Typography noWrap variant='body2' sx={{ color: 'text.disabled' }}>
                              {row.count}
                            </Typography>
                        </TableCell>
                      
                        <TableCell>
                          <Typography noWrap sx={{ fontWeight: 500, color: 'text.secondary' }}>
                            {row.product.category.name}
                          </Typography>
                        </TableCell>
                      </TableRow>
                    )
                  })}
                </TableBody>
              </Table>
            </TableContainer>
          </Card>
        )
      }}
    </GetContainer>
  )
}

export default TransferView
