// ** MUI Imports
import Card from '@mui/material/Card'
import { styled } from '@mui/material/styles'
import Typography from '@mui/material/Typography'
import CardContent from '@mui/material/CardContent'
import { useAuth } from 'src/hooks/useAuth'
import Translations from 'src/components/translations'

const Illustration = styled('img')(({ theme }) => ({
  right: 20,
  bottom: 0,
  position: 'absolute',
  [theme.breakpoints.down('sm')]: {
    right: 5,
    width: 110
  }
}))

const UserCard = () => {
  const auth = useAuth()
  
  return (
    <Card sx={{ position: 'relative' , p:{sm:7, xs:0}}}>
      <CardContent>
        <Typography variant='h5' sx={{ mb: 0.5 }}>
          {auth.user?.firstname || 'John'}{' '}{auth.user?.lastname || 'Doe'}! 🎉
        </Typography>
        <Typography variant='body2' sx={{ mb: 2, color: 'text.secondary' }}>{auth.user?.is_superuser ? 'super admin' : auth.user?.role.name}</Typography>
        <Typography variant='h4' sx={{ mb: 0.5, color: 'primary.main' }}>
          <Translations text='User Balance'/> : {' '}{auth.userBalance.detail}
        </Typography>
        <Typography variant='h4' sx={{ mb: 0.5, color: 'primary.main' }}>
          <Translations text='Store Balance'/> : {' '}{auth.storeBalance.balance}
        </Typography>
        <Illustration sx={{display:{xs:'none', sm:'block'}}} width={116} alt='congratulations john' src='/images/cards/congratulations-john.png' />
      </CardContent>
    </Card>
  )
}

export default UserCard
