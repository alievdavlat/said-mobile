// ** MUI Imports
import Box from '@mui/material/Box'
import Card from '@mui/material/Card'
import Table from '@mui/material/Table'
import TableRow from '@mui/material/TableRow'
import TableBody from '@mui/material/TableBody'
import TableHead from '@mui/material/TableHead'
import TableCell from '@mui/material/TableCell'
import Typography from '@mui/material/Typography'
import CardHeader from '@mui/material/CardHeader'
import TableContainer from '@mui/material/TableContainer'

// ** Types

// ** Custom Components Imports
import GetContainer from 'src/components/get-container'
import { useAuth } from 'src/hooks/useAuth'
import { useTranslation } from 'react-i18next'





export type NotificationsType = {
  id: number
  created_at: string
  updated_at: string
  price: string
  status: boolean
  from_store: number
  to_store: number
}

const TransactionView = () => {
  const auth = useAuth()
  const {t} = useTranslation()
  
  return (
    <GetContainer url={`/stock/transaction/${auth.user?.is_superuser ? 0 : auth.user?.store}`}>
    {({data,  isSuccess }) => {
      return (
        <Card>
          <CardHeader
            title={t('Transactions')}           
          />
          <TableContainer>
            <Table>
              <TableHead>
                <TableRow
                  sx={{ '& .MuiTableCell-root': { py: 2, borderTop: theme => `1px solid ${theme.palette.divider}` } }}
                >
                  <TableCell>{t('Price')}</TableCell>
                  <TableCell>{t('Created at')}</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {isSuccess &&  data?.results?.map((row: NotificationsType) => {
                  return (
                    <TableRow
                      key={row.id}
                      sx={{
                        '&:last-child .MuiTableCell-root': { pb: theme => `${theme.spacing(6)} !important` },
                        '& .MuiTableCell-root': { border: 0, py: theme => `${theme.spacing(2.25)} !important` },
                        '&:first-of-type .MuiTableCell-root': { pt: theme => `${theme.spacing(4.5)} !important` }
                      }}
                    >
                      <TableCell>
                        <Box sx={{ display: 'flex', alignItems: 'center', '& img': { mr: 4 } }}>
                            <Typography noWrap sx={{ fontWeight: 500, color: 'text.secondary' }}>
                              {row.price}
                            </Typography>
                        </Box>
                      </TableCell>
                      
                      <TableCell>
                        <Box sx={{ display: 'flex', alignItems: 'center', '& img': { mr: 4 } }}>
                            <Typography noWrap variant='body2' sx={{ color: 'text.disabled' }}>
                              {row.created_at}
                            </Typography>
                        </Box>
                      </TableCell>
                      
                    </TableRow>
                  )
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </Card>
      )
    }}
  </GetContainer>
  )
}

export default TransactionView
