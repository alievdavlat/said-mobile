import { Grid } from '@mui/material'
 import React from 'react'
import CardStatsHorizontal from 'src/@core/components/card-statistics/card-stats-horizontal'
import { CardStatsHorizontalProps } from 'src/@core/components/card-statistics/types'




const CountsCards = () => {
  const data:CardStatsHorizontalProps[] = [
    {
      stats: '86%',
      icon: 'tabler:cpu',
      title: 'CPU Usage'
    },
    {
      stats: '1.24gb',
      icon: 'tabler:server',
      title: 'Memory Usage',
      avatarColor: 'success'
    },
    {
      stats: '0.2%',
      avatarColor: 'error',
      title: 'Downtime Ratio',
      icon: 'tabler:chart-pie-2'
    },
    {
      stats: '128',
      title: 'Issues Found',
      avatarColor: 'warning',
      icon: 'tabler:alert-octagon'
    }
  ]
  
  const renderData = data
  ? data.map((item: CardStatsHorizontalProps, index: number) => (
      <Grid item xs={12} sm={6} md={3} key={index}>
        <CardStatsHorizontal {...item} />
      </Grid>
    ))
  : null

return (
  <Grid container spacing={6}>
    {renderData}
  </Grid>
)
}

export default CountsCards

