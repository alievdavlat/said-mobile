import React, { useState } from 'react'
import { SyntheticEvent } from 'react'
import Icon from 'src/@core/components/icon'
import { useTranslation } from 'react-i18next'
// ** MUI Imports
import Tab from '@mui/material/Tab'
import TabList from '@mui/lab/TabList'
import TabPanel from '@mui/lab/TabPanel'
import TabContext from '@mui/lab/TabContext'
import InfoSection from './sections/InfoSection'
import AnalyticsSection from './sections/AnalyticsSection'
import ApexChartWrapper from 'src/@core/styles/libs/react-apexcharts'
import KeenSliderWrapper from 'src/@core/styles/libs/keen-slider'

const Home = () => {
  // ** States
  const [value, setValue] = useState<string>('1')

  // ** Hook
  const { t } = useTranslation()

  // ** Handlers
  const handleChange = (event: SyntheticEvent, newValue: string) => {
    setValue(newValue)
  }

  
  
  return (
    <ApexChartWrapper>
      <KeenSliderWrapper>
        <br />
        <br />

        <TabContext value={value}>
          <TabList scrollButtons variant='fullWidth' onChange={handleChange} aria-label='forced scroll tabs example'>
            <Tab value='1' label={t('Info')} icon={<Icon icon='mage:dashboard-chart-arrow-fill' />} />
            <Tab value='2' label={t('Analytics')} icon={<Icon icon='carbon:analytics' />} />
          </TabList>
          <br />
          <br />
          <TabPanel value='1'>
            <InfoSection />
          </TabPanel>

          <TabPanel value='2'>
            <AnalyticsSection />
          </TabPanel>
        </TabContext>
      </KeenSliderWrapper>
    </ApexChartWrapper>
  )
}

export default Home
