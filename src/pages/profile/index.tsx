//@ts-nocheck
// ** MUI Components
import Grid from '@mui/material/Grid'


// ** Icon Imports

// ** Demo Components
import { useAuth } from 'src/hooks/useAuth'
import UserProfileHeader from './UserProfileHeader'
import Profile from './Profile'

const UserProfile = () => {
  const auth = useAuth()

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <UserProfileHeader avatar={auth.user?.photo ? auth.user?.photo : '/images/avatars/11.png'} role={auth.user?.role.name} fullname={auth?.user?.firstname + ' ' + auth?.user?.lastname} />
      </Grid>
      <Profile data={auth.user} />
    </Grid>
  )
}

export default UserProfile
