// ** MUI Components
import Box from '@mui/material/Box'
import Grid from '@mui/material/Grid'
import Card from '@mui/material/Card'
import Typography from '@mui/material/Typography'
import CardContent from '@mui/material/CardContent'

// ** Icon Imports
import Icon from 'src/@core/components/icon'
import { UserDataType } from 'src/context/types'
import Translations from 'src/components/translations'
import { useTranslation } from 'react-i18next'

// ** Types

type Props = {
  data: UserDataType
}

const renderData = (data:any, icon:string) => {
  if (data) {
      return (
        <Box
          sx={{
            display: 'flex',
            '&:not(:last-of-type)': { mb: 3 },
            '& svg': { color: 'text.secondary' }
          }}
        >
          <Box sx={{ columnGap: 2, display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
            <Typography sx={{ fontWeight: 500, color: 'text.secondary' }}>
            <Icon fontSize='1.25rem' icon={icon} />
            </Typography>
            <Typography sx={{ color: 'text.secondary' }}>
              {data}
            </Typography>
          </Box>
        </Box>
      )
  } else {
    return null
  }
}



const AboutOverivew = (props: Props) => {
  const { data } = props
  const {t} = useTranslation()


  return (
    <Grid container spacing={6}>
      <Grid item xs={6}>
        <Card>
          <CardContent>
            <Box sx={{ mb: 6 }}>
              <Typography variant='body2' sx={{ mb: 4, color: 'text.disabled', textTransform: 'uppercase' }}>
                <Translations text='About'/>
              </Typography>
              {renderData(data?.firstname, 'wpf:name')}
              {renderData(data?.lastname, 'wpf:name')}
              {renderData(data?.surname, 'wpf:name')}
              {renderData(data?.login, 'wpf:name')}
              {renderData(data?.role?.name,  'wpf:name')}
            </Box>
            <Box sx={{ mb: 6 }}>
              <Typography variant='body2' sx={{ mb: 4, color: 'text.disabled', textTransform: 'uppercase' }}>
               <Translations text='Access'/>
              </Typography>
              <Box sx={{display:'flex', alignItems:'center', flexWrap:'wrap'}}>
              {
                data.permission?.map((i) => <div key={i.id}>{renderData(i?.name + ',', '')}</div>)
              }
              </Box>
            </Box>
          
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={6}>
        <Card>
          <CardContent>
            <Box sx={{ mb: 6 }}>
              <Typography variant='body2' sx={{ mb: 4, color: 'text.disabled', textTransform: 'uppercase' }}>
                <Translations text='Rent'/>
              </Typography>
             
              {renderData(data?.rent ? `${t( "Yes")}` : `${t('No')}`,  '')}
            </Box>
            <Box sx={{ mb: 6 }}>
              <Typography variant='body2' sx={{ mb: 4, color: 'text.disabled', textTransform: 'uppercase' }}>
                <Translations text='Bonus'/>
              </Typography>
             
              {renderData(data?.bonus ? `${t( "Yes")}` : `${t('No')}`,  '')}
            </Box>
            <Box sx={{ mb: 6 }}>
              <Typography variant='body2' sx={{ mb: 4, color: 'text.disabled', textTransform: 'uppercase' }}>
                <Translations text='Joined Date'/>
              </Typography>
              {
                renderData(data?.joined_date, 'uiw:date')
              }
            </Box>
          
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  )
}

export default AboutOverivew
