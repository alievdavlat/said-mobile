// ** MUI Components
import Grid from '@mui/material/Grid'

// ** Demo Components

// ** Types
import { UserDataType } from 'src/context/types'
import dynamic from 'next/dynamic'

const AboutOverivew = dynamic(() => import('./AboutOverivew'), {ssr:false})

interface Props {
  data:UserDataType
}
const Profile = ({ data }: Props) => {
  return data && Object.values(data).length ? (
    <Grid item lg={6} md={5} xs={12}>
      <AboutOverivew data={data} />
    </Grid>
  ) : null
}

export default Profile
