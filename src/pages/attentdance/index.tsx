import { MRT_ColumnDef } from 'material-react-table'
import React, { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { Box, Button } from '@mui/material'
import Translations from 'src/layouts/components/Translations'
import CustomTable from '../../layouts/custom-table'
import CustomHeader from '../../layouts/custom-table/custom-header'
import Icon from 'src/@core/components/icon'
import Link from 'next/link'
import AnimationContainer from 'src/components/animations/anim-container/AnimAtionContainer'

interface IUser {
  id: number
  firstname: string
  lastname: string
  surname: string
}
type dataProps = {
  user: IUser[]
  check_in: string
  check_out: string
  created_at: string
}

function Atentdance() {
  const { t } = useTranslation()

  const columns = useMemo<MRT_ColumnDef<dataProps>[]>(
    () => [
      {
        accessorKey: 'user.firstname',
        header: t('Firstname'),
        size: 250,
        enableGlobalFilter: true
      },
      {
        accessorKey: 'user.lastname',
        header: t('Lastname'),
        size: 250,
        enableGlobalFilter: true
      },
      {
        accessorKey: 'user.surname',
        header: t('Surname'),
        enableGlobalFilter: true,

        size: 250
      },

      {
        accessorKey: 'check_in',
        header: t('Check in'),
        enableGlobalFilter: true,
        size: 250
      },

      {
        accessorKey: 'check_out',
        header: t('Check out'),
        enableGlobalFilter: true,
        size: 250
      },
      {
        accessorKey: 'created_at',
        header: t('Date'),
        enableGlobalFilter: true,
        size: 250
      },
      {
        accessorKey: 'id',
        header: t('Actions'),
        enableColumnFilter: false,
        enableGlobalFilter: false,
        enableSorting: false,
        Cell: ({ row }) => (
          <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center', gap: 2 }}>
            <Link href={`/attentdance/${row.id}`}>
              <Button variant='outlined' color='info'>
                <Icon icon='ph:eye' fontSize={20} />
                <Translations text='View' />
              </Button>
            </Link>
          </Box>
        ),
        size: 150
      }
    ],
    [t]
  )

  const animOne = {
    x: -100,
    opacity: 0
  }

  const animTwo = {
    opacity: 1,
    x: 0,
    transition: 0.2,
    delay: 0.2,
    stagger: 2
  }

  return (
    <AnimationContainer animOne={animOne} animTwo={animTwo}>
      <Box sx={{ pt: '2rem' }}>
        <CustomHeader pageName='Attendance' hideAdd />
        <CustomTable
          disableRowSelection
          url='/user/get-attendances'
          columns={columns}
          rowIdKey='id'
          disableBodyClick={true}
        />
      </Box>
    </AnimationContainer>
  )
}

export default Atentdance
