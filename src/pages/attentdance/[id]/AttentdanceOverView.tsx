// ** MUI Components
import Box from '@mui/material/Box'
import Grid from '@mui/material/Grid'
import Card from '@mui/material/Card'
import Typography from '@mui/material/Typography'
import CardContent from '@mui/material/CardContent'

// ** Icon Imports
import Icon from 'src/@core/components/icon'
import moment from 'moment'
import Translations from 'src/components/translations'
import { Avatar, Badge } from '@mui/material'

// ** Types
interface user {
  id: number
  firstname: string
  lastname: string
  surname: string
}
interface Props {
  user: user
  checkIn: string
  checkOut: string
  created_at?: string
}

const renderUser = (user: user, checkIn:string, checkOut:string) => {
  if (user) {
    return (
      <Box
        sx={{
          display: 'flex',
          '&:not(:last-of-type)': { mb: 3 },
          '& svg': { color: 'text.secondary' }
        }}
      >
        <Box sx={{ display: 'flex', mr: 2 }}>
          
          <Badge  variant='dot' color={checkIn && !checkOut && 'success' || checkIn && checkOut && 'error' ||  'primary'}>
            <Avatar  sizes='xl' src='/images/avatars/11.png' alt='User Avatar' />
          </Badge>
        </Box>

        <Box sx={{ columnGap: 2, display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
          <Typography sx={{ fontWeight: 500, color: 'text.secondary' }}>
            <Translations text={user.firstname} />,{' '}
          </Typography>
          <Typography sx={{ fontWeight: 500, color: 'text.secondary' }}>
            <Translations text={user.lastname} />,{' '}
          </Typography>
          <Typography sx={{ fontWeight: 500, color: 'text.secondary' }}>
            <Translations text={user.surname} />,
          </Typography>
        </Box>
      </Box>
    )
  } else {
    return null
  }
}

const renderDate = (date: string) => {
  return (
    <Box sx={{ columnGap: 2, display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
      <Typography sx={{ fontWeight: 500, color: 'text.secondary' }}>
        {moment(date).format('MMMM Do YYYY, h:mm:ss a')}
      </Typography>
    </Box>
  )
}

const AttendanceOverView = (props: Props) => {
  const { checkIn, checkOut, user, created_at = '' } = props

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        <Card>
          <CardContent>
            {/* user info  */}
            <Box sx={{ mb: 6 }}>
              <Typography variant='body2' sx={{ mb: 4, color: 'text.disabled', textTransform: 'uppercase' }}>
                <Translations text='User'/>
              </Typography>
              {renderUser(user, checkIn, checkOut)}
            </Box>

            {/* check in info */}
            <Box sx={{ mb: 6 }}>
              <Typography variant='body2' sx={{ mb: 4, color: 'text.disabled', textTransform: 'uppercase' }}>
               <Translations text='Check in'/>
              </Typography>
              <Box
                sx={{
                  display: 'flex',
                  '&:not(:last-of-type)': { mb: 3 },
                  '& svg': { color: 'text.secondary' }
                }}
              >
                <Box sx={{ display: 'flex', mr: 2 }}>
                  <Icon fontSize='1.25rem' icon={'icomoon-free:enter'} />
                </Box>

                <Box sx={{ columnGap: 2, display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
                  <Typography  sx={{ fontWeight: 500, color: 'text.secondary' }}>
                    {!checkIn ? (
                      <Translations text='not yet working or unknown' />
                    ) : (
                      moment(checkIn, 'HH:mm:ss.SSSSSS').format('hh:mm:ss.SSS A')
                    )}
                  </Typography>
                </Box>
              </Box>
            </Box>

            {/* check out info */}
            <Box sx={{ mb: 6 }}>
              <Typography variant="body2"   sx={{ mb: 4, color: 'text.disabled', textTransform: 'uppercase' }}>
                <Translations text='Check out'/>
              </Typography>
              <Box
                sx={{
                  display: 'flex',
                  '&:not(:last-of-type)': { mb: 3 },
                  '& svg': { color: 'text.secondary' }
                }}
              >
                <Box sx={{ display: 'flex', mr: 2 }}>
                  <Icon fontSize='1.25rem' icon={'material-symbols:digital-out-of-home-outline'} />
                </Box>

                <Box sx={{ columnGap: 2, display: 'flex', flexWrap: 'wrap', alignItems: 'center' }}>
                  <Typography sx={{ fontWeight: 500, color: 'text.secondary' }}>
                    {!checkOut ? (
                      < >
                        <Translations text='not yet out of business or unknown' />
                      </>
                    ) : (
                      moment(checkOut, 'HH:mm:ss.SSSSSS').format('hh:mm:ss.SSS A')
                    )}
                  </Typography>
                </Box>
              </Box>
            </Box>

            {created_at && (
              <Box sx={{ mb: 6 }}>
                <Typography variant='body2' sx={{ mb: 4, color: 'text.disabled', textTransform: 'uppercase' }}>
                  <Translations text='Date'/>
                </Typography>
                {renderDate(created_at)}
              </Box>
            )}
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  )
}

export default AttendanceOverView
