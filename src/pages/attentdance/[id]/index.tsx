import { Box, Grid, Typography } from '@mui/material'
import React, { useContext } from 'react'
import GetContainer from 'src/components/get-container'
import { TableFilterContext } from 'src/context/TableFilterContext'
import useQueryParams from 'src/hooks/useQueryParams'
import Translations from 'src/components/translations'
import dynamic from 'next/dynamic'
import BackBtn from 'src/components/back-btn/BackBtn'

const Loader = dynamic(() => import('../../../components/loader'), {ssr:false})
const AttendanceOverView = dynamic(() => import('./AttentdanceOverView'), {ssr:false})

const AttendanceSingle = () => {
  const filter = useContext(TableFilterContext)
  const { globalFilter, pagination } = filter
  const query = useQueryParams()
  



  return (
    <>
    {
      query.get('id') ?  <GetContainer
      url={`/user/get-attendance/${query?.get('id')}`}
      hideLoading
      params={{
        search: globalFilter,
        page: pagination.pageIndex + 1,
        size: pagination.pageSize
      }}
    >
      {({ data, isError }) => (

        <>
        <BackBtn href='/attentdance' showIcon/>
          {isError ? (
            <Box
              sx={{ width: '100%', height: '100vh', display: 'flex', alignItems: 'center', justifyContent: 'center' }}
            >
              <Typography variant='h1'>
                <Translations text='Data Not Found!'/>
              </Typography>
            </Box>
          ) : null}
          {data ? (
            <Grid item lg={12} md={12} xs={12}>
              <AttendanceOverView
                user={data?.user}
                checkIn={data?.check_in}
                checkOut={data?.check_out}
                created_at={data?.created_at}
              />
            </Grid>
          ) : (
            <Box
              sx={{ width: '100%', height: '100vh', display: 'flex', alignItems: 'center', justifyContent: 'center' }}
            >
              <Typography variant='h1'>
              <Translations text='Data Not Found!'/>
              </Typography>
            </Box>
          )}
        </>
      )}
    </GetContainer> : <Loader/>
    }
    </>
   
  )
}

export default AttendanceSingle
