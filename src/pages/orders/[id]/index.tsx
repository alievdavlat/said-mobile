import React from 'react'
import GetContainer from 'src/components/get-container'
// ** MUI Imports
import Tab from '@mui/material/Tab'
import TabList from '@mui/lab/TabList'
import TabPanel from '@mui/lab/TabPanel'
import TabContext from '@mui/lab/TabContext'
import OrdeerItemList from './List'
import BackBtn from 'src/components/back-btn/BackBtn'
import { useTranslation } from 'react-i18next'
import useQueryParams from 'src/hooks/useQueryParams'
import Icon from 'src/@core/components/icon'
import OrderInfo from './Order-Previw'
import { CustomModal } from 'src/layouts/custom-table/custom-modal'
import dynamic from 'next/dynamic'

const OrderItemInfo  = dynamic(() => import('./Order-Item-Preview'), {ssr:false})

const OrdersSingle = () => {
  // ** state
  const [selectedRowData, setSelectedRowData] = React.useState<any>({})
  const [value, setValue] = React.useState<string>('1')

  
  //  ** hooks

  const { t } = useTranslation()
  const query = useQueryParams()

  //  ** handlers
  const handleChange = (event: React.SyntheticEvent, newValue: string) => {
    setValue(newValue)
  }

  const handleRowClick = (row: any) => {
    setSelectedRowData(row)

    query.set('order-item', 'true')

  }

  return (
    <GetContainer url={`/order/info/${query.get('id')}`}>
      {({ data, isSuccess }) => {
        
        return (
          <>
            {isSuccess && (
              <TabContext value={value}>
                <BackBtn href='/orders' showIcon />
                <br />
                <br />
                <TabList
                  scrollButtons
                  variant='fullWidth'
                  onChange={handleChange}
                  aria-label='forced scroll tabs example'
                >
                  <Tab value='1' label={t('Order')} icon={<Icon icon='lets-icons:order' />} />
                  <Tab value='2' label={t('List')} icon={<Icon icon='line-md:list' />} />
                </TabList>

                {/* Tab body */}
                <TabPanel value='1'>
                  {/* cargo info */}
                  {isSuccess && data && <OrderInfo data={data} />}
                </TabPanel>

                <TabPanel value='2'>
                  {/* list info */}
                  <br />
                  <br />

                  <OrdeerItemList
                    data={data?.product_item || []}
                    rowCount={data?.product_item?.length && data?.product_item?.length + 1}
                    onClick={handleRowClick}
                    rowIdKey='id'
                  />
                </TabPanel>
              </TabContext>
            )}

            <CustomModal
              open={query.has('order-item')}
              onClose={() => {
                if (query.has('order-item')) {
                  query.remove('order-item')
                }
              }}
            >
             <OrderItemInfo data={selectedRowData}/>
            </CustomModal>
          </>
        )
      }}
    </GetContainer>
  )
}

export default OrdersSingle
