import { Box, Button } from '@mui/material'
import { MRT_ColumnDef, MaterialReactTable, useMaterialReactTable } from 'material-react-table'
import React, { useContext, useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import AnimationContainer from 'src/components/animations/anim-container/AnimAtionContainer'
import { TableFilterContext } from 'src/context/TableFilterContext'
import CustomHeader from 'src/layouts/custom-table/custom-header'
import Icon from 'src/@core/components/icon'



type Props = {
  data: any
  rowIdKey?: string
  onClick: (row: any) => void
  rowCount: number
  disableRowSelection?: boolean
}

type dataProps =  {
    id: number,
    product: {
      id: number,
      name: string,
      brand: { id: number, name: string },
      category: { id: number, name: string },
      type: { id: number, name: string }
    },
    price: string,
    code: string

}

const Table = (props: {
  data: any
  columns: any
  rowIdKey?: string
  onClick: (row: any) => void
  rowCount: number
}) => {
  const filter = useContext(TableFilterContext)

  const { columnFilters, globalFilter, pagination, setPagination } = filter



  const table = useMaterialReactTable({
    columns: props.columns || [],
    data: props.data || [],
    defaultColumn: {
      maxSize: 400,
      minSize: 80,
      size: 150 // default size is usually 180
    },
    enableColumnResizing: true,
    columnResizeMode: 'onChange',
    manualFiltering: true,
    manualPagination: true,
    state: {
      columnFilters,
      globalFilter,
      pagination
    },
    enableFullScreenToggle: false,
    rowCount: props.rowCount,
    onPaginationChange: setPagination,
    getRowId: (row) => row[props.rowIdKey ? props?.rowIdKey : 'id'] || row.id,
    muiTableBodyRowProps: ({ row }) => ({
      onClick: () => props?.onClick(row.original),
      style: {
        cursor: 'pointer'
      }
    }),
  });

  return <MaterialReactTable table={table} />
}

const OrdeerItemList = (props: Props) => {
  // ** Hooks
  const { t } = useTranslation()

  // ** columns
  const columns = useMemo<MRT_ColumnDef<dataProps>[]>(
    () => [
      {
        accessorKey: 'product.name',
        header: t('Product Name'),
        size: 200,
        enableGlobalFilter: true
      },

      {
        accessorKey: 'product.brand.name',
        header: t('Brand Name'),
        size: 200,
        enableGlobalFilter: true
      },

      {
        accessorKey: 'product.category.name',
        header: t('Category'),
        size: 200,
        enableGlobalFilter: true
      },

      {
        accessorKey: 'product.type.name',
        header: t('Type Name'),
        size: 200,
        enableGlobalFilter: true
      },

      {
        accessorKey: 'price',
        header: t('Price'),
        enableGlobalFilter: true,

        size: 200
      },
      {
        accessorKey: 'code',
        header: t('Code'),
        enableGlobalFilter: true,
        size: 200,
        Cell: ({ renderedCellValue }) => (
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: '1rem'
            }}
          >
            <span>{renderedCellValue}</span>
          </Box>
        )
      },
      {
        accessorKey: 'id',
        header: t('Actions'),
        enableColumnFilter: false,
        enableGlobalFilter: false,
        enableSorting: false,
        Cell: () => (
          <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center', gap: 2 }}>
              <Button variant='outlined' color='info' >
                <Icon icon='ph:eye' fontSize={20} /> 
                {t("View")}
              </Button>
          </Box>
        ),
        size: 200
      },
    ],
    [t]
  )


  // ** animations
  const animOne = {
    x: -100,
    opacity: 0
  }

  const animTwo = {
    opacity: 1,
    x: 0,
    transition: 0.2,
    delay: 0.2,
    stagger: 2
  }

  return (
    <>
      <AnimationContainer animOne={animOne} animTwo={animTwo}>
        <CustomHeader pageName='Cargo Products'  hideAdd hideDelete />
        <Table
          data={props.data || []}
          columns={columns}
          onClick={props.onClick}
          rowCount={props?.rowCount}
          rowIdKey='id'

        />
      </AnimationContainer>
    </>
  )
}

export default OrdeerItemList
