// ** MUI Imports
import Grid from '@mui/material/Grid'
import Card from '@mui/material/Card'
import Table from '@mui/material/Table'
import Divider from '@mui/material/Divider'
import TableRow from '@mui/material/TableRow'
import TableBody from '@mui/material/TableBody'
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box'
import CardContent from '@mui/material/CardContent'
import { styled, useTheme } from '@mui/material/styles'
import TableCell, { TableCellBaseProps } from '@mui/material/TableCell'

// ** Configs
import themeConfig from 'src/configs/themeConfig'

import Translations from 'src/components/translations'

const MUITableCell = styled(TableCell)<TableCellBaseProps>(({ theme }) => ({
  borderBottom: 0,
  paddingLeft: '0 !important',
  paddingRight: '0 !important',
  '&:not(:last-child)': {
    paddingRight: `${theme.spacing(2)} !important`
  }
}))

type dataProps =  {
  id: number,
  product: {
    id: number,
    name: string,
    brand: { id: number, name: string },
    category: { id: number, name: string },
    type: { id: number, name: string }
  },
  price: string,
  code: string

}


interface Props {
  data:dataProps
}

const OrderItemInfo = (props:Props) => {
  const {data} = props
  // ** Hook
  const theme = useTheme()

  return (
            <Card>
              {/* Client */}
              <CardContent sx={{ p: [`${theme.spacing(6)} !important`, `${theme.spacing(10)} !important`] }}>
                <Grid container>
                  <Grid item sm={6} xs={12} sx={{ mb: { md: 8, xs: 4 } }}>
                    <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                      <Typography variant='h4' sx={{ fontWeight: 700, lineHeight: '24px' }}>
                        {themeConfig.templateName}
                      </Typography>
                    </Box>
                  </Grid>

                  <Divider />

                  <Grid item xs={12}>
                    <Table>
                      <TableBody sx={{ '& .MuiTableCell-root': { py: `${theme.spacing(1.5)} !important` } }}>
                        <TableRow>
                          <MUITableCell>
                            <Typography variant='h4'>
                              <Translations text='Info' />
                            </Typography>
                          </MUITableCell>
                        </TableRow>
                        <br />
                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Product Name' /> : {`${data.product.name}`}
                          </Typography>
                        </TableRow>
                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Brand Name' /> : {`${data.product.brand.name}`}
                          </Typography>
                        </TableRow>
                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Type Name' /> : {`${data.product.type.name}`}
                          </Typography>
                        </TableRow>
                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Category' /> : {`${data.product.category.name}`}
                          </Typography>
                        </TableRow>

                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Price' /> : {data.price}
                          </Typography>
                        </TableRow>
                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Code' /> : {data.code}
                          </Typography>
                        </TableRow>
                      </TableBody>
                    </Table>
                  </Grid>
                </Grid>
              </CardContent>

            </Card>
  )
}

export default OrderItemInfo
