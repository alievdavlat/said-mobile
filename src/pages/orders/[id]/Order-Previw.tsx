// ** MUI Imports
import Grid from '@mui/material/Grid'
import Card from '@mui/material/Card'
import Table from '@mui/material/Table'
import Divider from '@mui/material/Divider'
import TableRow from '@mui/material/TableRow'
import TableBody from '@mui/material/TableBody'
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box'
import CardContent from '@mui/material/CardContent'
import { styled, useTheme } from '@mui/material/styles'
import TableCell, { TableCellBaseProps } from '@mui/material/TableCell'

// ** Configs
import themeConfig from 'src/configs/themeConfig'

import Translations from 'src/components/translations'

const MUITableCell = styled(TableCell)<TableCellBaseProps>(({ theme }) => ({
  borderBottom: 0,
  paddingLeft: '0 !important',
  paddingRight: '0 !important',
  '&:not(:last-child)': {
    paddingRight: `${theme.spacing(2)} !important`
  }
}))

type dataProps = {
  id: number
  client: {
    id: number
    first_name: string
    last_name: string
    phone_number: string
    note: string
    address: string
  }
  currency: {
    id: number
    name: string
    symbol: string
    exchange_rate: number
    created_at: string
  }
  user: {
    id: number
    firstname: string
    lastname: string
    surname: string
  }
  seller: {
    id: number
    firstname: string
    lastname: string
    surname: string
  }
  store: {
    id: number
    name: string
  }
  product_item: [
    {
      id: number
      product: number
      price: string
      code: string
    }
  ]
  created_at: string
  updated_at: string
  total_price: string
  discount_amount: string
  price_type: number
  status: number
  comment: string
}


interface Props {
  data:dataProps
}
const OrderInfo = (props:Props) => {
  const {data} = props
  
  // ** Hook
  const theme = useTheme()

  return (
            <Card>
              {/* Client */}
              <CardContent sx={{ p: [`${theme.spacing(6)} !important`, `${theme.spacing(10)} !important`] }}>
                <Grid container>
                  <Grid item sm={6} xs={12} sx={{ mb: { md: 8, xs: 4 } }}>
                    <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                      <Typography variant='h4' sx={{ fontWeight: 700, lineHeight: '24px' }}>
                        {themeConfig.templateName}
                      </Typography>
                    </Box>
                  </Grid>

                  <Divider />

                  <Grid item xs={12}>
                    <Table>
                      <TableBody sx={{ '& .MuiTableCell-root': { py: `${theme.spacing(1.5)} !important` } }}>
                        <TableRow>
                          <MUITableCell>
                            <Typography variant='h4'>
                              <Translations text='Clients' />
                            </Typography>
                          </MUITableCell>
                        </TableRow>
                        <br />
                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Client Name' /> : {`${data.client.first_name}`}
                          </Typography>
                        </TableRow>
                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Client LastName' /> : {data.client.last_name}
                          </Typography>
                        </TableRow>
                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Client PhoneNumber' /> : {data.client.phone_number}
                          </Typography>
                        </TableRow>

                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Note' /> : {data.client.note}
                          </Typography>
                        </TableRow>
                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Client Address' /> : {data.client.address}
                          </Typography>
                        </TableRow>
                      </TableBody>
                    </Table>
                  </Grid>
                </Grid>
              </CardContent>

              <Divider />

              {/* user */}
              <CardContent sx={{ p: [`${theme.spacing(6)} !important`, `${theme.spacing(10)} !important`] }}>
                <Grid container>
                  <Grid item xs={12} md={12} sx={{ mb: { lg: 0, xs: 4 } }}>
                    <Typography variant='h4' sx={{ mb: 6 }}>
                      <Translations text='User' />
                    </Typography>
                    <Typography sx={{ mb: 1.5, color: 'text.secondary' }}>
                      <Translations text='User Name' /> : {data.user.firstname}
                    </Typography>
                    <Typography sx={{ mb: 1.5, color: 'text.secondary' }}>
                      {' '}
                      <Translations text='User LastName' /> : {data.user.lastname}
                    </Typography>
                    <Typography sx={{ mb: 1.5, color: 'text.secondary' }}>
                      <Translations text='User Surname' /> : {data.user.surname}
                    </Typography>
                  </Grid>
                </Grid>
              </CardContent>

              <Divider />

                {/* seller */}
              <CardContent sx={{ p: [`${theme.spacing(6)} !important`, `${theme.spacing(10)} !important`] }}>
                <Grid container>
                  <Grid item xs={12} md={12} sx={{ mb: { lg: 0, xs: 4 } }}>
                    <Typography variant='h4' sx={{ mb: 6 }}>
                      <Translations text='Seller' />
                    </Typography>
                    <Typography sx={{ mb: 1.5, color: 'text.secondary' }}>
                      <Translations text='Seller Name' /> : {data.seller.firstname}
                    </Typography>
                    <Typography sx={{ mb: 1.5, color: 'text.secondary' }}>
                      {' '}
                      <Translations text='Seller LastName' /> : {data.seller.lastname}
                    </Typography>
                    <Typography sx={{ mb: 1.5, color: 'text.secondary' }}>
                      <Translations text='Seller Surname' /> : {data.seller.surname}
                    </Typography>
                  </Grid>
                </Grid>
              </CardContent>

              <Divider />

              {/* Currensy */}
              <CardContent sx={{ p: [`${theme.spacing(6)} !important`, `${theme.spacing(10)} !important`] }}>
                <Grid container>
                  <Grid item xs={12} md={12} sx={{ mb: { lg: 0, xs: 4 } }}>
                    <Typography variant='h4' sx={{ mb: 6 }}>
                      <Translations text='Currency' />
                    </Typography>
                    <Typography sx={{ mb: 1.5, color: 'text.secondary' }}>
                      <Translations text='Currency Name' /> : {data.currency.name}
                    </Typography>
                    <Typography sx={{ mb: 1.5, color: 'text.secondary' }}>
                      {' '}
                      <Translations text='Symbol' /> : {data.currency.symbol}
                    </Typography>
                    <Typography sx={{ mb: 1.5, color: 'text.secondary' }}>
                      <Translations text='Exchange Rate' /> : {data.currency.exchange_rate}
                    </Typography>
                  </Grid>
                </Grid>
              </CardContent>

              <Divider />

              {/* Store */}
              <CardContent sx={{ p: [`${theme.spacing(6)} !important`, `${theme.spacing(10)} !important`] }}>
                <Grid container>
                  <Grid item xs={12} md={12} sx={{ mb: { lg: 0, xs: 4 } }}>
                    <Typography variant='h4' sx={{ mb: 6 }}>
                      <Translations text='Store' />
                    </Typography>
                    <Typography sx={{ mb: 1.5, color: 'text.secondary' }}>
                      <Translations text='Store Name' /> : {data.store.name}
                    </Typography>
                  </Grid>
                </Grid>
              </CardContent>

              <Divider />

               {/* Info */}
               <CardContent sx={{ p: [`${theme.spacing(6)} !important`, `${theme.spacing(10)} !important`] }}>
                <Grid container>
                  <Grid item xs={12} md={12} sx={{ mb: { lg: 0, xs: 4 } }}>
                    <Typography variant='h4' sx={{ mb: 6 }}>
                      <Translations text='Info' />
                    </Typography>
                    <Typography sx={{ mb: 1.5, color: 'text.secondary' }}>
                      <Translations text='Total Price' /> : {data.total_price}
                    </Typography>
                    <Typography sx={{ mb: 1.5, color: 'text.secondary' }}>
                      <Translations text='Discount Amoun' /> : {data.discount_amount}
                    </Typography>

                    <Typography sx={{ mb: 1.5, color: 'text.secondary' }}>
                      <Translations text='Price Type' /> : {data.price_type}
                    </Typography>

                    
                    <Typography sx={{ mb: 1.5, color: 'text.secondary' }}>
                      <Translations text='Status' /> : {data.status}
                    </Typography>

                    <Typography sx={{ mb: 1.5, color: 'text.secondary' }}>
                      <Translations text='Comment' /> : {data.comment}
                    </Typography>
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
  )
}

export default OrderInfo
