import React from 'react'
import AnimationContainer from 'src/components/animations/anim-container/AnimAtionContainer'
import { MRT_ColumnDef } from 'material-react-table'
import { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { Box, Button, Typography } from '@mui/material'
import Translations from 'src/layouts/components/Translations'
import CustomHeader from '../../layouts/custom-table/custom-header'
import CustomTable from '../../layouts/custom-table'
import { CustomDrawer } from '../../layouts/custom-table/custom-drawer'
import useQueryParams from 'src/hooks/useQueryParams'
import Icon from 'src/@core/components/icon'
import dynamic from 'next/dynamic'

import moment from 'moment'
import Link from 'next/link'

const OrderForm = dynamic(() => import('src/components/forms/order/OrderForm'), { ssr: false })

type dataProps = {
  id: number
  client: {
    id: number
    first_name: string
    last_name: string
    phone_number: string
    note: string
    address: string
  }
  currency: {
    id: number
    name: string
    symbol: string
    exchange_rate: number
    created_at: string
  }
  user: {
    id: number
    firstname: string
    lastname: string
    surname: string
  }
  seller: {
    id: number
    firstname: string
    lastname: string
    surname: string
  }
  store: {
    id: number
    name: string
  }
  product_item: [
    {
      id: number
      product: number
      price: string
      code: string
    }
  ]
  created_at: string
  updated_at: string
  total_price: string
  discount_amount: string
  price_type: number
  status: number
  comment: string
}

const Orders = () => {
  const { t } = useTranslation()
  const query = useQueryParams()

  const animOne = {
    x: -100,
    opacity: 0
  }

  const animTwo = {
    opacity: 1,
    x: 0,
    transition: 0.2,
    delay: 0.2,
    stagger: 2
  }

  const columns = useMemo<MRT_ColumnDef<dataProps>[]>(
    () => [
      {
        accessorKey: 'id',
        header: t('Actions'),
        enableColumnFilter: false,
        enableGlobalFilter: false,
        enableSorting: false,
        Cell: ({ row }) => (
          <Link
            href={`/orders/${row.id}`}
            style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: 2, textDecoration: 'none' }}
          >
            <Button variant='text' color='primary'>
              <Icon icon='mdi:eye' fontSize={20} />
              <Translations text='View' />
            </Button>
          </Link>
        ),
        size: 200
      },
      {
        accessorKey: 'client.first_name',
        header: t('Client Name'),
        size: 200,
        enableGlobalFilter: true
      },
      {
        accessorKey: 'client.last_name',
        header: t('Client LastName'),
        size: 200,
        enableGlobalFilter: true
      },
      {
        accessorKey: 'client.phone_number',
        header: t('Client PhoneNumber'),
        enableGlobalFilter: true,
        size: 200
      },
      {
        accessorKey: 'client.address',
        header: t('Client Address'),
        enableGlobalFilter: true,
        size: 200
      },
      {
        accessorKey: 'comment',
        header: t('Comment'),
        enableGlobalFilter: true,
        size: 200
      },
      {
        accessorKey: 'user.firstname',
        header: t('User Name'),
        size: 200
      },
      {
        accessorKey: 'user.lastname',
        header: t('User LastName'),
        enableGlobalFilter: true,
        size: 200
      },
      {
        accessorKey: 'user.surname',
        header: t('User Surname'),
        size: 200
      },
      {
        accessorKey: 'seller.firstname',
        header: t('Seller Name'),
        size: 200
      },
      {
        accessorKey: 'seller.lastname',
        header: t('Seller LastName'),
        enableGlobalFilter: true,
        size: 200
      },
      {
        accessorKey: 'seller.surname',
        header: t('Seller Surname'),
        size: 200
      },
      {
        accessorKey: 'currency.name',
        header: t('Currency Name'),
        size: 200
      },
      {
        accessorKey: 'currency.symbol',
        header: t('Symbol'),
        enableGlobalFilter: true,
        size: 200
      },
      {
        accessorKey: 'currency.exchange_rate',
        header: t('Exchange Rate'),
        size: 200
      },
      {
        accessorKey: 'store.name',
        header: t('Store Name'),
        size: 200
      },
      {
        accessorKey: 'total_price',
        header: t('Total Price'),
        enableGlobalFilter: true,
        size: 200
      },
      {
        accessorKey: 'discount_amount',
        header: t('Discount Amount'),
        size: 200
      },
      {
        accessorKey: 'price_type',
        header: t('Price Type'),
        size: 200
      },
      {
        accessorKey: 'status',
        header: t('Status'),
        size: 200
      },
      {
        accessorKey: 'updated_at',
        header: t('Updated At'),
        size: 200,
        Cell: ({ cell }) => <Typography>{moment(String(cell.getValue())).format('ll')}</Typography>
      },
      {
        accessorKey: 'created_at',
        header: t('Created at'),
        size: 200,
        Cell: ({ cell }) => <Typography>{moment(String(cell.getValue())).format('ll')}</Typography>
      }
    ],
    [t]
  )

  return (
    <Box>
      <AnimationContainer animOne={animOne} animTwo={animTwo}>
        <CustomHeader pageName='Orders' hideDelete />
        <CustomTable url='/order/list' columns={columns} rowIdKey='id' showExport={true} disableBodyClick={true} disableRowSelection={true} />
      </AnimationContainer>

      <CustomDrawer
        open={query.has('onAdd')}
        onClose={() => {
          if (query.has('onAdd')) {
            query.remove('onAdd')
          }
        }}
      >
        <OrderForm />
      </CustomDrawer>
    </Box>
  )
}

export default Orders
