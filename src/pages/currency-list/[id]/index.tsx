import { Grid } from '@mui/material'
import { MRT_ColumnDef } from 'material-react-table'
import React, { useContext, useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import AnimationContainer from 'src/components/animations/anim-container/AnimAtionContainer'
import BackBtn from 'src/components/back-btn/BackBtn'
import { TableFilterContext } from 'src/context/TableFilterContext'
import useQueryParams from 'src/hooks/useQueryParams'
import CustomTable from 'src/layouts/custom-table'
import CustomHeader from 'src/layouts/custom-table/custom-header'

interface currencyHistoryData {
  id: number
  currency: {
    id: number
    name: string
    symbol: string
  }
  exchange_rate: number
  timestamp: string
}

const CurrencyHistory = () => {
  const filter = useContext(TableFilterContext)
  const { globalFilter, pagination } = filter
  const { t } = useTranslation()
  const query = useQueryParams()
  const columns = useMemo<MRT_ColumnDef<currencyHistoryData>[]>(
    () => [
      {
        accessorKey: 'currency.name',
        header: t('Name'),
        size: 400,
        enableGlobalFilter: true
      },
      {
        accessorKey: 'currency.symbol',
        header: t('Symbol'),
        size: 400,
        enableGlobalFilter: true
      },

      {
        accessorKey: 'timestamp',
        header: t('Created at'),
        size: 400
      }
    ],
    [t]
  )

  const animOne = {
    x: -100,
    opacity: 0
  }

  const animTwo = {
    opacity: 1,
    x: 0,
    transition: 0.2,
    delay: 0.2,
    stagger: 2
  }

  return (
    <AnimationContainer animOne={animOne} animTwo={animTwo}>
      <Grid item xs={12} marginBottom={10} >
      <BackBtn href='/currency-list' showIcon/>
      </Grid>
      <Grid item xs={12}>
        <CustomHeader pageName='Currency History' hideAdd hideDelete />
        <CustomTable
          url={'/currency/history-list'}
          params={{
            search: globalFilter,
            page: pagination.pageIndex + 1,
            size: pagination.pageSize,
            currency: query.get('id')
          }}
          disableBodyClick={true}
          columns={columns}
          rowIdKey='id'
        />
      </Grid>
    </AnimationContainer>
  )
}

export default CurrencyHistory
