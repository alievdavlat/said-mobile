import { MRT_ColumnDef } from 'material-react-table'
import React, { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { Box, Button, Typography, Grid } from '@mui/material'
import Translations from 'src/layouts/components/Translations'
import CustomHeader from '../../layouts/custom-table/custom-header'
import CustomTable from '../../layouts/custom-table'
import { CustomDrawer } from '../../layouts/custom-table/custom-drawer'
import useQueryParams from 'src/hooks/useQueryParams'
import moment from 'moment'
import Icon from 'src/@core/components/icon'
import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import AnimationContainer from 'src/components/animations/anim-container/AnimAtionContainer'

const CurrencyForm = dynamic(() => import('src/components/forms/CurrencyForm'), { ssr: false })

type dataProps = {
  name: string
  address: string
  part: boolean
  sell: boolean
  sklad: boolean
}

function Currency() {
  const { t } = useTranslation()
  const query = useQueryParams()
  const router = useRouter()

  const columns = useMemo<MRT_ColumnDef<dataProps>[]>(
    () => [
      {
        accessorKey: 'name',
        header: t('Name'),
        size: 200,
        enableGlobalFilter: true
      },
      {
        accessorKey: 'symbol',
        header: t('Symbol'),
        size: 200,
        enableGlobalFilter: true
      },
      {
        accessorKey: 'exchange_rate',
        header: t('Exchange Rate'),
        enableGlobalFilter: true,
        size: 250
      },

      {
        accessorKey: 'created_at',
        header: t('Created at'),
        size: 250,
        Cell: ({ cell }) => <Typography>{moment(String(cell.getValue())).format('ll')}</Typography>
      },
      {
        accessorKey: 'id',
        header: t('Actions'),
        enableColumnFilter: false,
        enableGlobalFilter: false,
        enableSorting: false,

        Cell: ({ row }) => (
          <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: 2 }}>
            <Button variant='outlined' color='success' onClick={() => query.set('id', row.id)}>
              <Icon icon='tabler:edit' fontSize={20} />
              <Translations text='Edit' />
            </Button>

            <Button
              variant='outlined'
              color='info'
              onClick={e => {
                e.stopPropagation()
                router.push({
                  pathname: '/currency-list/[id]',
                  query: {
                    id: row.id
                  }
                })
              }}
            >
              <Icon icon='ph:eye' fontSize={20} />
              <Translations text='History' />
            </Button>
          </Box>
        ),
        size: 300
      }
    ],
    [t, router, query]
  )

  const animOne = {
    x: -100,
    opacity: 0
  }

  const animTwo = {
    opacity: 1,
    x: 0,
    transition: 0.2,
    delay: 0.2,
    stagger: 2
  }

  return (
    <AnimationContainer animOne={animOne} animTwo={animTwo}>
      <Grid container spacing={8}>
        <Grid item xs={12}>
          <CustomHeader pageName='Currency' deleteUrl='/currency/delete' />
          <CustomTable url='/currency/list' columns={columns} rowIdKey='id' disableBodyClick={true} />
          <CustomDrawer
            open={query.has('id') || query.has('onAdd')}
            onClose={() => {
              if (query.has('id')) {
                query.remove('id')
              } else if (query.has('onAdd')) {
                query.remove('onAdd')
              }
            }}
          >
            <CurrencyForm />
          </CustomDrawer>
        </Grid>
      </Grid>
    </AnimationContainer>
  )
}

export default Currency
