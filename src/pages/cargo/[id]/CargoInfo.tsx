import React from 'react'

// ** React Imports
import { Fragment } from 'react'

// ** MUI Imports
import List from '@mui/material/List'
import Divider from '@mui/material/Divider'
import ListItem from '@mui/material/ListItem'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import ListItemButton from '@mui/material/ListItemButton'

// ** Icon Imports
import Icon from 'src/@core/components/icon'
import moment from 'moment'
import { useTranslation } from 'react-i18next'
import { Grid } from '@mui/material'
import Image from 'next/image'
import {  MEDIA_URL } from 'src/configs/req'

type Props = {
  invoice: string
  totalPrice: string
  createdAt: string
  additionalOutput: string
  file: string
}

const CargoInfo = (props: Props) => {
  const { additionalOutput, createdAt, file, invoice, totalPrice } = props
  const { t } = useTranslation()

  const url = MEDIA_URL + file

  

  return (
    <Grid container spacing={6} sx={{alignItems:"center"}}>
      {file && (
        <Grid item md={6} xs={12} sx={{alignItems:"center", justifyContent:'center'}}>
          <Image src={file && url ?  url : '/images/products/apple-11.png'} alt='Product' width={300} height={300}   style={{ objectFit:'contain', borderRadius:"10px"}} />
        </Grid>
      )}
      <Grid item md={6} xs={12}>
        <Fragment>
          <List component='nav' aria-label='main mailbox'>
            <ListItem disablePadding>
              <ListItemButton>
                <ListItemIcon>
                  <Icon icon='ph:invoice' fontSize={20} />
                </ListItemIcon>
                <ListItemText primary={`${t('Invoice')}: ${invoice}`} />
              </ListItemButton>
            </ListItem>


          
          </List>

          <Divider sx={{ m: '0 !important' }} />
          <List component='nav' aria-label='secondary mailbox'>
            <ListItem disablePadding>
              <ListItemButton>
                <ListItemIcon>
                  <Icon icon='material-symbols-light:price-check-rounded' fontSize={20} />
                </ListItemIcon>
                <ListItemText primary={`${t('Total Price')}: ${totalPrice}`} />
              </ListItemButton>
            </ListItem>

            <ListItem disablePadding>
              <ListItemButton component='a' href='#simple-list'>
                <ListItemIcon>
                  <Icon icon='uiw:date' fontSize={20} />
                </ListItemIcon>
                <ListItemText primary={`${t('Created at')}: ${moment(createdAt).format('LLL')}`} />
              </ListItemButton>
            </ListItem>

           
          </List>
        </Fragment>
      </Grid>
    </Grid>
  )
}

export default CargoInfo
