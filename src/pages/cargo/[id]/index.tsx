import React, { SyntheticEvent, useState } from 'react'
import { useTranslation } from 'react-i18next'
import useQueryParams from 'src/hooks/useQueryParams'
import Icon from 'src/@core/components/icon'
import CargoInfo from './CargoInfo'

// ** MUI Imports
import Tab from '@mui/material/Tab'
import TabList from '@mui/lab/TabList'
import TabPanel from '@mui/lab/TabPanel'
import TabContext from '@mui/lab/TabContext'
import GetContainer from 'src/components/get-container'
import CargoProductList from './List'
import BackBtn from 'src/components/back-btn/BackBtn'

function CargoSingle() {
  const { t } = useTranslation()
  const query = useQueryParams()

  // ** state
  const [value, setValue] = useState<string>('1')

  const handleChange = (event: SyntheticEvent, newValue: string) => {
    setValue(newValue)
  }

  return (
    <>
      <GetContainer url={`/cargo/info/${query.get('id')}`}>
        {({ data, isSuccess}) => {


          return (
            <>
              {isSuccess && (
                <TabContext value={value}>
                  <BackBtn href='/cargo' showIcon/>
                  <br />
                  <br />
                  <TabList
                    scrollButtons
                    variant='fullWidth'
                    onChange={handleChange}
                    aria-label='forced scroll tabs example'
                  >
                    <Tab value='1' label={t('Cargo')} icon={<Icon icon='mdi:truck-cargo-container' />} />
                    <Tab value='2' label={t('List')} icon={<Icon icon='line-md:list' />} />
                  </TabList>

                  {/* Tab body */}
                  <TabPanel value='1'>
                    {/* cargo info */}
                    <CargoInfo
                      invoice={data?.invoice}
                      totalPrice={data?.total_price}
                      additionalOutput={data?.additional_output}
                      createdAt={data?.created_at}
                      file={data?.file}
                    />
                  </TabPanel>

                  <TabPanel value='2'>
                    {/* list info */}
                    <br />
                    <br />

                    <CargoProductList
                      data={data?.cargo_products || []}
                      rowCount={data?.cargo_products?.length && data?.cargo_products?.length}
                    />
                  </TabPanel>
                </TabContext>
              )}
            </>
          )
        }}
      </GetContainer>
    </>
  )
}

export default CargoSingle
