import { Box, Button } from '@mui/material'
import { MRT_ColumnDef, MaterialReactTable, useMaterialReactTable } from 'material-react-table'
import React, { useContext, useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import AnimationContainer from 'src/components/animations/anim-container/AnimAtionContainer'
import Translations from 'src/components/translations'
import { TableFilterContext } from 'src/context/TableFilterContext'
import useQueryParams from 'src/hooks/useQueryParams'
import CustomHeader from 'src/layouts/custom-table/custom-header'


type Props = {
  data: any
  rowIdKey?: string
  onClick?: (row: any) => void
  rowCount: number
  disableRowSelection?: boolean
}

type dataProps = {
  id: number
  product: {
    id: number
    name: string
    brand: {
      id: number
      name: string
    }
    category: {
      id: number
      name: string
    }
    type: {
      id: number
      name: string
    }
  }
  quantity: number
  price: string
  total_price: string
  cost_price: string
  remain_price: string
  remain_count: number
}

const Table = (props: {
  data: any
  columns: any
  rowIdKey?: string
  onClick?: (row: any) => void
  rowCount: number
  disableRowSelection?: boolean
}) => {
  const filter = useContext(TableFilterContext)

  const { columnFilters, globalFilter, pagination, setPagination } = filter

  const table = useMaterialReactTable({
    columns: props.columns || [],
    data: props?.data,
    defaultColumn: {
      maxSize: 400,
      minSize: 80,
      size: 150 //default size is usually 180
    },
    enableColumnResizing: true,
    columnResizeMode: 'onChange',
    manualFiltering: true,
    manualPagination: true,
    state: {
      columnFilters,
      globalFilter,
      pagination
    },
    enableFullScreenToggle: false,
    rowCount: props.rowCount,
    onPaginationChange: setPagination
  })

  return <MaterialReactTable table={table} />
}

const CargoProductList = (props: Props) => {
  // ** Hooks
  const query = useQueryParams()
  const { t } = useTranslation()

  // ** handler
  const handleOpenEditCode = (storeId: any, productId: any) => {
    query.setObj({ updateCode: 'true', cargoId: query.get('id'), storeId, productId })
  }

  // ** columns
  const columns = useMemo<MRT_ColumnDef<dataProps>[]>(
    () => [
      {
        accessorKey: 'product.name',
        header: t('Product Name'),
        size: 300,
        enableGlobalFilter: true
      },
      {
        accessorKey: 'quantity',
        header: t('Quantity'),
        enableGlobalFilter: true,
        size: 200,
        Cell: ({ renderedCellValue }) => (
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: '1rem'
            }}
          >
            <span>{renderedCellValue}</span>
          </Box>
        )
      },
      {
        accessorKey: 'total_price',
        header: t('Total Price'),
        size: 200,
        enableGlobalFilter: true
      },
     

      {
        accessorKey: 'remain_price',
        header: t('Remain Price'),
        enableGlobalFilter: true,
        size: 200,
        Cell: ({ renderedCellValue }) => (
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: '1rem'
            }}
          >
            <span>{renderedCellValue}</span>
          </Box>
        )
      },
      {
        accessorKey: 'remain_count',
        header: t('Remain Count'),
        enableGlobalFilter: true,
        size: 200,
        Cell: ({ renderedCellValue }) => (
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: '1rem'
            }}
          >
            <span>{renderedCellValue}</span>
          </Box>
        )
      },
    ],
    [t, handleOpenEditCode]
  )



  // ** animations
  const animOne = {
    x: -100,
    opacity: 0
  }

  const animTwo = {
    opacity: 1,
    x: 0,
    transition: 0.2,
    delay: 0.2,
    stagger: 2
  }

  return (
    <>
      <AnimationContainer animOne={animOne} animTwo={animTwo}>
        <CustomHeader pageName='Cargo Products'  hideAdd hideDelete />
        <Table
          data={props.data || []}
          columns={columns}
          onClick={props.onClick}
          rowCount={props?.rowCount}
          rowIdKey='id'
          disableRowSelection
        />
      </AnimationContainer>

    </>
  )
}

export default CargoProductList
