// ** MUI Imports
import Grid from '@mui/material/Grid'
import Card from '@mui/material/Card'
import Table from '@mui/material/Table'
import Divider from '@mui/material/Divider'
import TableRow from '@mui/material/TableRow'
import TableBody from '@mui/material/TableBody'
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box'
import CardContent from '@mui/material/CardContent'
import { styled, useTheme } from '@mui/material/styles'
import TableCell, { TableCellBaseProps } from '@mui/material/TableCell'


// ** Types
import GetContainer from 'src/components/get-container'
import useQueryParams from 'src/hooks/useQueryParams'
import Translations from 'src/components/translations'
import Image from 'next/image'
import { MEDIA_URL } from 'src/configs/req'

const MUITableCell = styled(TableCell)<TableCellBaseProps>(({ theme }) => ({
  borderBottom: 0,
  paddingLeft: '0 !important',
  paddingRight: '0 !important',
  '&:not(:last-child)': {
    paddingRight: `${theme.spacing(2)} !important`
  }
}))


type dataProps = {
  id: number
  invoice: string
  total_price: string
  file: string
  cargo_products: [
    {
      id: number
      product: {
        id: number
        name: string
        brand: {
          id: number
          name: string
        }
        category: {
          id: number
          name: string
        }
        type: {
          id: number
          name: string
        }
      }
      quantity: number
      price: string
      total_price: string
      remain_price: string
      remain_count: number
      cost_price:number
    }
  ]
}

const CargoPreview = () => {
  // ** Hook
  const theme = useTheme()
  const query = useQueryParams()

  return (
    <GetContainer url={`/cargo/info-id/${query.get('preview')}`}>
      {({ data, isError, isLoading }: { data: dataProps; isError: any; isLoading: any }) => {
        if (data && !isLoading) {
          return (
            <Card sx={{overflowY:"scroll"}}>
              <CardContent sx={{ p: [`${theme.spacing(6)} !important`, `${theme.spacing(10)} !important`] }}>
                <Grid container spacing={6}>
                  <Grid item md={12} xs={12}>
                    <Image
                      src={data && data.file ? MEDIA_URL + data.file : ''}
                      width={350}
                      height={350}
                      alt='user image'
                      style={{ objectFit: 'cover', borderRadius: '2.5rem', border: '2px solid white' }}
                    />
                  </Grid>
                  <Divider />

                  <Grid item xs={12}>
                    <Table>
                      <TableBody sx={{ '& .MuiTableCell-root': { py: `${theme.spacing(1.5)} !important` } }}>
                        <TableRow>
                          <MUITableCell>
                            <Typography variant='h4'>
                              <Translations text='Info' />
                            </Typography>
                          </MUITableCell>
                        </TableRow>
                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Invoice' /> : {`${data.invoice}`}
                          </Typography>
                        </TableRow>
                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Total Price' /> : {data.total_price}
                          </Typography>
                        </TableRow>
                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            {/* <Translations text='Surname' /> : {data.cargo_products} */}
                          </Typography>
                        </TableRow>

                      </TableBody>
                    </Table>
                  </Grid>


                  <Grid item xs={12}>
                    <Table>
                      <TableBody sx={{ '& .MuiTableCell-root': { py: `${theme.spacing(1.5)} !important` } }}>
                        <TableRow>
                          <MUITableCell>
                            <Typography variant='h4'>
                              <Translations text='Product' />
                            </Typography>
                          </MUITableCell>
                        </TableRow>
                          {
                            data.cargo_products.map(i => (
                              <Box key={i.id}>
                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text={'Cost'} /> : {i.cost_price}
                          </Typography>
                        </TableRow>
                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text={'Price'} /> : {i.price}
                          </Typography>
                        </TableRow>

                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text={'Product Name'} /> : {i.product.name}
                          </Typography>
                        </TableRow>

                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text={'Quantity'} /> : {i.quantity}
                          </Typography>
                        </TableRow>

                              </Box>
                            ))
                          }

                      </TableBody>
                    </Table>
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
          )
        } else if (isError) {
          return (
            <Typography>
              <Translations text='Something Went Wrong' />
            </Typography>
          )
        } else {
          return (
            <Typography>
              <Translations text='Data Not Found!' />
            </Typography>
          )
        }
      }}
    </GetContainer>
  )
}

export default CargoPreview
