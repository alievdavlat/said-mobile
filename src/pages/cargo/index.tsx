import { MRT_ColumnDef } from 'material-react-table'
import React, { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { Box, Button } from '@mui/material'
import Translations from 'src/layouts/components/Translations'
import CustomHeader from '../../layouts/custom-table/custom-header'
import CustomTable from '../../layouts/custom-table'
import { CustomDrawer } from '../../layouts/custom-table/custom-drawer'
import useQueryParams from 'src/hooks/useQueryParams'
import Icon from 'src/@core/components/icon'
import Link from 'next/link'
import dynamic from 'next/dynamic'
import AnimationContainer from 'src/components/animations/anim-container/AnimAtionContainer'
import { CustomModal } from 'src/layouts/custom-table/custom-modal'
import CargoPreview from './CargoPrieview'

const CargoForm = dynamic(() => import('src/components/forms/cargo/CargoForm'), { ssr: false })

type dataProps = {
  id: string
  invoice: string
  total_price: string
  file: string
  cargo_products: [
    {
      id: number
      product: {
        id: number
        name: string
        brand: {
          id: number
          name: string
        }
        category: {
          id: number
          name: string
        }
        type: {
          id: number
          name: string
        }
      }
      quantity: number
      price: string
      total_price: string
      remain_price: string
      remain_count: number
      cost_price:number
    }
  ]
}

function Cargo() {
  const { t } = useTranslation()
  const query = useQueryParams()

  const columns = useMemo<MRT_ColumnDef<dataProps>[]>(
    () => [
      {
        accessorKey: 'invoice',
        header: t('Invoice'),
        size: 200,
        enableGlobalFilter: true
      },
      {
        accessorKey: 'total_price',
        header: t('Total Price'),
        size: 200,
        enableGlobalFilter: true
      },
 
      {
        accessorFn: row => `${row.cargo_products[0]?.product.name}`,
        accessorKey: 'name',
        header: t('Product'),
        enableGlobalFilter: true,
        size: 150,
        Cell: ({ renderedCellValue }) => (
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: '1rem'
            }}
          >
            <span>{renderedCellValue}</span>
          </Box>
        )
      },

      {
        accessorFn: row => `${row.cargo_products[0].price}`,
        accessorKey: 'price',
        header: t('Price'),
        enableGlobalFilter: true,
        size: 150,
        Cell: ({ renderedCellValue }) => (
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: '1rem'
            }}
          >
            <span>{renderedCellValue}</span>
          </Box>
        )
      },
      {
        accessorFn: row => `${row.cargo_products[0].remain_price}`,

        accessorKey: 'remain_price',
        header: t('Remain price'),
        enableGlobalFilter: true,
        size: 150,
        Cell: ({ renderedCellValue }) => (
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: '1rem'
            }}
          >
            <span>{renderedCellValue}</span>
          </Box>
        )
      },

     
      {
        accessorKey: 'id',
        header: t('Actions'),
        enableColumnFilter: false,
        enableGlobalFilter: false,
        enableSorting: false,
        Cell: ({ row }) => (
          <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: 2 }}>
            <Button variant='outlined' color='primary' onClick={() => query.set('id', row?.original?.id)}>
              <Icon icon='tabler:edit' fontSize={20} />
              <Translations text='Edit' />
            </Button>
        
            <Link href={`/cargo/${row.original.id}`}>
            <Button variant='outlined' color='secondary'>
              <Icon icon='ph:eye' fontSize={20} />
              <Translations text='View' />
            </Button>
            </Link>
          </Box>
        ),
        size: 300 //decrease the width of this column
      }
    ],
    [t]
  )

  const animOne = {
    x: -100,
    opacity: 0
  }

  const animTwo = {
    opacity: 1,
    x: 0,
    transition: 0.2,
    delay: 0.2,
    stagger: 2
  }

  return (
    <AnimationContainer animOne={animOne} animTwo={animTwo}>
      <Box>
        <CustomHeader pageName='Cargo' hideDelete />
        <CustomTable url='/cargo/list' columns={columns} rowIdKey='id' disableBodyClick={true}  />
        <CustomDrawer
          open={query.has('id') || query.has('onAdd')}
          onClose={() => {
            if (query.has('id')) {
              query.remove('id')
            } else if (query.has('onAdd')) {
              query.remove('onAdd')
            }
          }}
        >
          <CargoForm />
        </CustomDrawer>

        <CustomModal open={query.has('preview')} onClose={() => {
            if (query.has('preview')) {
                query.remove('preview')
            }
          }}>
            <CargoPreview/>
          </CustomModal>
      </Box>
    </AnimationContainer>
  )
}

export default Cargo
