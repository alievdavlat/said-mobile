import { MRT_ColumnDef } from 'material-react-table'
import React, { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { Box } from '@mui/material'
import CustomHeader from '../../layouts/custom-table/custom-header'
import CustomTable from '../../layouts/custom-table'
import AnimationContainer from 'src/components/animations/anim-container/AnimAtionContainer'

type dataProps = {
  id: number
  product: {
    id: number
    name: string
    brand: {
      id: number
      name: string
    }
    category: {
      id: number
      name: string
    }
    type: {
      id: number
      name: string
    }
  }
  status: boolean
  count: number
  from_store: {
    id: number
    name: string
  }
  to_store: {
    id: number
    name: string
  }
}

function Trasfers() {
  const { t } = useTranslation()

  const columns = useMemo<MRT_ColumnDef<dataProps>[]>(
    () => [
      {
        accessorKey: 'firstname.name',
        header: t('Name'),
        size: 200,
        enableGlobalFilter: true
      },
      {
        accessorKey: 'product.brand.name',
        header: t('Brand Name'),
        size: 200,
        enableGlobalFilter: true
      },
      {
        accessorKey: 'product.category.name',
        header: t('Category Name'),
        enableGlobalFilter: true,

        size: 200
      },

      {
        accessorKey: 'product.type.name',
        header: t('Type Name'),
        enableGlobalFilter: true,
        size: 200
      },

      {
        accessorKey: 'count',
        header: t('Count'),
        enableGlobalFilter: true,
        size: 200
      },
      {
        accessorKey: 'from_store.name',
        header: t('From Store'),
        enableGlobalFilter: true,
        size: 200
      },
      {
        accessorKey: 'to_store.name',
        header: t('To Store'),
        enableGlobalFilter: true,
        size: 200
      }
    ],
    [t]
  )

  const animOne = {
    x: -100,
    opacity: 0
  }

  const animTwo = {
    opacity: 1,
    x: 0,
    transition: 0.2,
    delay: 0.2,
    stagger: 2
  }

  return (
    <AnimationContainer animOne={animOne} animTwo={animTwo}>
      <Box sx={{ pt: '2rem' }}>
        <CustomHeader pageName='Transfer' hideAdd hideDelete />
        <CustomTable
          disableRowSelection
          url='/stock/transfer-list'
          columns={columns}
          rowIdKey='id'
          disableBodyClick={true}
        />
      </Box>
    </AnimationContainer>
  )
}

export default Trasfers
