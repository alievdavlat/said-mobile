import React from 'react'
import AnimationContainer from 'src/components/animations/anim-container/AnimAtionContainer'
import { MRT_ColumnDef } from 'material-react-table'
import { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { Box, Button, Typography } from '@mui/material'
import Translations from 'src/layouts/components/Translations'
import CustomHeader from '../../layouts/custom-table/custom-header'
import CustomTable from '../../layouts/custom-table'
import { CustomDrawer } from '../../layouts/custom-table/custom-drawer'
import useQueryParams from 'src/hooks/useQueryParams'
import Icon from 'src/@core/components/icon'
import dynamic from 'next/dynamic'
import moment from 'moment'

const PriceListForm = dynamic(() => import('src/components/forms/PriceListForm'), { ssr: false })

type dataProps = {
  id: number
  created_at: string
  updated_at: string
  perpercentage: number
  product: number
  store:number
}

const PriceList = () => {
  const { t } = useTranslation()
  const query = useQueryParams()

  const animOne = {
    x: -100,
    opacity: 0
  }

  const animTwo = {
    opacity: 1,
    x: 0,
    transition: 0.2,
    delay: 0.2,
    stagger: 2
  }

  const columns = useMemo<MRT_ColumnDef<dataProps>[]>(
    () => [
      {
        accessorKey: 'perpercentage',
        header: t('Price'),
        size: 150,
        enableGlobalFilter: true
      },
      {
        accessorKey: 'created_at',
        header: t('Created at'),
        size: 200,
        Cell: ({ cell }) => <Typography>{moment(String(cell.getValue())).format('LLL')}</Typography>
      },
      {
        accessorKey: 'updated_at',
        header: t('Updated At'),
        size: 200,
        Cell: ({ cell }) => <Typography>{moment(String(cell.getValue())).format('LLL')}</Typography>
      },
     
      {
        accessorKey: 'id',
        header: t('Actions'),
        enableColumnFilter: false,
        enableGlobalFilter: false,
        enableSorting: false,
        Cell: () => (
          <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: 2 }}>
            <Button variant='text' color='primary'>
              <Icon icon='tabler:edit' fontSize={20} />
              <Translations text='Edit' />
            </Button>
          </Box>
        ),
        size: 200
      }
    ],
    [t]
  )

  return (
    <div>
      <AnimationContainer animOne={animOne} animTwo={animTwo}>
        <Box>
          <CustomHeader pageName='Prices' deleteUrl='/stock/pricelist-delete' />
          <CustomTable url='/stock/pricelist-list' columns={columns} rowIdKey='id' />
          <CustomDrawer
            open={query.has('id') || query.has('onAdd')}
            onClose={() => {
              if (query.has('id')) {
                query.remove('id')
              } else if (query.has('onAdd')) {
                query.remove('onAdd')
              }
            }}
          >
            <PriceListForm />
          </CustomDrawer>
        </Box>
      </AnimationContainer>
    </div>
  )
}

export default PriceList
