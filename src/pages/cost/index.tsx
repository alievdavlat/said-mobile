import { Box, Button, Typography } from '@mui/material'
import { MRT_ColumnDef } from 'material-react-table'
import React, { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import AnimationContainer from 'src/components/animations/anim-container/AnimAtionContainer'
import useQueryParams from 'src/hooks/useQueryParams'
import CustomTable from 'src/layouts/custom-table'
import { CustomDrawer } from 'src/layouts/custom-table/custom-drawer'
import CustomHeader from 'src/layouts/custom-table/custom-header'
import Icon from 'src/@core/components/icon'
import Link from 'next/link'
import Translations from 'src/components/translations'
import dynamic from 'next/dynamic'
import moment from 'moment'

const CostForm = dynamic(() => import('src/components/forms/CostForm'), { ssr: false })

interface CargoProductProps {
  id: number,
  client: {
    id: number,
    first_name: string,
    last_name: string,
    phone_number: string,
    note: string,
    address: string,
    type: number
  }
  cargo: {
    id: number,
    invoice: string
    file: string
    supplier: number
  }
  products: [
    {
      id:number
      product: {
        id: number
        name: string
        brand: {
          id:number
          name: string
        }
        category: {
          id: number
          name: string
        }
        type: {
          id: number
          name: string
        }
      }
      quantity: number
      price: string
      total_price: string
      remain_price: string
      remain_count: string
      codes: []
    }
  ]
  created_at: string
  updated_at: string
  name: string
  price: string
  type: string
  description: string
}

const CargoProduct = () => {

  const query = useQueryParams()
  const { t } = useTranslation()

  const columns = useMemo<MRT_ColumnDef<CargoProductProps>[]>(
    () => [
      {
        accessorFn: row => `${row.client.first_name + ' ' + row.client.last_name}`,
        header: t('Client'),
        size: 200,
        enableGlobalFilter: true,
        Cell: ({ renderedCellValue }) => (
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              gap: '1rem'
            }}
          >
            <span>{renderedCellValue}</span>
          </Box>
        )
      },

      {
        accessorKey: 'cargo.invoice',
        header: t('Invoice'),
        enableGlobalFilter: true,
        size: 150
      },

      {
        accessorKey: 'name',
        header: t('Name'),
        size: 150,
        enableGlobalFilter: true
      },


      {
        accessorKey: 'price',
        header: t('Price'),
        enableGlobalFilter: true,
        size: 150
      },
      {
        accessorKey: 'type',
        header: t('Type'),
        enableGlobalFilter: true,
        size: 150
      },

      {
        accessorKey: 'description',
        header: t('Description'),
        enableGlobalFilter: true,
        size: 150
      },
      {
        accessorKey: 'created_at',
        header: t('Created at'),
        size: 250,
        Cell: ({ cell }) => <Typography>{moment(String(cell.getValue())).format('ll')}</Typography>
      },

    ],
    [t]
  )

  const animOne = {
    x: -100,
    opacity: 0
  }

  const animTwo = {
    opacity: 1,
    x: 0,
    transition: 0.2,
    delay: 0.2,
    stagger: 2
  }



  return (
    <AnimationContainer animOne={animOne} animTwo={animTwo}>
      <Box>
        <CustomHeader pageName='Costs' hideDelete />
        <CustomTable
          url='/cargo/cost-list'
          columns={columns}
          rowIdKey='id'
        />
        <CustomDrawer
           open={query.has('id') || query.has('onAdd')}
           onClose={() => {
             if (query.has('id')) {
               query.remove('id')
             } else if (query.has('onAdd')) {
               query.remove('onAdd')
             }
           }}
        >
          <CostForm />
        </CustomDrawer>


      </Box>
    </AnimationContainer>
  )
}

export default CargoProduct
