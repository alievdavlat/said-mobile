// ** MUI Imports
import Grid from '@mui/material/Grid'
import Card from '@mui/material/Card'
import Table from '@mui/material/Table'
import Divider from '@mui/material/Divider'
import TableRow from '@mui/material/TableRow'
import TableBody from '@mui/material/TableBody'
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box'
import CardContent from '@mui/material/CardContent'
import { styled, useTheme } from '@mui/material/styles'
import TableCell, { TableCellBaseProps } from '@mui/material/TableCell'


// ** Types
import GetContainer from 'src/components/get-container'
import useQueryParams from 'src/hooks/useQueryParams'
import Translations from 'src/components/translations'
import Image from 'next/image'
import { MEDIA_URL } from 'src/configs/req'

const MUITableCell = styled(TableCell)<TableCellBaseProps>(({ theme }) => ({
  borderBottom: 0,
  paddingLeft: '0 !important',
  paddingRight: '0 !important',
  '&:not(:last-child)': {
    paddingRight: `${theme.spacing(2)} !important`
  }
}))

interface perrmissonProps {
  id: string
  name: string
  slug: string
  created_at: string
}

type dataProps = {
  id: string
  firstname: string
  lastname: string
  surname: string
  access_urls: string[]
  login: string
  role: {
    id: string
    name: string
    created_at: string
  }
  permission: perrmissonProps[]
  rent: boolean
  rent_percentage: number
  bonus: boolean
  currency: number
  photo: string
  store: number
  joined_date: string
  is_superuser: boolean
}

const UserPreview = () => {
  // ** Hook
  const theme = useTheme()
  const query = useQueryParams()

  return (
    <GetContainer url={`/user/info/${query.get('preview')}`}>
      {({ data, isError, isLoading }: { data: dataProps; isError: any; isLoading: any }) => {
        if (data && !isLoading) {
          return (
            <Card sx={{overflowY:"scroll"}}>
              <CardContent sx={{ p: [`${theme.spacing(6)} !important`, `${theme.spacing(10)} !important`] }}>
                <Grid container spacing={6}>
                  <Grid item xs={12}>
                    <Image
                      src={data && data.photo ? MEDIA_URL + data.photo.slice(33) : ''}
                      width={350}
                      height={350}
                      alt='user image'
                      style={{ objectFit: 'cover', borderRadius: '2.5rem', border: '2px solid white' }}
                    />
                  </Grid>
                  <Divider />

                  <Grid item xs={12}>
                    <Table>
                      <TableBody sx={{ '& .MuiTableCell-root': { py: `${theme.spacing(1.5)} !important` } }}>
                        <TableRow>
                          <MUITableCell>
                            <Typography variant='h4'>
                              <Translations text='Info' />
                            </Typography>
                          </MUITableCell>
                        </TableRow>
                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='First Name' /> : {`${data.firstname}`}
                          </Typography>
                        </TableRow>
                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Last Name' /> : {data.lastname}
                          </Typography>
                        </TableRow>
                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Surname' /> : {data.surname}
                          </Typography>
                        </TableRow>

                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Login' /> : {data.login}
                          </Typography>
                        </TableRow>

                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Role' /> : {data.role.name}
                          </Typography>
                        </TableRow>

                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Rent Percentage' /> : {data.rent_percentage}
                          </Typography>
                        </TableRow>

                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Rent' /> : <Translations text={data.rent ? 'Yes' : 'No'} />
                          </Typography>
                        </TableRow>
                      </TableBody>
                    </Table>
                  </Grid>
                  <Grid item xs={12}>
                            <Typography variant='h4'  sx={{ mb: 4 }}>
                              <Translations text='Permissions' />
                            </Typography>
                       <Box sx={{display:'flex', alignItems:"center", gap:2, flexWrap:'wrap' }}>
                       {data.permission.length &&
                          data.permission.map(permission => (
                              <Typography  sx={{ mb: 1.5, color: 'text.secondary' }} key={permission.id}>{`${permission.name}`}, </Typography>
                          ))}
                       </Box>
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
          )
        } else if (isError) {
          return (
            <Typography>
              <Translations text='Something Went Wrong' />
            </Typography>
          )
        } else {
          return (
            <Typography>
              <Translations text='Data Not Found!' />
            </Typography>
          )
        }
      }}
    </GetContainer>
  )
}

export default UserPreview
