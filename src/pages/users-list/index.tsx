"use client"

import { MRT_ColumnDef } from 'material-react-table'
import React, { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { Box, Button, Typography } from '@mui/material'
import Translations from 'src/layouts/components/Translations'
import CustomHeader from '../../layouts/custom-table/custom-header'
import CustomTable from '../../layouts/custom-table'
import { CustomDrawer } from '../../layouts/custom-table/custom-drawer'
import useQueryParams from 'src/hooks/useQueryParams'
import moment from 'moment'
import Icon from 'src/@core/components/icon'
import dynamic from 'next/dynamic'
import AnimationContainer from 'src/components/animations/anim-container/AnimAtionContainer'
import { CustomModal } from 'src/layouts/custom-table/custom-modal'

const UserForm = dynamic(() => import('src/components/forms/UserForm'), { ssr: false })
const UserPreview = dynamic(() => import('./ClientPreview') ,{ssr:false})
interface perrmissonProps {
  id: string
  name: string
  slug: string
  created_at: string
}

type dataProps = {
  id: string
  firstname: string
  lastname: string
  surname: string
  access_urls: string[]
  login: string
  role: {
    id: string
    name: string
    created_at: string
  }
  permission: perrmissonProps[]
  rent: boolean
  rent_percentage: number
  bonus: boolean
  currency: number
  photo: string
  store: number
  joined_date: string
  is_superuser: boolean
}

function Users() {
  const { t } = useTranslation()
  const query = useQueryParams()

  const columns = useMemo<MRT_ColumnDef<dataProps>[]>(
    () => [
      {
        accessorKey: 'firstname',
        header: t('First Name'),
        size: 150,
        enableGlobalFilter: true
      },
      {
        accessorKey: 'lastname',
        header: t('Last Name'),
        size: 150,
        enableGlobalFilter: true
      },
      {
        accessorKey: 'surname',
        header: t('Surname'),
        enableGlobalFilter: true,
        size: 200
      },
      {
        accessorKey: 'login',
        header: t('Login'),
        enableGlobalFilter: true,
        size: 150
      },
      {
        accessorKey: 'role.name',
        header: t('Role'),
        enableGlobalFilter: true,
        size: 150
      },
      {
        accessorKey: 'rent',
        header: t('Rent'),
        size: 150,
        Cell: ({ cell }) => <Typography>{cell?.getValue() === 'false' ? 'No' : 'Yes'}</Typography>
      },
      {
        accessorKey: 'joined_date',
        header: t('Joined Date'),
        size: 200,
        Cell: ({ cell }) => <Typography>{moment(String(cell.getValue())).format('ll')}</Typography>
      },
      {
        accessorKey: 'id',
        header: t('Actions'),
        enableColumnFilter: false,
        enableGlobalFilter: false,
        enableSorting: false,
        Cell: ({ row }) => (
          <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: 2 }}>
            <Button variant='outlined' color='primary' onClick={() => query.set('id', row.original.id)}>
              <Icon icon='tabler:edit' fontSize={20} />
              <Translations text='Edit' />
            </Button>
            <Button variant='outlined' color='success' onClick={() => query.set('preview', row.original.id)}>
              <Icon icon='ph:eye' fontSize={20} />
              <Translations text='View' />
            </Button>
          </Box>
        ),
        size: 300 //decrease the width of this column
      }
    ],
    [t]
  )

  const animOne = {
    x: -100,
    opacity: 0
  }

  const animTwo = {
    opacity: 1,
    x: 0,
    transition: 0.2,
    delay: 0.2,
    stagger: 2
  }

  return (
    <AnimationContainer animOne={animOne} animTwo={animTwo}>
      <Box>
        <CustomHeader pageName='Users' deleteUrl='/user/delete' />
        <CustomTable url='/user/list' columns={columns} rowIdKey='id' disableBodyClick={true} />
        <CustomDrawer
          open={query.has('id') || query.has('onAdd')}
          onClose={() => {
            if (query.has('id')) {
              query.remove('id')
            } else if (query.has('onAdd')) {
              query.remove('onAdd')
            }
          }}
        >
          <UserForm />
        </CustomDrawer>

        
        <CustomModal open={query.has('preview')} onClose={() => {
            if (query.has('preview')) {
                query.remove('preview')
            }
          }}>
            <UserPreview/>
          </CustomModal>
      </Box>
    </AnimationContainer>
  )
}

export default Users
