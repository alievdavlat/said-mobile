// ** MUI Imports
import Grid from '@mui/material/Grid'
import Card from '@mui/material/Card'
import Table from '@mui/material/Table'
import Divider from '@mui/material/Divider'
import TableRow from '@mui/material/TableRow'
import TableBody from '@mui/material/TableBody'
import Typography from '@mui/material/Typography'
import CardContent from '@mui/material/CardContent'
import { styled, useTheme } from '@mui/material/styles'
import TableCell, { TableCellBaseProps } from '@mui/material/TableCell'


// ** Types
import GetContainer from 'src/components/get-container'
import useQueryParams from 'src/hooks/useQueryParams'
import Translations from 'src/components/translations'

const MUITableCell = styled(TableCell)<TableCellBaseProps>(({ theme }) => ({
  borderBottom: 0,
  paddingLeft: '0 !important',
  paddingRight: '0 !important',
  '&:not(:last-child)': {
    paddingRight: `${theme.spacing(2)} !important`
  }
}))

type dataProps = {
  id:string
  first_name: string
  last_name: string
  phone_number: boolean
  note: boolean
  address: boolean
  type: string
}
const supplierData = [
  {
    id:1,
    name:'Client',
    value:1
  },
  {
    id:2,
    name:'Supplier',
    value:2
  },
  {
    id:3,
    name:'Investor',
    value:3
  },
]

const ClientPreview = () => {
  // ** Hook
  const theme = useTheme()
  const query = useQueryParams()

  return (
    <GetContainer url={`/client/info/${query.get('preview')}`}>
      {({ data, isError, isLoading }: { data: dataProps; isError: any; isLoading: any }) => {
        if (data && !isLoading) {
          
          return (
            <Card>
              <CardContent sx={{ p: [`${theme.spacing(6)} !important`, `${theme.spacing(10)} !important`] }}>
                <Grid container>
                 
              
                  <Divider />

                  <Grid item xs={12}>
                    <Table>
                      <TableBody sx={{ '& .MuiTableCell-root': { py: `${theme.spacing(1.5)} !important` } }}>
                        <TableRow>
                          <MUITableCell>
                            <Typography variant='h4'>
                              <Translations text='Info' />
                            </Typography>
                          </MUITableCell>
                        </TableRow>
                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Client Name' /> :{' '}{`${data.first_name}`}
                          </Typography>
                        </TableRow>
                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Client LastName' /> : {' '}{data.last_name} 
                          </Typography>
                        </TableRow>
                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Client PhoneNumber' /> : {data.phone_number}
                          </Typography>
                        </TableRow>

                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Note' /> : {data.note}
                          </Typography>
                        </TableRow>

                        
                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                            <Translations text='Client Address' /> : {data.address}
                          </Typography>
                        </TableRow>

                        <TableRow>
                          <Typography sx={{ color: 'text.secondary' }}>
                              <Translations text='Type' /> : <Translations text={supplierData.find((i:any) => i.id === data?.type)?.name  || ''}/>
                          </Typography>
                        </TableRow>
                      </TableBody>
                    </Table>
                  </Grid>
                </Grid>
              </CardContent>
            </Card>
          )
        } else if (isError) {
          return (
            <Typography>
              <Translations text='Something Went Wrong' />
            </Typography>
          )
        } else {
          return (
            <Typography>
              <Translations text='Data Not Found!' />
            </Typography>
          )
        }
      }}
    </GetContainer>
  )
}

export default ClientPreview
