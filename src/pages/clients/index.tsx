import React from 'react'
import AnimationContainer from 'src/components/animations/anim-container/AnimAtionContainer'
import { MRT_ColumnDef } from 'material-react-table'
import { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { Box, Button } from '@mui/material'
import Translations from 'src/layouts/components/Translations'
import CustomHeader from '../../layouts/custom-table/custom-header'
import CustomTable from '../../layouts/custom-table'
import { CustomDrawer } from '../../layouts/custom-table/custom-drawer'
import useQueryParams from 'src/hooks/useQueryParams'
import Icon from 'src/@core/components/icon'
import dynamic from 'next/dynamic'
import { CustomModal } from 'src/layouts/custom-table/custom-modal'

const ClientForm = dynamic(() => import('src/components/forms/ClientForm'), { ssr: false })
const ClientPreview = dynamic(() => import('./ClientPreview'), { ssr: false })

type dataProps = {
  id:string
  first_name: string
  last_name: string
  phone_number: boolean
  note: boolean
  address: boolean
}

const Clients = () => {
  const { t } = useTranslation()
  const query = useQueryParams()

  const animOne = {
    x: -100,
    opacity: 0
  }

  const animTwo = {
    opacity: 1,
    x: 0,
    transition: 0.2,
    delay: 0.2,
    stagger: 2
  }

  const columns = useMemo<MRT_ColumnDef<dataProps>[]>(
    () => [
      {
        accessorKey: 'first_name',
        header: t('First Name'),
        size: 150,
        enableGlobalFilter: true
      },
      {
        accessorKey: 'last_name',
        header: t('Last Name'),
        size: 150,
        enableGlobalFilter: true
      },
      {
        accessorKey: 'phone_number',
        header: t('Phone'),
        enableGlobalFilter: true,
        size: 200
      },
      {
        accessorKey: 'note',
        header: t('Note'),
        enableGlobalFilter: true,
        size: 150
      },
      {
        accessorKey: 'address',
        header: t('Address'),
        size: 150,
      },
      {
        accessorKey: 'id',
        header: t('Actions'),
        enableColumnFilter: false,
        enableGlobalFilter: false,
        enableSorting: false,
        Cell: ({row}) => (
          <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: 2 }}>
            <Button variant='outlined' color='primary' onClick={() => query.set('id', row.original.id)}>
              <Icon icon='tabler:edit' fontSize={20} />
              <Translations text='Edit' />
            </Button>
            <Button variant='outlined' color='success' onClick={() => query.set('preview', row.original.id)}>
              <Icon icon='ph:eye' fontSize={20} />
              <Translations text='View' />
            </Button>
          </Box>
        ),
        size: 350
      }
    ],
    [t]
  )

  return (
    <div>
      <AnimationContainer animOne={animOne} animTwo={animTwo}>
        <Box>
          <CustomHeader pageName='Clients' deleteUrl='/client/delete' />
          <CustomTable url='/client/list' columns={columns} rowIdKey='id' disableBodyClick={true} />
          <CustomDrawer
            open={query.has('id') || query.has('onAdd')}
            onClose={() => {
              if (query.has('id')) {
                query.remove('id')
              } else if (query.has('onAdd')) {
                query.remove('onAdd')
              }
            }}
          >
            <ClientForm />
          </CustomDrawer>

          <CustomModal open={query.has('preview')} onClose={() => {
            if (query.has('preview')) {
                query.remove('preview')
            }
          }}>
            <ClientPreview/>
          </CustomModal>
        </Box>
      </AnimationContainer>
    </div>
  )
}

export default Clients
