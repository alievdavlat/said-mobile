import { MRT_ColumnDef } from 'material-react-table'
import React, { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { Box, Button } from '@mui/material'
import CustomHeader from '../../layouts/custom-table/custom-header'
import CustomTable from '../../layouts/custom-table'
import moment from 'moment'
import { useAuth } from 'src/hooks/useAuth'
import AnimationContainer from 'src/components/animations/anim-container/AnimAtionContainer'

type dataProps = {
  id: number
  created_at: string
  updated_at: string
  debit: number
  credit: number
  description: string
  store: number
}

function StoreBalance() {
  const { t } = useTranslation()
  const auth = useAuth()

  const [columnVisibility, setColumnVisibility] = React.useState<any>({})

  const columns = useMemo<MRT_ColumnDef<dataProps>[]>(
    () => [
      {
        accessorKey: 'debit',
        header: t('Coming'),
        size: 250,
        enableGlobalFilter: true,
        enableColumnActions: false,

        Cell: ({renderedCellValue}:{renderedCellValue:any}) => (
          <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: 2, color: columnVisibility?.credit === false ? 'green' :'' }}>
            {renderedCellValue}
          </Box>
        )

      },
      {
        accessorKey: 'description',
        header: t('Description'),
        size: 250,
        enableGlobalFilter: true,
        enableColumnActions: false
      },
      {
        accessorKey: 'credit',
        header: t('Expense'),
        enableGlobalFilter: true,
        enableColumnActions: false,
        size: 250,

        Cell: ({ renderedCellValue }: { renderedCellValue: any }) => (
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'row',
              alignItems: 'center',
              gap: 2,
              color: columnVisibility?.debit === false ? 'red' : ''
            }}
          >
            {renderedCellValue}
          </Box>
        )
      },
      {
        accessorKey: 'created_at',
        header: t('Created at'),
        enableGlobalFilter: true,
        enableColumnActions: false,
        size: 250,
        Cell: ({ row }: { row: any }) => (
          <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: 2 }}>
            {moment(row.getValue()).format('ll')}
          </Box>
        )
      },
      {
        accessorKey: 'updated_at',
        header: t('Updated At'),
        enableGlobalFilter: true,
        enableColumnActions: false,
        size: 250,
        Cell: ({ row }: { row: any }) => (
          <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: 2 }}>
            {moment(row.getValue()).format('ll')}
          </Box>
        )
      }
    ],
    [t, columnVisibility]
  )

  const handleSHowHide = (key?: string) => {
    if (key === 'Coming') {
      setColumnVisibility({ credit: false })
    } else if (key === 'Expense') {
      setColumnVisibility({ debit: false })
    } else if (key === 'All') {
      setColumnVisibility({})
    }
  }

  const animOne = {
    x: -100,
    opacity: 0
  }

  const animTwo = {
    opacity: 1,
    x: 0,
    transition: 0.2,
    delay: 0.2,
    stagger: 2
  }

  const RightButtons = () => {
    return (
      <Box display={'flex'} alignItems={'center'} justifyContent={'center'} gap={4}>
        <Button color='success' variant='outlined' onClick={() => handleSHowHide('Coming')}>
          {t('Coming')}
        </Button>

        <Button color='error' variant='outlined' onClick={() => handleSHowHide('Expense')}>
          {t('Expense')}
        </Button>

        <Button variant='outlined' onClick={() => handleSHowHide('All')}>
          {t('All')}
        </Button>
      </Box>
    )
  }

  return (
    <AnimationContainer animOne={animOne} animTwo={animTwo}>
      {
        <Box>
          <CustomHeader pageName='Store Balance History' rightButtons={<RightButtons />} hideAdd hideDelete />
          <CustomTable
            url={`stock/balance-history/${auth?.user?.store ? auth?.user?.store : 0}`}
            columns={columns}
            rowIdKey='id'
            columnVisibilityData={columnVisibility}
            disableBodyClick={true}
          />
        </Box>
      }
    </AnimationContainer>
  )
}

export default StoreBalance
