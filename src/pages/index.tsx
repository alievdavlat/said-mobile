import { useRouter } from "next/router"
import { useEffect } from "react"
import { useAuth } from "src/hooks/useAuth"
import getHomeRoute from "src/layouts/components/acl/getHomeRoute"

const Home = () => {
  const router = useRouter()
  const auth  = useAuth()
  useEffect(() => {
    if (router.asPath === '/' && localStorage.getItem('userData')) {
      if (auth?.user?.role?.name) {
        
        const homeRoute = getHomeRoute(auth?.user?.role?.name)
        router.replace(homeRoute)
      } else {
        router.replace('/home')
      }
    } else if (!localStorage.getItem('userData') && router.asPath === '/') {
      router.replace('/login')
    }
  }, [router])
  
  return <>Do Not access this page</>
}

export default Home
