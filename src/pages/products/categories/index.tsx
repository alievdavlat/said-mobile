import { Box, Button } from '@mui/material'
import React, { useMemo } from 'react'
import Icon from 'src/@core/components/icon'
import Translations from 'src/components/translations'
import { CustomDrawer } from 'src/layouts/custom-table/custom-drawer'
import useQueryParams from 'src/hooks/useQueryParams'
import { useTranslation } from 'react-i18next'
import dynamic from 'next/dynamic'
import AnimationContainer from 'src/components/animations/anim-container/AnimAtionContainer'
import CustomHeader from 'src/layouts/custom-table/custom-header'
import CustomTable from 'src/layouts/custom-table'
import { MRT_ColumnDef } from 'material-react-table'

const CategoryForm = dynamic(() => import('src/components/forms/CategoryForm'), { ssr: false })

interface Data {
  id: string
  name: string
  children: Data[]
}

const Categories = () => {
  const { t } = useTranslation()
  const query = useQueryParams()


  const columns = useMemo<MRT_ColumnDef<Data>[]>(
    () => [
      {
        accessorKey: 'name',
        header: t('Name'),
        size: 300,
        enableGlobalFilter: true
      },
      {
        accessorKey: 'id',
        header: t('Actions'),
        enableColumnFilter: false,
        enableGlobalFilter: false,
        enableSorting: false,
        Cell: () => (
          <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: 2 }}>
            <Button variant='text' color='primary'>
              <Icon icon='tabler:edit' fontSize={20} />
              <Translations text='Edit' />
            </Button>
          </Box>
        ),
        size: 200
      }
    ],
    [t]
  )

  const animOne = {
    x: -100,
    opacity: 0
  }

  const animTwo = {
    opacity: 1,
    x: 0,
    transition: 0.2,
    delay: 0.2,
    stagger: 2
  }

  return (
    <AnimationContainer animOne={animOne} animTwo={animTwo}>
      <Box>
        <CustomHeader pageName='Categories' deleteUrl='/product/category/delete' />
        <CustomTable url={'/product/category/list'} columns={columns} rowIdKey='id'  enableExpanding/>
        <CustomDrawer
          open={query.has('id') || query.has('onAdd')}
          onClose={() => {
            if (query.has('id')) {
              query.remove('id')
            } else if (query.has('onAdd')) {
              query.remove('onAdd')
            }
          }}
        >
          <CategoryForm />
        </CustomDrawer>
      </Box>
    </AnimationContainer>
  )
}

export default Categories
