import { MRT_ColumnDef } from 'material-react-table'
import React, { useMemo, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Box, Button } from '@mui/material'
import Translations from 'src/layouts/components/Translations'
import CustomHeader from '../../../layouts/custom-table/custom-header'
import CustomTable from '../../../layouts/custom-table'
import { CustomDrawer } from '../../../layouts/custom-table/custom-drawer'
import useQueryParams from 'src/hooks/useQueryParams'
import Icon from 'src/@core/components/icon'
import dynamic from 'next/dynamic'
import ProductFilter from 'src/components/forms/filters/ProductFilter'
import { CustomModal } from 'src/layouts/custom-table/custom-modal'
import AnimationContainer from 'src/components/animations/anim-container/AnimAtionContainer'

const ProductForm = dynamic(() => import('src/components/forms/ProductForm'), { ssr: false })

type dataProps = {
  name: string
  brand: {
    id: number
    name: string
  }
  category: {
    id: number
    name: string
  }
  type: {
    id: number
    name: string
  }
}

function ProductsList() {
  const { t } = useTranslation()
  const query = useQueryParams()
  const [category, setCategory] = useState<string>('')
  const [brand, setBrand] = useState<string>('')
  const [type, setType] = useState<string>('')

  

  const columns = useMemo<MRT_ColumnDef<dataProps>[]>(
    () => [
      {
        accessorKey: 'name',
        header: t('Name'),
        size: 250,
        enableGlobalFilter: true
      },
      {
        accessorKey: 'brand.name',
        header: t('Brand'),
        size: 250,
        enableGlobalFilter: true
      },
      {
        accessorKey: 'category.name',
        header: t('Category'),
        enableGlobalFilter: true,
        size: 250
      },
      {
        accessorKey: 'type.name',
        header: t('Type'),
        enableGlobalFilter: true,
        size: 250
      },
      {
        accessorKey: 'id',
        header: t('Actions'),
        enableColumnFilter: false,
        enableGlobalFilter: false,
        enableSorting: false,
        Cell: () => (
          <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: 2 }}>
            <Button variant='text' color='primary'>
              <Icon icon='tabler:edit' fontSize={20} />
              <Translations text='Edit' />
            </Button>
          </Box>
        ),
        size: 200 //decrease the width of this column
      }
    ],
    [t]
  )

  const RightButtons = () => {
    return (
      <Button variant='contained' onClick={() => query.set('filter', 'true')}>
        {t('Filter')}
      </Button>
    )
  }

  const animOne = {
    x: -100,
    opacity: 0
  }

  const animTwo = {
    opacity: 1,
    x: 0,
    transition: 0.2,
    delay: 0.2,
    stagger: 2
  }

  return (
    <AnimationContainer animOne={animOne} animTwo={animTwo}>
      <Box>
        <CustomHeader pageName='Products' deleteUrl='/product/delete' rightButtons={<RightButtons />} />
        <CustomTable
          url='/product/list'
          columns={columns}
          rowIdKey='id'
          params={{
            category,
            brand,
            type
          }}
        />
        <CustomDrawer
          open={query.has('id') || query.has('onAdd')}
          onClose={() => {
            if (query.has('id')) {
              query.remove('id')
            } else if (query.has('onAdd')) {
              query.remove('onAdd')
            }
          }}
        >
          <ProductForm />
        </CustomDrawer>

        <CustomModal
          open={query.has('filter')}
          onClose={() => {
            if (query.has('filter')) {
              query.remove('filter')
            }
          }}
        >
          <ProductFilter
            category={category}
            setCategory={setCategory}
            brand={brand}
            setBrand={setBrand}
            setType={setType}
            type={type}
          />
        </CustomModal>
      </Box>
    </AnimationContainer>
  )
}

export default ProductsList
