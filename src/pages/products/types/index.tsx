import { MRT_ColumnDef } from 'material-react-table'
import React, { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { Box, Button } from '@mui/material'
import Translations from 'src/layouts/components/Translations'
import CustomHeader from '../../../layouts/custom-table/custom-header'
import CustomTable from '../../../layouts/custom-table'
import { CustomDrawer } from '../../../layouts/custom-table/custom-drawer'
import useQueryParams from 'src/hooks/useQueryParams'
import Icon from 'src/@core/components/icon'
import dynamic from 'next/dynamic'
import AnimationContainer from 'src/components/animations/anim-container/AnimAtionContainer'

const TypeForm = dynamic(() => import('src/components/forms/TypeForm'), { ssr: false })
type dataProps = {
  name: string
}

function Types() {
  const { t } = useTranslation()
  const query = useQueryParams()

  const columns = useMemo<MRT_ColumnDef<dataProps>[]>(
    () => [
      {
        accessorKey: 'name',
        header: t('Name'),
        size: 300,
        enableGlobalFilter: true
      },
      {
        accessorKey: 'id',
        header: t('Actions'),
        enableColumnFilter: false,
        enableGlobalFilter: false,
        enableSorting: false,
        Cell: () => (
          <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: 2 }}>
            <Button variant='text' color='primary'>
              <Icon icon='tabler:edit' fontSize={20} />
              <Translations text='Edit' />
            </Button>
          </Box>
        ),
        size: 200 //decrease the width of this column
      }
    ],
    [t]
  )

  const animOne = {
    x: -100,
    opacity: 0
  }

  const animTwo = {
    opacity: 1,
    x: 0,
    transition: 0.2,
    delay: 0.2,
    stagger: 2
  }

  return (
    <AnimationContainer animOne={animOne} animTwo={animTwo}>
      <Box>
        <CustomHeader pageName='Types' deleteUrl='/product/type/delete' />
        <CustomTable url='/product/type/list' columns={columns} rowIdKey='id' />
        <CustomDrawer
          open={query.has('id') || query.has('onAdd')}
          onClose={() => {
            if (query.has('id')) {
              query.remove('id')
            } else if (query.has('onAdd')) {
              query.remove('onAdd')
            }
          }}
        >
          <TypeForm />
        </CustomDrawer>
      </Box>
    </AnimationContainer>
  )
}

export default Types
