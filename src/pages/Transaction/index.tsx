import { MRT_ColumnDef } from 'material-react-table'
import React, { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { Box } from '@mui/material'
import CustomHeader from '../../layouts/custom-table/custom-header'
import CustomTable from '../../layouts/custom-table'
import AnimationContainer from 'src/components/animations/anim-container/AnimAtionContainer'

type dataProps = {
  id: number
  price: string
  status: boolean
  to_store: {
    id: number
    name: string
  }
  from_store: {
    id: number
    name: string
  }
}

function Transactions() {
  const { t } = useTranslation()

  const columns = useMemo<MRT_ColumnDef<dataProps>[]>(
    () => [
      {
        accessorKey: 'price',
        header: t('Price'),
        size: 300,
        enableGlobalFilter: true
      },

      {
        accessorKey: 'from_store.name',
        header: t('From Store'),
        enableGlobalFilter: true,
        size: 300
      },
      {
        accessorKey: 'to_store.name',
        header: t('To Store'),
        enableGlobalFilter: true,
        size: 300
      }
    ],
    [t]
  )

  const animOne = {
    x: -100,
    opacity: 0
  }

  const animTwo = {
    opacity: 1,
    x: 0,
    transition: 0.2,
    delay: 0.2,
    stagger: 2
  }

  return (
    <AnimationContainer animOne={animOne} animTwo={animTwo}>
      <Box sx={{ pt: '2rem' }}>
        <CustomHeader pageName='Transaction' hideAdd hideDelete />
        <CustomTable
          disableRowSelection
          url='/stock/transaction-list'
          columns={columns}
          rowIdKey='id'
          disableBodyClick={true}
        />
      </Box>
    </AnimationContainer>
  )
}

export default Transactions
