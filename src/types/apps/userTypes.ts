// ** Types
import { ThemeColor } from 'src/@core/layouts/types'

export type UserRole  = {
  id: number
  name: string
  created_at: string
}
interface PermissionsProps {
  id:number
  name: string
  slug:string
  created_at: string
}
export type UsersType = {
  id: number
  access_urls: string[]
  bonus?:boolean | null
  currency?:number | string
  firstname?: string
  lastname?: string
  surname?: string
  login?:string
  permission?: PermissionsProps[]
  photo?: string | null
  rent?: boolean
  rent_percentage?:number | string
  role?: UserRole
  store?: number
  joined_date?:string 
  is_superuser?:boolean
}

export type ProjectListDataType = {
  id: number
  img: string
  hours: string
  totalTask: string
  projectType: string
  projectTitle: string
  progressValue: number
  progressColor: ThemeColor
}
