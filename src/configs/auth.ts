export default {
  meEndpoint: process.env.NEXT_API_URL + '/user/info/',
  loginEndpoint: process.env.NEXT_API_URL ?  process.env.NEXT_API_URL +  '/auth/login'  : 'https://test.janaporcelain.uz/api/auth/login',
  registerEndpoint: '/jwt/register',
  storageTokenKeyName: 'access',
  onTokenExpiration: 'refresh' // logout | refreshToken
}
