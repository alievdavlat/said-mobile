// ** Type import
import { VerticalNavItemsType } from 'src/@core/layouts/types'

const navigation = (): VerticalNavItemsType => {
  return [
    {
      title: 'Home',
      icon: 'tabler:smart-home',
      path: '/home',
      badgeContent: 'Main',
      badgeColor: 'success'
    },

    {
      title: 'Users',
      icon: 'ph:users-four-thin',
      path: '/users-list'
    },
    {
      title: 'Attentdance',
      icon: 'clarity:list-line',
      path: '/attentdance'
    },
    {
      title: 'Employees',
      icon: 'mdi:human-queue',
      path: '/employees-list'
    },
    {
      title: 'Currency',
      icon: 'healthicons:money-bag-outline',
      path: '/currency-list'
    },
    {
      title: 'Stocks',
      icon: 'bi:box',
      path: '/stocks-list'
    },

    {
      title: 'Store Products',
      icon: 'material-symbols-light:store-outline',
      path: '/store-products'
    },
    {
      title: 'Store Balance',
      icon: 'fluent:money-hand-16-regular',
      path: '/store-balance'
    },
    {
      title: 'Products',
      icon: 'ic:baseline-blur-on',
      path: '/products',
      children: [
        {
          title: 'Products',
          path: '/products/products-list'
        },
        {
          title: 'Brands',
          path: '/products/brands'
        },

        {
          title: 'Types',
          path: '/products/types'
        },
        {
          title: 'Categories',
          path: '/products/categories'
        },
        {
          title: 'Regions',
          path: '/products/region'
        }
      ]
    },
    {
      title: 'Prices',
      icon: 'material-symbols-light:attach-money-rounded',
      path: '/prices-list'
    },
    {
      title: 'Invoices',
      icon: 'iconamoon:invoice-thin',
      path: '/invoices-list'
    },
    {
      title: 'Sale',
      icon: 'teenyicons:bag-outline',
      path: '/sale-list'
    },
    {
      title: 'Cargo',
      icon: 'iconoir:delivery-truck',
      path: '/cargo'
    },
    {
      title: 'Reception',
      icon: 'guidance:office',
      path: '/reception'
    },
    {
      title: 'Distribution',
      icon: 'uil:horizontal-distribution-center',
      path: '/distribution'
    },

    {
      title: 'Accounting',
      icon: 'mdi:account-cog-outline',
      path: '/accounting'
    },
    {
      title: 'Profile',
      icon: 'healthicons:ui-user-profile-outline',
      path: '/pages/user-profile/profile'
    },
  ]
}

export default navigation
