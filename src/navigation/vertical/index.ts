// ** Type import
import { VerticalNavItemsType } from 'src/@core/layouts/types'

const navigation = (): VerticalNavItemsType => {
  return [
    {
      title: 'Home',
      icon: 'tabler:smart-home',
      path: '/home',
      badgeContent: 'Main',
      badgeColor: 'success',
      subject:'home'
    },

    {
      title: 'Users',
      icon: 'ph:users-four-thin',
      path: '/users-list',
      subject:"users"
    },
    {
      title: 'Attentdance',
      icon: 'clarity:list-line',
      path: '/attentdance',
      subject:'attendance'
    },
    {
      title:"Transfers",
      icon:"ri:currency-line",
      path:'/Transfer',
      subject:'transfer'
    },
    {
      title:"Transactions",
      icon:"uil:transaction",
      path:'/Transaction',
      subject:"transaction"
    },

 
    {
      title: 'Currency',
      icon: 'healthicons:money-bag-outline',
      path: '/currency-list',
      subject:'currency'
    },
    {
      title: 'Stocks',
      icon: 'bi:box',
      path: '/stocks-list',
      subject:'stocks'
    },

    {
      title: 'Store Products',
      icon: 'material-symbols-light:store-outline',
      path: '/store-products',
      subject:'store-products'
    },
    {
      title: 'Store Balance',
      icon: 'fluent:money-hand-16-regular',
      path: '/store-balance',
      subject:'store-balance'
    },
    {
      title: 'Costs',
      icon: 'ic:sharp-currency-exchange',
      path: '/cost',
      subject:'cost'
    },
    {
      title: 'Products',
      icon: 'ic:baseline-blur-on',
      path: '/products',
      subject:'products',
      children: [
        {
          title: 'Products',
          path: '/products/products-list',
          subject:'products',

        },
        {
          title: 'Brands',
          path: '/products/brands',
          subject:'brands'
        },

        {
          title: 'Types',
          path: '/products/types',
          subject:'types'
        },
        {
          title: 'Categories',
          path: '/products/categories',
          subject:'categories'
        },
        {
          title: 'Regions',
          path: '/products/region',
          subject:'regions'
        }
      ]
    },
    {
      title: 'Prices',
      icon: 'material-symbols-light:attach-money-rounded',
      path: '/prices-list',
      subject:'prices'
    },

    {
      title: 'Cargo',
      icon: 'iconoir:delivery-truck',
      path: '/cargo',
      subject:'cargo'
    },
    {
      title: 'Clients',
      icon: 'carbon:customer',
      path: '/clients',
      subject:"clients"
    },
    {
      title: 'Orders',
      icon: 'material-symbols:order-approve-outline',
      path: '/orders',
      subject:'orders'
    },
    {
      title: 'Profile',
      icon: 'healthicons:ui-user-profile-outline',
      path: '/profile',
      subject:'profile'
    },
    
       // {
    //   title: 'Invoices',
    //   icon: 'iconamoon:invoice-thin',
    //   path: '/invoices-list',
    //   subject:'invoices'
    // },
       // {
    //   title: 'Employees',
    //   icon: 'mdi:human-queue',
    //   path: '/employees-list',
    //   subject:'employees'
    // },
     
    // {
    //   title: 'Sale',
    //   icon: 'teenyicons:bag-outline',
    //   path: '/sale-list',
    //   subject:'sale'
    // },
    // {
    //   title: 'Reception',
    //   icon: 'guidance:office',
    //   path: '/reception',
    //   subject:'reception'
    // },
    // {
    //   title: 'Accounting',
    //   icon: 'mdi:account-cog-outline',
    //   path: '/accounting',
    //   subject:"accounting"
    // },
    // {
    //   title: 'Distribution',
    //   icon: 'uil:horizontal-distribution-center',
    //   path: '/distribution',
    //   subject:"distribution"
    // },
  ]
}

export default navigation
